package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoginsSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<Logins> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private LoginsSearchInput searchInput;

   public LoginsSearchResult()
   {
      super();
   }

   public LoginsSearchResult(Long count, List<Logins> resultList,
         LoginsSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<Logins> getResultList()
   {
      return resultList;
   }

   public LoginsSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<Logins> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(LoginsSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
