package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PropertyTypeSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<PropertyType> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private PropertyTypeSearchInput searchInput;

   public PropertyTypeSearchResult()
   {
      super();
   }

   public PropertyTypeSearchResult(Long count, List<PropertyType> resultList,
         PropertyTypeSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<PropertyType> getResultList()
   {
      return resultList;
   }

   public PropertyTypeSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<PropertyType> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(PropertyTypeSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
