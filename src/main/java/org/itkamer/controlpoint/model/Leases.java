package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Lease 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Leases  extends CPBaseEntity implements Serializable {
	

	
	@ManyToOne
	private Unit unit;
	@ManyToOne
	private Logins login;
	private Date start;
	private Date end;
	private BigDecimal amount;
	@Column
	@Enumerated(EnumType.STRING)
	private LeaseFrequency leaseFrequency;
	private Date nextDue;
	private String comment;

	public Leases() {
	}



	
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}


	public Logins getLogin() {
		return this.login;
	}

	public void setLogin(Logins login) {
		this.login = login;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="start", nullable=false, length=19)
	public Date getStart() {
		return this.start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="end", length=19)
	public Date getEnd() {
		return this.end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}


	@Column(name="amount", nullable=false)
	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public LeaseFrequency getLeaseFrequency() {
		return this.leaseFrequency;
	}

	public void setLeaseFrequency(LeaseFrequency leaseFrequency) {
		this.leaseFrequency = leaseFrequency;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="nextDue", nullable=false, length=19)
	public Date getNextDue() {
		return this.nextDue;
	}

	public void setNextDue(Date nextDue) {
		this.nextDue = nextDue;
	}


	@Column(name="comment", length=254)
	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}




}


