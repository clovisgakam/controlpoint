package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AppInstancesSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<AppInstances> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private AppInstancesSearchInput searchInput;

   public AppInstancesSearchResult()
   {
      super();
   }

   public AppInstancesSearchResult(Long count, List<AppInstances> resultList,
         AppInstancesSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<AppInstances> getResultList()
   {
      return resultList;
   }

   public AppInstancesSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<AppInstances> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(AppInstancesSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
