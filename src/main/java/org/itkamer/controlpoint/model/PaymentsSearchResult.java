package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PaymentsSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<Payments> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private PaymentsSearchInput searchInput;

   public PaymentsSearchResult()
   {
      super();
   }

   public PaymentsSearchResult(Long count, List<Payments> resultList,
         PaymentsSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<Payments> getResultList()
   {
      return resultList;
   }

   public PaymentsSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<Payments> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(PaymentsSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
