package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author clovisgakam
 * The Quotation 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Quotations  extends CPBaseEntity implements Serializable {
	

     
     @ManyToOne
     private Vendors vendor;
     @ManyToOne
     private Tasks task;
     private String heading;
     private Date date;
     private BigDecimal amount;
     private String details;
     private boolean approved;

    public Quotations() {
    }

	
    
   
    

    
    public Vendors getVendor() {
        return this.vendor;
    }
    
    public void setVendor(Vendors vendor) {
        this.vendor = vendor;
    }

    
    public Tasks getTask() {
        return this.task;
    }
    
    public void setTask(Tasks task) {
        this.task = task;
    }

    
    @Column(name="heading", nullable=false, length=254)
    public String getHeading() {
        return this.heading;
    }
    
    public void setHeading(String heading) {
        this.heading = heading;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date", nullable=false, length=19)
    public Date getDate() {
        return this.date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }

    
    @Column(name="amount", nullable=false)
    public BigDecimal getAmount() {
        return this.amount;
    }
    
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    
    @Column(name="details", length=254)
    public String getDetails() {
        return this.details;
    }
    
    public void setDetails(String details) {
        this.details = details;
    }

    
    @Column(name="approved", nullable=false)
    public boolean isApproved() {
        return this.approved;
    }
    
    public void setApproved(boolean approved) {
        this.approved = approved;
    }




}


