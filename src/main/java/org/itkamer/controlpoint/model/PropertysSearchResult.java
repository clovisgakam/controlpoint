package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PropertysSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<Propertys> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private PropertysSearchInput searchInput;

   public PropertysSearchResult()
   {
      super();
   }

   public PropertysSearchResult(Long count, List<Propertys> resultList,
         PropertysSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<Propertys> getResultList()
   {
      return resultList;
   }

   public PropertysSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<Propertys> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(PropertysSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
