package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.time.DateUtils;

/**
 * Login 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Logins  extends CPBaseEntity implements Serializable {



	@ManyToOne(cascade={CascadeType.MERGE})
	private AppInstances appInstance;
	@ManyToOne(cascade={CascadeType.PERSIST,CascadeType.MERGE})
	private Address address;
	private String firstName;
	private String lastName;
	private String homePhone;
	private String mobilePhone;
	private String landPhone;
	private String email;
	private String password;
	@Column
	@Enumerated(EnumType.STRING)
	private Gender gender;
	private Date recordDate;
	@Column
	@Enumerated(EnumType.STRING)
	private LoginStatus status = LoginStatus.ACTIVE;
	private String avatarPath;
	@Column
	private Boolean disableLogin = Boolean.FALSE;
	
	@Column
	private Boolean registered = Boolean.FALSE;

	@Column
	private Boolean admin = Boolean.FALSE;

	@Column
	private Boolean accountLocked = Boolean.FALSE;

	@Temporal(TemporalType.TIMESTAMP)
	private Date credentialExpiration = DateUtils.addYears(new Date(), 10);

	@Temporal(TemporalType.TIMESTAMP)
	private Date accountExpiration = DateUtils.addYears(new Date(), 10);

	public Logins() {
	}

	public AppInstances getAppInstance() {
		return this.appInstance;
	}

	public void setAppInstance(AppInstances appInstance) {
		this.appInstance = appInstance;
	}


	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}


	@Column(name="firstName", length=254)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	@Column(name="lastName", length=254)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	@Column(name="homePhone", length=254)
	public String getHomePhone() {
		return this.homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}


	@Column(name="mobilePhone", length=254)
	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}


	@Column(name="landPhone", length=254)
	public String getLandPhone() {
		return this.landPhone;
	}

	public void setLandPhone(String landPhone) {
		this.landPhone = landPhone;
	}


	@Column(name="email", nullable=false, length=254)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	@Column(name="password", nullable=false, length=254)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public Gender getGender() {
		return this.gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="recordDate", nullable=false, length=19)
	public Date getRecordDate() {
		return this.recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}


	public LoginStatus getStatus() {
		return this.status;
	}

	public void setStatus(LoginStatus status) {
		this.status = status;
	}


	@Column(name="avatarPath", length=254)
	public String getAvatarPath() {
		return this.avatarPath;
	}

	public void setAvatarPath(String avatarPath) {
		this.avatarPath = avatarPath;
	}

	public Boolean getDisableLogin() {
		return disableLogin;
	}

	public void setDisableLogin(Boolean disableLogin) {
		this.disableLogin = disableLogin;
	}

	public Boolean getAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(Boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public Date getCredentialExpiration() {
		return credentialExpiration;
	}

	public void setCredentialExpiration(Date credentialExpiration) {
		this.credentialExpiration = credentialExpiration;
	}

	public Date getAccountExpiration() {
		return accountExpiration;
	}

	public void setAccountExpiration(Date accountExpiration) {
		this.accountExpiration = accountExpiration;

	}
	public Boolean getAdmin() {
		return admin;
	}
	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}

	public Boolean getRegistered() {
		return registered;
	}

	public void setRegistered(Boolean registered) {
		this.registered = registered;
	}
	

}


