package org.itkamer.controlpoint.model;

public enum LoginStatus
{
	ACTIVE, CLOSED
}