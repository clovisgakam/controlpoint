package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TasksSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<Tasks> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private TasksSearchInput searchInput;

   public TasksSearchResult()
   {
      super();
   }

   public TasksSearchResult(Long count, List<Tasks> resultList,
         TasksSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<Tasks> getResultList()
   {
      return resultList;
   }

   public TasksSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<Tasks> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(TasksSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
