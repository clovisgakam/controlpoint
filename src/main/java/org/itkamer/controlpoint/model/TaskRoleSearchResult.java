package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaskRoleSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<TaskRole> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private TaskRoleSearchInput searchInput;

   public TaskRoleSearchResult()
   {
      super();
   }

   public TaskRoleSearchResult(Long count, List<TaskRole> resultList,
         TaskRoleSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<TaskRole> getResultList()
   {
      return resultList;
   }

   public TaskRoleSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<TaskRole> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(TaskRoleSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
