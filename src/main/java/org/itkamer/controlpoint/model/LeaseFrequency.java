package org.itkamer.controlpoint.model;

public enum LeaseFrequency
{
	ONCE, DAILY, WEEKLY, MONTHLY, YEARLY
}