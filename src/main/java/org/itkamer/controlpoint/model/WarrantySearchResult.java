package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WarrantySearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<Warranty> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private WarrantySearchInput searchInput;

   public WarrantySearchResult()
   {
      super();
   }

   public WarrantySearchResult(Long count, List<Warranty> resultList,
         WarrantySearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<Warranty> getResultList()
   {
      return resultList;
   }

   public WarrantySearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<Warranty> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(WarrantySearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
