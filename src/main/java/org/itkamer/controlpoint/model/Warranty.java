package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Warranty 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Warranty  extends CPBaseEntity implements Serializable {
	

     
     @ManyToOne
     private ScheduledEvent scheduledEvent;
     @ManyToOne
     private Vendors vendor;
     private String heading;
     private Date start;
     private Date end;
     private String details;

    public Warranty() {
    }

	
   
   
    

    
    public ScheduledEvent getScheduledEvent() {
        return this.scheduledEvent;
    }
    
    public void setScheduledEvent(ScheduledEvent scheduledEvent) {
        this.scheduledEvent = scheduledEvent;
    }

    
    public Vendors getVendor() {
        return this.vendor;
    }
    
    public void setVendor(Vendors vendor) {
        this.vendor = vendor;
    }

    
    @Column(name="heading", nullable=false, length=254)
    public String getHeading() {
        return this.heading;
    }
    
    public void setHeading(String heading) {
        this.heading = heading;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="start", nullable=false, length=19)
    public Date getStart() {
        return this.start;
    }
    
    public void setStart(Date start) {
        this.start = start;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="end", nullable=false, length=19)
    public Date getEnd() {
        return this.end;
    }
    
    public void setEnd(Date end) {
        this.end = end;
    }

    
    @Column(name="details", length=254)
    public String getDetails() {
        return this.details;
    }
    
    public void setDetails(String details) {
        this.details = details;
    }




}


