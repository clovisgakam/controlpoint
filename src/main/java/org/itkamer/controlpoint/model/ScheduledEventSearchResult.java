package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ScheduledEventSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<ScheduledEvent> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private ScheduledEventSearchInput searchInput;

   public ScheduledEventSearchResult()
   {
      super();
   }

   public ScheduledEventSearchResult(Long count, List<ScheduledEvent> resultList,
         ScheduledEventSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<ScheduledEvent> getResultList()
   {
      return resultList;
   }

   public ScheduledEventSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<ScheduledEvent> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(ScheduledEventSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
