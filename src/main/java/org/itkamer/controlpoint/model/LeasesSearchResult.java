package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LeasesSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<Leases> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private LeasesSearchInput searchInput;

   public LeasesSearchResult()
   {
      super();
   }

   public LeasesSearchResult(Long count, List<Leases> resultList,
         LeasesSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<Leases> getResultList()
   {
      return resultList;
   }

   public LeasesSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<Leases> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(LeasesSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
