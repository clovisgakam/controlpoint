package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author clovisgakam
 * Task 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Tasks  extends CPBaseEntity implements Serializable {
	

     
     @ManyToOne
     private TaskCategorys category;
     private String refNumber;
     @Column
     @Enumerated(EnumType.STRING)
     private TaskType type;
     private String heading;
     private String description;
     private Date creation;
     private Date estCompletion;
     private Date actCompletion;
     @Column
     @Enumerated(EnumType.STRING)
     private TaskPriority priority;
     private int pkiScore;
     @Column
     @Enumerated(EnumType.STRING)
     private TaskStatus status;
     private boolean sentToVendor;
     private boolean sentToTrustee;
     private boolean approved;

    public Tasks() {
    }

	
   
   
   
    
    @Column(name="refNumber", nullable=false, length=254)
    public String getRefNumber() {
        return this.refNumber;
    }
    
    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }
    
    public TaskType getType() {
        return this.type;
    }
    
    public void setType(TaskType type) {
        this.type = type;
    }

    
    @Column(name="heading", nullable=false, length=254)
    public String getHeading() {
        return this.heading;
    }
    
    public void setHeading(String heading) {
        this.heading = heading;
    }

    
    @Column(name="description", length=254)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="creation", nullable=false, length=19)
    public Date getCreation() {
        return this.creation;
    }
    
    public void setCreation(Date creation) {
        this.creation = creation;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="estCompletion", length=19)
    public Date getEstCompletion() {
        return this.estCompletion;
    }
    
    public void setEstCompletion(Date estCompletion) {
        this.estCompletion = estCompletion;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="actCompletion", length=19)
    public Date getActCompletion() {
        return this.actCompletion;
    }
    
    public void setActCompletion(Date actCompletion) {
        this.actCompletion = actCompletion;
    }

    
    public TaskPriority getPriority() {
        return this.priority;
    }
    
    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    
    @Column(name="pkiScore", nullable=false)
    public int getPkiScore() {
        return this.pkiScore;
    }
    
    public void setPkiScore(int pkiScore) {
        this.pkiScore = pkiScore;
    }

    
    @Column(name="status", nullable=false, length=254)
    public TaskStatus getStatus() {
        return this.status;
    }
    
    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    
    @Column(name="sentToVendor", nullable=false)
    public boolean isSentToVendor() {
        return this.sentToVendor;
    }
    
    public void setSentToVendor(boolean sentToVendor) {
        this.sentToVendor = sentToVendor;
    }

    
    @Column(name="sentToTrustee", nullable=false)
    public boolean isSentToTrustee() {
        return this.sentToTrustee;
    }
    
    public void setSentToTrustee(boolean sentToTrustee) {
        this.sentToTrustee = sentToTrustee;
    }

    
    @Column(name="approved", nullable=false)
    public boolean isApproved() {
        return this.approved;
    }
    
    public void setApproved(boolean approved) {
        this.approved = approved;
    }

	public TaskCategorys getCategory() {
		return category;
	}

	public void setCategory(TaskCategorys category) {
		this.category = category;
	}




}


