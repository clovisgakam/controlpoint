package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class VendorCategorysSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<VendorCategorys> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private VendorCategorysSearchInput searchInput;

   public VendorCategorysSearchResult()
   {
      super();
   }

   public VendorCategorysSearchResult(Long count, List<VendorCategorys> resultList,
         VendorCategorysSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<VendorCategorys> getResultList()
   {
      return resultList;
   }

   public VendorCategorysSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<VendorCategorys> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(VendorCategorysSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
