package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 * @author clovisgakam
 * the AttachedFile
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class AttachedFile  extends CPBaseEntity implements Serializable {

	
     
     @ManyToOne
     private Tasks task;
     private String path;
     private String type;

    public AttachedFile() {
    }

   
   

    @Column(name="path", nullable=false, length=254)
    public String getPath() {
        return this.path;
    }
    
    public void setPath(String path) {
        this.path = path;
    }

    
    @Column(name="type", nullable=false, length=254)
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }


	public Tasks getTask() {
		return task;
	}


	public void setTask(Tasks task) {
		this.task = task;
	}




}


