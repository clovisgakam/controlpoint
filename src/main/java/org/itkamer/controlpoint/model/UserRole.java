package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.time.DateUtils;

/**
 * UserRole 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class UserRole  extends CPBaseEntity implements Serializable {
	

     
     @ManyToOne
     private Logins login;
     @ManyToOne
     private Unit unit;
     @ManyToOne
     private Propertys property;
     @Column
     @Enumerated(EnumType.STRING)
     private RoleNames role = RoleNames.CARETAKER;
     private Date start = new Date();
     private Date end = DateUtils.addDays(new Date(), 30);
     @Column
     @Enumerated(EnumType.STRING)
     private UserPermission permission = UserPermission.VIEW;
     private boolean invited = false;
     @Column
     @Enumerated(EnumType.STRING)
     private LoginStatus status = LoginStatus.ACTIVE;

    public UserRole() {
    }
    public Logins getLogin() {
        return this.login;
    }
    
    public void setLogin(Logins login) {
        this.login = login;
    }

    
    public Unit getUnit() {
        return this.unit;
    }
    
    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    
    public Propertys getProperty() {
        return this.property;
    }
    
    public void setProperty(Propertys property) {
        this.property = property;
    }

    
    public RoleNames getRole() {
        return this.role;
    }
    
    public void setRole(RoleNames role) {
        this.role = role;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="start", nullable=false, length=19)
    public Date getStart() {
        return this.start;
    }
    
    public void setStart(Date start) {
        this.start = start;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="end", length=19)
    public Date getEnd() {
        return this.end;
    }
    
    public void setEnd(Date end) {
        this.end = end;
    }

    
    public UserPermission getPermission() {
        return this.permission;
    }
    
    public void setPermission(UserPermission permission) {
        this.permission = permission;
    }

    
    @Column(name="invited", nullable=false)
    public boolean isInvited() {
        return this.invited;
    }
    
    public void setInvited(boolean invited) {
        this.invited = invited;
    }

    
    public LoginStatus getStatus() {
        return this.status;
    }
    
    public void setStatus(LoginStatus status) {
        this.status = status;
    }




}


