package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class QuotationsSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<Quotations> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private QuotationsSearchInput searchInput;

   public QuotationsSearchResult()
   {
      super();
   }

   public QuotationsSearchResult(Long count, List<Quotations> resultList,
         QuotationsSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<Quotations> getResultList()
   {
      return resultList;
   }

   public QuotationsSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<Quotations> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(QuotationsSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
