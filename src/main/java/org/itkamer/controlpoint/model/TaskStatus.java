package org.itkamer.controlpoint.model;

public enum TaskStatus
{
   OK, IMMINENT, CRITICAL, CLOSED
}