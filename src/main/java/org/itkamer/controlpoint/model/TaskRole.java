package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 * TaskRole 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class TaskRole  extends CPBaseEntity implements Serializable {



	@ManyToOne
	private Logins login;
	@ManyToOne
	private Tasks task;
	@Column
	@Enumerated(EnumType.STRING)
	private TaskRoles role;

	public TaskRole() {
	}
	public Logins getLogin() {
		return login;
	}

	public void setLogin(Logins login) {
		this.login = login;
	}

	public Tasks getTask() {
		return task;
	}

	public void setTask(Tasks task) {
		this.task = task;
	}






	public TaskRoles getRole() {
		return this.role;
	}

	public void setRole(TaskRoles role) {
		this.role = role;
	}




}


