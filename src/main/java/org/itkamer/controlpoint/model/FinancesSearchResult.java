package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FinancesSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<Finances> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private FinancesSearchInput searchInput;

   public FinancesSearchResult()
   {
      super();
   }

   public FinancesSearchResult(Long count, List<Finances> resultList,
         FinancesSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<Finances> getResultList()
   {
      return resultList;
   }

   public FinancesSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<Finances> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(FinancesSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
