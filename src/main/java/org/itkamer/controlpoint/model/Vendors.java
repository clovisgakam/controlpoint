package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 * Vendor 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Vendors  extends CPBaseEntity implements Serializable {
	

     
	 @ManyToOne(cascade={CascadeType.PERSIST,CascadeType.REMOVE},fetch=FetchType.EAGER)
     private Company company;
     @ManyToOne(cascade={CascadeType.PERSIST,CascadeType.REMOVE},fetch=FetchType.EAGER)
     private Address address;
     @ManyToOne
     private VendorCategorys category;
     private String firstName;
     private String lastName;
     private String phone;
     private String fax;
     private String email;
     private Integer rate;

    public Vendors() {
    }

    
    public Company getCompany() {
        return this.company;
    }
    
    public void setCompany(Company company) {
        this.company = company;
    }

    
    public Address getAddress() {
        return this.address;
    }
    
    public void setAddress(Address address) {
        this.address = address;
    }

    
    public VendorCategorys getCategory() {
        return this.category;
    }
    
    public void setCategory(VendorCategorys category) {
        this.category = category;
    }

    
    @Column(name="firstName", length=254)
    public String getFirstName() {
        return this.firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    
    @Column(name="lastName", nullable=false, length=254)
    public String getLastName() {
        return this.lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    
    @Column(name="phone", nullable=false, length=254)
    public String getPhone() {
        return this.phone;
    }
    
    public void setPhone(String phone) {
        this.phone = phone;
    }

    
    @Column(name="fax", length=254)
    public String getFax() {
        return this.fax;
    }
    
    public void setFax(String fax) {
        this.fax = fax;
    }

    
    @Column(name="email", nullable=false, length=254)
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    
    @Column(name="rate")
    public Integer getRate() {
        return this.rate;
    }
    
    public void setRate(Integer rate) {
        this.rate = rate;
    }




}


