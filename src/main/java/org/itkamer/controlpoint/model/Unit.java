package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 * Unit 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Unit  extends CPBaseEntity implements Serializable {
	

     
     @ManyToOne
     private Propertys property;
     private String name;
     private Integer size;
     @Column
     @Enumerated(EnumType.STRING)
     private UnitAvailableStaus status;
     private String description;
     private String imagePath;

    public Unit() {
    }

	
   
   
    

    
    public Propertys getProperty() {
        return this.property;
    }
    
    public void setProperty(Propertys property) {
        this.property = property;
    }

    
    @Column(name="name", nullable=false, length=254)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="size")
    public Integer getSize() {
        return this.size;
    }
    
    public void setSize(Integer size) {
        this.size = size;
    }

    
    public UnitAvailableStaus getStatus() {
        return this.status;
    }
    
    public void setStatus(UnitAvailableStaus status) {
        this.status = status;
    }

    
    @Column(name="description", length=254)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="imagePath", length=254)
    public String getImagePath() {
        return this.imagePath;
    }
    
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }




}


