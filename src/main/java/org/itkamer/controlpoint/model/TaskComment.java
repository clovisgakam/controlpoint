package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 * @author clovisgakam
 * The TaskComment 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class TaskComment  extends CPBaseEntity implements Serializable {
	

     
     @ManyToOne
     private Tasks task;
     @ManyToOne
     private Logins login;
     private String content;

    public TaskComment() {
    }

  
   
    

    
    public Tasks getTask() {
        return this.task;
    }
    
    public void setTask(Tasks task) {
        this.task = task;
    }

    
    public Logins getLogin() {
        return this.login;
    }
    
    public void setLogin(Logins login) {
        this.login = login;
    }

    
    @Column(name="content", nullable=false, length=254)
    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }




}


