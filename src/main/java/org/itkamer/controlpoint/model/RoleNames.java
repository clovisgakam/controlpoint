package org.itkamer.controlpoint.model;

public enum RoleNames
{
   PUBLIC, ADMINISTRATOR, CARETAKER, PROPERTY_MANAGER, OWNER, TENANT,TRUSTEE
}