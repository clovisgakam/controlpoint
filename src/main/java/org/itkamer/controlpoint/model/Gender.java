package org.itkamer.controlpoint.model;

public enum Gender
{
   MALE, FEMALE, NEUTRAL
}