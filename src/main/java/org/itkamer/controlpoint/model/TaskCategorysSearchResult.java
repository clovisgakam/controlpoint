package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaskCategorysSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<TaskCategorys> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private TaskCategorysSearchInput searchInput;

   public TaskCategorysSearchResult()
   {
      super();
   }

   public TaskCategorysSearchResult(Long count, List<TaskCategorys> resultList,
         TaskCategorysSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<TaskCategorys> getResultList()
   {
      return resultList;
   }

   public TaskCategorysSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<TaskCategorys> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(TaskCategorysSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
