package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class VendorsSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<Vendors> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private VendorsSearchInput searchInput;

   public VendorsSearchResult()
   {
      super();
   }

   public VendorsSearchResult(Long count, List<Vendors> resultList,
         VendorsSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<Vendors> getResultList()
   {
      return resultList;
   }

   public VendorsSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<Vendors> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(VendorsSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
