package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AddressSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<Address> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private AddressSearchInput searchInput;

   public AddressSearchResult()
   {
      super();
   }

   public AddressSearchResult(Long count, List<Address> resultList,
         AddressSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<Address> getResultList()
   {
      return resultList;
   }

   public AddressSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<Address> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(AddressSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
