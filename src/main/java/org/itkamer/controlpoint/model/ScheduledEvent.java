package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author clovisgakam
 *  The ScheduledEvent 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class ScheduledEvent  extends CPBaseEntity implements Serializable {

	
     
     @ManyToOne
     private Propertys property;
     private String heading;
     private Date start;
     private int frequency;
     private boolean remind;
     @Column
     @Enumerated(EnumType.STRING)
     private RemindOptions remindOption;

    public ScheduledEvent() {
    }

	
    
   
    

    
    public Propertys getProperty() {
        return this.property;
    }
    
    public void setProperty(Propertys property) {
        this.property = property;
    }

    
    @Column(name="heading", nullable=false, length=254)
    public String getHeading() {
        return this.heading;
    }
    
    public void setHeading(String heading) {
        this.heading = heading;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="start", nullable=false, length=19)
    public Date getStart() {
        return this.start;
    }
    
    public void setStart(Date start) {
        this.start = start;
    }

    
    @Column(name="frequency", nullable=false)
    public int getFrequency() {
        return this.frequency;
    }
    
    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    
    @Column(name="remind", nullable=false)
    public boolean isRemind() {
        return this.remind;
    }
    
    public void setRemind(boolean remind) {
        this.remind = remind;
    }

    
    public RemindOptions getRemindOption() {
        return this.remindOption;
    }
    
    public void setRemindOption(RemindOptions remindOption) {
        this.remindOption = remindOption;
    }




}


