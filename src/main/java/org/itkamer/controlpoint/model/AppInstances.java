package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * @author clovisgakam 
 * the  AppInstance
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class AppInstances  extends CPBaseEntity implements Serializable {
	

     
     @ManyToOne
     private Address address;
     @NotNull
     @Column
     private String company;
     private Date start;
     private Date end;
     @Column
     @Enumerated(EnumType.STRING)
     private LoginStatus status = LoginStatus.ACTIVE;
     private boolean trial;

    public AppInstances() {
    }

	
 
   
    

    
    public Address getAddress() {
        return this.address;
    }
    
    public void setAddress(Address address) {
        this.address = address;
    }

    public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="start", nullable=false, length=19)
    public Date getStart() {
        return this.start;
    }
    
    public void setStart(Date start) {
        this.start = start;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="end", length=19)
    public Date getEnd() {
        return this.end;
    }
    
    public void setEnd(Date end) {
        this.end = end;
    }

    public LoginStatus getStatus() {
		return status;
	}

	public void setStatus(LoginStatus status) {
		this.status = status;
	}

	@Column(name="trial", nullable=false)
    public boolean isTrial() {
        return this.trial;
    }
    
    public void setTrial(boolean trial) {
        this.trial = trial;
    }




}


