package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserRoleSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<UserRole> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private UserRoleSearchInput searchInput;

   public UserRoleSearchResult()
   {
      super();
   }

   public UserRoleSearchResult(Long count, List<UserRole> resultList,
         UserRoleSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<UserRole> getResultList()
   {
      return resultList;
   }

   public UserRoleSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<UserRole> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(UserRoleSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
