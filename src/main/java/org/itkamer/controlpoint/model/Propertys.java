package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 * @author clovisgakam
 * the Property 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Propertys  extends CPBaseEntity implements Serializable {

	
     
     @ManyToOne(cascade={CascadeType.PERSIST,CascadeType.REMOVE})
     private Address address;
     @ManyToOne
     private AppInstances appInstance;
     @ManyToOne
     private PropertyType type;
     private String name;
     private int numberOfUnits;
     private Integer size;
     private String description;
     private String imagePath;

    public Propertys() {
    }

    
    public Address getAddress() {
        return this.address;
    }
    
    public void setAddress(Address address) {
        this.address = address;
    }

    
    public AppInstances getAppInstance() {
        return this.appInstance;
    }
    
    public void setAppInstance(AppInstances appInstance) {
        this.appInstance = appInstance;
    }

    
    public PropertyType getType() {
        return this.type;
    }
    
    public void setType(PropertyType type) {
        this.type = type;
    }

    
    @Column(name="name", nullable=false, length=254)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="numberOfUnits", nullable=false)
    public int getNumberOfUnits() {
        return this.numberOfUnits;
    }
    
    public void setNumberOfUnits(int numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    
    @Column(name="size")
    public Integer getSize() {
        return this.size;
    }
    
    public void setSize(Integer size) {
        this.size = size;
    }

    
    @Column(name="description", length=254)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="imagePath", length=254)
    public String getImagePath() {
        return this.imagePath;
    }
    
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }




}


