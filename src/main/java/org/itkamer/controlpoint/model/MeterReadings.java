package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author clovisgakam
 * the MeterReadings 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class MeterReadings  extends CPBaseEntity implements Serializable {
	

     
     @ManyToOne
     private Unit unit;
     private Date start;
     private Date end;
     private BigDecimal water;
     private BigDecimal electricity;

    public MeterReadings() {
    }

  
   
    

    
    public Unit getUnit() {
        return this.unit;
    }
    
    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="start", nullable=false, length=19)
    public Date getStart() {
        return this.start;
    }
    
    public void setStart(Date start) {
        this.start = start;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="end", nullable=false, length=19)
    public Date getEnd() {
        return this.end;
    }
    
    public void setEnd(Date end) {
        this.end = end;
    }

    
    @Column(name="water", nullable=false)
    public BigDecimal getWater() {
        return this.water;
    }
    
    public void setWater(BigDecimal water) {
        this.water = water;
    }

    
    @Column(name="electricity", nullable=false)
    public BigDecimal getElectricity() {
        return this.electricity;
    }
    
    public void setElectricity(BigDecimal electricity) {
        this.electricity = electricity;
    }




}


