package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UnitSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<Unit> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private UnitSearchInput searchInput;

   public UnitSearchResult()
   {
      super();
   }

   public UnitSearchResult(Long count, List<Unit> resultList,
         UnitSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<Unit> getResultList()
   {
      return resultList;
   }

   public UnitSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<Unit> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(UnitSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
