package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MeterReadingsSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<MeterReadings> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private MeterReadingsSearchInput searchInput;

   public MeterReadingsSearchResult()
   {
      super();
   }

   public MeterReadingsSearchResult(Long count, List<MeterReadings> resultList,
         MeterReadingsSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<MeterReadings> getResultList()
   {
      return resultList;
   }

   public MeterReadingsSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<MeterReadings> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(MeterReadingsSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
