package org.itkamer.controlpoint.model;

public enum PaymentMode
{
	CASH, CHECK, CREDIT_CARD, DIRECT_DEPOSIT, ELECTRONIC_PAYMENT, MONEY_ORDER
}