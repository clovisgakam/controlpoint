package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaskCommentSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<TaskComment> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private TaskCommentSearchInput searchInput;

   public TaskCommentSearchResult()
   {
      super();
   }

   public TaskCommentSearchResult(Long count, List<TaskComment> resultList,
         TaskCommentSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<TaskComment> getResultList()
   {
      return resultList;
   }

   public TaskCommentSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<TaskComment> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(TaskCommentSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
