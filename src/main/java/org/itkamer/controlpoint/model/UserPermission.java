package org.itkamer.controlpoint.model;

public enum UserPermission
{
	VIEW, ADMIN,NONE
}