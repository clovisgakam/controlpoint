package org.itkamer.controlpoint.model;
// Generated Apr 27, 2015 11:41:14 AM by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author clovisgakam
 * The Payment 
 */
@Entity 
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Payments  extends CPBaseEntity implements Serializable {
	

     
     @ManyToOne
     private Quotations quotation;
     private String refNumber;
     private String heading;
     @Column
     @Enumerated(EnumType.STRING)
     private PaymentMode method;
     private Date date;
     private BigDecimal amount;
     private String details;

    public Payments() {
    }

    
    public Quotations getQuotation() {
        return this.quotation;
    }
    
    public void setQuotation(Quotations quotation) {
        this.quotation = quotation;
    }

    
    @Column(name="refNumber", nullable=false, length=254)
    public String getRefNumber() {
        return this.refNumber;
    }
    
    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    
    @Column(name="heading", nullable=false, length=254)
    public String getHeading() {
        return this.heading;
    }
    
    public void setHeading(String heading) {
        this.heading = heading;
    }

    
    public PaymentMode getMethod() {
        return this.method;
    }
    
    public void setMethod(PaymentMode method) {
        this.method = method;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date", nullable=false, length=19)
    public Date getDate() {
        return this.date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }

    
    @Column(name="amount", nullable=false)
    public BigDecimal getAmount() {
        return this.amount;
    }
    
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    
    @Column(name="details", length=254)
    public String getDetails() {
        return this.details;
    }
    
    public void setDetails(String details) {
        this.details = details;
    }




}


