package org.itkamer.controlpoint.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AttachedFileSearchResult
{

   /*
    * The number of entities matching this search.
    */
   private Long count;

   /*
    * The result list.
    */
   private List<AttachedFile> resultList;

   /*
    * The original search input object. For stateless clients.
    */
   private AttachedFileSearchInput searchInput;

   public AttachedFileSearchResult()
   {
      super();
   }

   public AttachedFileSearchResult(Long count, List<AttachedFile> resultList,
         AttachedFileSearchInput searchInput)
   {
      super();
      this.count = count;
      this.resultList = resultList;
      this.searchInput = searchInput;
   }

   public Long getCount()
   {
      return count;
   }

   public List<AttachedFile> getResultList()
   {
      return resultList;
   }

   public AttachedFileSearchInput getSearchInput()
   {
      return searchInput;
   }

   public void setCount(Long count)
   {
      this.count = count;
   }

   public void setResultList(List<AttachedFile> resultList)
   {
      this.resultList = resultList;
   }

   public void setSearchInput(AttachedFileSearchInput searchInput)
   {
      this.searchInput = searchInput;
   }

}
