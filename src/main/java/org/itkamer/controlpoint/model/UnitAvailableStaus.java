package org.itkamer.controlpoint.model;

public enum UnitAvailableStaus
{
	VACCANT, RESERVED, OCCUPIED
}