package org.itkamer.controlpoint.utils;

import java.lang.reflect.Field;
// TODO: Auto-generated Javadoc

/**
 * The Class PropertyReader.
 * 
 * @author clovisgakam
 */
public class PropertyReader {


	

	/**
	 * Gets the property value.
	 * 
	 * @param source
	 *            the source
	 * @param fieldName
	 *            the field name
	 * @return the property value
	 */
	public static Object getPropertyValue(Object source, String fieldName) {
		Object  value = null ;
		try {
			Field field = source.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			 value = field.get(source);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return value ;
		
	}
	
	/**
	 * Copy fields.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @throws Exception
	 *             the exception
	 */
	public static <T> void copyFields(T source, T target) throws Exception{
	    Class<?> clazz = source.getClass();

	    for (Field field : clazz.getFields()) {
	        Object value = field.get(source);
	        field.set(target, value);
	    }
	}

	

}
