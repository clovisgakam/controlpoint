package org.itkamer.controlpoint.utils;

import java.io.Serializable;
// TODO: Auto-generated Javadoc

/**
 * The Class Message.
 * message to display to the client side by angular-growl
 * @author clovisgakam 
 */
public class Message implements Serializable {
 
	
	/** The text. */
	private String text ;
	
	/** The severity. */
	private MessageSeverity severity ;

	/**
	 * Instantiates a new message.
	 */
	public Message() {
		
	}
	
	/**
	 * Instantiates a new message.
	 * 
	 * @param text
	 *            the text
	 * @param severity
	 *            the severity
	 */
	public Message(String text, MessageSeverity severity) {
		super();
		this.text = text;
		this.severity = severity;
	}

	/**
	 * Gets the text.
	 * 
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the text.
	 * 
	 * @param text
	 *            the new text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the severity.
	 * 
	 * @return the severity
	 */
	public MessageSeverity getSeverity() {
		return severity;
	}

	/**
	 * Sets the severity.
	 * 
	 * @param severity
	 *            the new severity
	 */
	public void setSeverity(MessageSeverity severity) {
		this.severity = severity;
	}
	
	
	
}
