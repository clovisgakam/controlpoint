package org.itkamer.controlpoint.utils;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.RandomStringUtils;
import org.itkamer.controlpoint.model.Tasks;
import org.itkamer.controlpoint.repository.TasksRepository;

// TODO: Auto-generated Javadoc
/**
 * @author clovisgakam
 * The Class TaskRefGenerator.
 */
@javax.inject.Singleton
public class TaskRefGenerator {

	/** The repository. */
	@Inject
	private TasksRepository repository ;


	/**
	 * New sending number.
	 * 
	 * @return the string
	 */
	public String newRefNumber(){
		String newRefNumber = RandomStringUtils.randomNumeric(10);
		List<Tasks> tasks = repository.findByRefNumber(newRefNumber).maxResults(1).getResultList();
		if(tasks.isEmpty())
			return newRefNumber;
		return newRefNumber();
	}
}
