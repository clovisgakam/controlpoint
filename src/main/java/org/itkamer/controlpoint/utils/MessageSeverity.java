package org.itkamer.controlpoint.utils;

// TODO: Auto-generated Javadoc
/**
 * @author clovisgakam
 * The Enum MessageSeverity.
 */
public enum MessageSeverity {
	
	/** The warn. */
	warn,
/** The info. */
info,
/** The error. */
error,
/** The success. */
success
}
