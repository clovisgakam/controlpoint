package org.itkamer.controlpoint.utils;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * @author clovisgakam 
 * The Class MessageBuilder.
 */
public class MessageBuilder implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The message holder. */
	private MessageHolder messageHolder ;

	/**
	 * Instantiates a new message builder.
	 */
	public MessageBuilder() {
		messageHolder = new MessageHolder() ;
	}
	
	/**
	 * Creates the.
	 * 
	 * @return the message builder
	 */
	public MessageBuilder create(){
		messageHolder = new MessageHolder() ;
		return this ;
	}

	/**
	 * Adds the message.
	 * 
	 * @param message
	 *            the message
	 * @return the message builder
	 */
	public MessageBuilder addMessage(Message message){
		messageHolder.getMessages().add(message);
		return this ;
	}

	/**
	 * Builds the.
	 * 
	 * @return the message holder
	 */
	public MessageHolder build(){
		return messageHolder ;
	}

}
