/**
 * 
 */
package org.itkamer.controlpoint.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class Contract.
 * 
 * @author clovis gakam
 */
public class Contract {
	
	/**
	 * Assert not null.
	 * 
	 * @param message
	 *            the message
	 * @param object
	 *            the object
	 */
	public static void assertNotNull(String message,Object object){
		if(object == null){
			throw new NullPointerException(message); 
		}
	}

	/**
	 * Assert not null.
	 * 
	 * @param object
	 *            the object
	 */
	public static void assertNotNull(Object object){
		if(object == null){
			throw new NullPointerException(); 
		}
	}
	
	/**
	 * Assert true.
	 * 
	 * @param expression
	 *            the expression
	 */
	public static void assertTrue(boolean expression){
		if(expression == false){
			throw new IllegalArgumentException("expression if false");
		}
	}
}
