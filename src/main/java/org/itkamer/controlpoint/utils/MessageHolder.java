package org.itkamer.controlpoint.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
// TODO: Auto-generated Javadoc
/** 
* @author clovisgakam
*hold the list of  messages to display to the client by angular-growl 
*/
public class MessageHolder implements Serializable{
	
	/** The messages. */
	private List<Message> messages = new ArrayList<Message>();

	/**
	 * Gets the messages.
	 * 
	 * @return the messages
	 */
	public List<Message> getMessages() {
		return messages;
	}

	/**
	 * Sets the messages.
	 * 
	 * @param messages
	 *            the new messages
	 */
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	

}
