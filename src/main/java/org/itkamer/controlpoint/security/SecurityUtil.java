package org.itkamer.controlpoint.security;

import java.security.Principal;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.itkamer.controlpoint.model.AppInstances;
import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.RoleNames;
import org.itkamer.controlpoint.repository.LoginsRepository;

/**
 * The Class SecurityUtil.
 * @author clovisgakam
 */
@Stateless
public class SecurityUtil {

	/** The session context. */
	@Resource
	private SessionContext sessionContext;

	/** The login endpoint. */
	@Inject
	private LoginsRepository loginRepository;


	/**
	 * Gets the user email.
	 * 
	 * @return the user email
	 */
	public String getUserEmail() {
		Principal callerPrincipal = sessionContext.getCallerPrincipal();
		if(callerPrincipal==null) return "System";
		return callerPrincipal.getName();
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	public String getUserName() {
		if(hasConnectedUser()){
			Logins user = getConnectedUser();
			return user.getFirstName()+" "+user.getLastName() ;
			
		}
		return "System";
	}

	/**
	 * Gets the connected user.
	 * @return the connected user
	 */
	public Logins getConnectedUser() {
		String email = getUserEmail();
		if(email==null) throw new IllegalStateException("No user connected");
		List<Logins> resultList = loginRepository.findByEmail(email).maxResults(1).getResultList();
		if(resultList.isEmpty()) throw new IllegalStateException("user with email not found: " + email);
		return resultList.iterator().next();
	}

	public boolean hasConnectedUser() {
		String userMail = getUserEmail();
		if(userMail==null) return false;
		List<Logins> resultList = loginRepository.findByEmail(userMail).maxResults(1).getResultList();
		if(resultList.isEmpty()) return false;
		return true;
	}

	public AppInstances getInstances(){
		Logins login = getConnectedUser();
		if(login!=null) return login.getAppInstance() ;
		return null ;
	}

	public boolean hasRole(RoleNames roleName){
		Logins login = getConnectedUser();
	//	if(login!=null) return login.getRoleNames().contains(roleName);
		return false ;
	}
	
	


}
