package org.itkamer.controlpoint.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.Propertys;
import org.itkamer.controlpoint.model.UserRole;

public class PropertyData {
	
	private Propertys property ;
	
	private List<UserRole> users = new ArrayList<UserRole>();
	
	private BigDecimal incommes = BigDecimal.ZERO ;
	
	private BigDecimal expenses = BigDecimal.ZERO ;
	
	private BigDecimal overalScore = BigDecimal.ZERO ;
	
	private BigInteger openedTask = BigInteger.ZERO ;
	
	private BigInteger closedTask = BigInteger.ZERO ;
	
	private BigInteger criticalTask = BigInteger.ZERO ;
	
	private BigInteger overdueTask = BigInteger.ZERO ;

	public Propertys getProperty() {
		return property;
	}

	public void setProperty(Propertys property) {
		this.property = property;
	}

	

	
	public List<UserRole> getUsers() {
		return users;
	}

	public void setUsers(List<UserRole> users) {
		this.users = users;
	}

	public BigDecimal getIncommes() {
		return incommes;
	}

	public void setIncommes(BigDecimal incommes) {
		this.incommes = incommes;
	}

	public BigDecimal getExpenses() {
		return expenses;
	}

	public void setExpenses(BigDecimal expenses) {
		this.expenses = expenses;
	}

	public BigDecimal getOveralScore() {
		return overalScore;
	}

	public void setOveralScore(BigDecimal overalScore) {
		this.overalScore = overalScore;
	}

	public BigInteger getOpenedTask() {
		return openedTask;
	}

	public void setOpenedTask(BigInteger openedTask) {
		this.openedTask = openedTask;
	}

	public BigInteger getClosedTask() {
		return closedTask;
	}

	public void setClosedTask(BigInteger closedTask) {
		this.closedTask = closedTask;
	}

	public BigInteger getCriticalTask() {
		return criticalTask;
	}

	public void setCriticalTask(BigInteger criticalTask) {
		this.criticalTask = criticalTask;
	}

	public BigInteger getOverdueTask() {
		return overdueTask;
	}

	public void setOverdueTask(BigInteger overdueTask) {
		this.overdueTask = overdueTask;
	}
	
	
	

}
