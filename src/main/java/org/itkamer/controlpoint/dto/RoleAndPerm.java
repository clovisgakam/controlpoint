package org.itkamer.controlpoint.dto;

import org.itkamer.controlpoint.model.RoleNames;
import org.itkamer.controlpoint.model.UserPermission;

public class RoleAndPerm {
	private RoleNames role  ;
	private UserPermission perm ;
	public RoleAndPerm() {
	}
	public RoleAndPerm(RoleNames role, UserPermission perm) {
		super();
		this.role = role;
		this.perm = perm;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((perm == null) ? 0 : perm.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleAndPerm other = (RoleAndPerm) obj;
		if (perm != other.perm)
			return false;
		if (role != other.role)
			return false;
		return true;
	}
	public RoleNames getRole() {
		return role;
	}
	public void setRole(RoleNames role) {
		this.role = role;
	}
	public UserPermission getPerm() {
		return perm;
	}
	public void setPerm(UserPermission perm) {
		this.perm = perm;
	}



}
