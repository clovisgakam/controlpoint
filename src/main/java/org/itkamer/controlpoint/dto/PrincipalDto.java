package org.itkamer.controlpoint.dto;

import java.util.ArrayList;
import java.util.List;

import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.RoleNames;
import org.itkamer.controlpoint.model.UserPermission;
import org.itkamer.controlpoint.model.UserRole;


/**
 * 
 * @author clovisgakam
 * this class represent the connected user in the front end
 */
public class PrincipalDto {
	private Long UserId = null ;

	private String username ;

	private List<RoleAndPerm> roleAndPerms = new ArrayList<RoleAndPerm>();

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public PrincipalDto(Logins login,List<UserRole> userRoles) {
		this.username = login.getFirstName();
		this.roleAndPerms = roleAndPermFromUserRole(userRoles);
		if(login.getAdmin()){
			roleAndPerms.add(new RoleAndPerm(RoleNames.ADMINISTRATOR, UserPermission.ADMIN));
		}
		if(login.getRegistered()){
			roleAndPerms.add(new RoleAndPerm(RoleNames.PROPERTY_MANAGER, UserPermission.ADMIN));
		}

		this.UserId = login.getId();
	}

	public Long getUserId() {
		return UserId;
	}

	public void setUserId(Long userId) {
		UserId = userId;
	}

	public List<RoleAndPerm> getRoleAndPerms() {
		return roleAndPerms;
	}

	public void setRoleAndPerms(List<RoleAndPerm> roleAndPerm) {
		this.roleAndPerms = roleAndPerm;
	}

	private List<RoleAndPerm> roleAndPermFromUserRole(List<UserRole> userRoles){
		List<RoleAndPerm> roleAndPerm = new ArrayList<RoleAndPerm>();
		for (UserRole userRole : userRoles) {
			RoleAndPerm rap = new RoleAndPerm(userRole.getRole(), userRole.getPermission());
			if(!roleAndPerm.contains(rap))
				roleAndPerm.add(rap);
		}
		return roleAndPerm;
	}



}
