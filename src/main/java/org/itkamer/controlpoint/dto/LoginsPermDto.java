package org.itkamer.controlpoint.dto;

import java.util.HashMap;

import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.Propertys;
import org.itkamer.controlpoint.model.RoleNames;
import org.itkamer.controlpoint.model.UserPermission;

public class LoginsPermDto {
	private UserPermission perm;
	private Logins login;
	private Propertys property;
	
	public LoginsPermDto() {
	}

	public LoginsPermDto(UserPermission perm,
			Logins login, Propertys property) {
		super();
		this.perm = perm;																																						
		this.login = login;
		this.property = property;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((perm == null) ? 0 : perm.hashCode());
		result = prime * result
				+ ((property == null) ? 0 : property.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoginsPermDto other = (LoginsPermDto) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (perm != other.perm)
			return false;
		if (property == null) {
			if (other.property != null)
				return false;
		} else if (!property.equals(other.property))
			return false;
		return true;
	}

	public UserPermission getPerm() {
		return perm;
	}

	public void setPerm(UserPermission perm) {
		this.perm = perm;
	}

	public Logins getLogin() {
		return login;
	}

	public void setLogin(Logins login) {
		this.login = login;
	}

	public Propertys getProperty() {
		return property;
	}

	public void setProperty(Propertys property) {
		this.property = property;
	}

}
