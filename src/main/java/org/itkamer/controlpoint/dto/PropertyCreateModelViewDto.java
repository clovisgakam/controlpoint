package org.itkamer.controlpoint.dto;


import java.util.ArrayList;
import java.util.List;

import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.Propertys;
import org.itkamer.controlpoint.model.UserRole;

public class PropertyCreateModelViewDto {

	public Propertys property;
	
	public List<UserRole> users = new ArrayList<UserRole>() ;
	

	public Propertys getProperty() {
		return property;
	}

	public void setProperty(Propertys property) {
		this.property = property;
	}

	public List<UserRole> getUsers() {
		return users;
	}

	public void setUsers(List<UserRole> users) {
		this.users = users;
	}

	
}
