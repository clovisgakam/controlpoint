package org.itkamer.controlpoint.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.itkamer.controlpoint.model.AppInstances;

public class PortefolioModelViewDto {

	public int propertyNb = 0;
	
	public AppInstances instance = new AppInstances();

	public BigDecimal incommes = BigDecimal.ZERO ;

	public BigDecimal expenses = BigDecimal.ZERO ;

	public BigDecimal oweralScore = BigDecimal.ZERO ;

	// tasks status statistics
	public BigInteger openedTasks = BigInteger.ONE;

	public BigInteger closedTask = BigInteger.ZERO;

	public BigInteger criticalTasks = BigInteger.ZERO;

	public BigInteger overdueTasks = BigInteger.ZERO;

	// scoring 

	public String higestScoringProperty = "";

	public String lowestScoringProperty = "";

	public String higestScoringCaretaker = "";

	public String lowestScoringCaretaker = "";
	
	//averrage and valuable
	
	public BigDecimal aveMontlyIncome = BigDecimal.ZERO ;

	public BigDecimal aveMontlyExpence = BigDecimal.ZERO ;
	
	public String mostValuableProperty = "";

	public String leastValuableProperty = "";

	public List<PropertyData> propertyDatas = new ArrayList<PropertyData>();

	public int getPropertyNb() {
		return propertyNb;
	}

	public void setPropertyNb(int propertyNb) {
		this.propertyNb = propertyNb;

	}

	public PortefolioModelViewDto(List<PropertyData> propertyDatas, AppInstances appInstances) {
		super();
		this.propertyDatas = propertyDatas;
		this.propertyNb = propertyDatas.size();
		this.instance = appInstances;
		computeAccoutingAndTaskStatus();
		evaluateScoring();
	}

	private void computeAccoutingAndTaskStatus(){
		for (PropertyData data : propertyDatas) {
			this.incommes = this.incommes.add(data.getIncommes());
			this.expenses = this.expenses.add(data.getExpenses());
			this.oweralScore = this.oweralScore.add(data.getOveralScore());
			this.closedTask = this.closedTask.add(data.getClosedTask());
			this.openedTasks = this.openedTasks.and(data.getOpenedTask()) ;
			this.criticalTasks = this.criticalTasks.add(data.getCriticalTask());
			this.overdueTasks = this.overdueTasks.add(data.getOverdueTask());

		}

		this.oweralScore = this.oweralScore.divide(propertyDatas.size()!=0?BigDecimal.valueOf(propertyDatas.size()):BigDecimal.valueOf(1));
	}

	private void evaluateScoring(){
		//TODO :
		//
	}

	public BigDecimal getIncommes() {
		return incommes;
	}

	public void setIncommes(BigDecimal incommes) {
		this.incommes = incommes;
	}

	public BigDecimal getExpenses() {
		return expenses;
	}

	public void setExpenses(BigDecimal expenses) {
		this.expenses = expenses;
	}

	public BigDecimal getOweralScore() {
		return oweralScore;
	}

	public void setOweralScore(BigDecimal oweralScore) {
		this.oweralScore = oweralScore;
	}

	public BigInteger getOpenedTasks() {
		return openedTasks;
	}

	public void setOpenedTasks(BigInteger openedTasks) {
		this.openedTasks = openedTasks;
	}

	public BigInteger getClosedTask() {
		return closedTask;
	}

	public void setClosedTask(BigInteger closedTask) {
		this.closedTask = closedTask;
	}

	public BigInteger getCriticalTasks() {
		return criticalTasks;
	}

	public void setCriticalTasks(BigInteger criticalTasks) {
		this.criticalTasks = criticalTasks;
	}

	public BigInteger getOverdueTasks() {
		return overdueTasks;
	}

	public void setOverdueTasks(BigInteger overdueTasks) {
		this.overdueTasks = overdueTasks;
	}

	public String getHigestScoringProperty() {
		return higestScoringProperty;
	}

	public void setHigestScoringProperty(String higestScoringProperty) {
		this.higestScoringProperty = higestScoringProperty;
	}

	public String getLowestScoringProperty() {
		return lowestScoringProperty;
	}

	public void setLowestScoringProperty(String lowestScoringProperty) {
		this.lowestScoringProperty = lowestScoringProperty;
	}

	public String getHigestScoringCaretaker() {
		return higestScoringCaretaker;
	}

	public void setHigestScoringCaretaker(String higestScoringCaretaker) {
		this.higestScoringCaretaker = higestScoringCaretaker;
	}

	public String getLowestScoringCaretaker() {
		return lowestScoringCaretaker;
	}

	public void setLowestScoringCaretaker(String lowestScoringCaretaker) {
		this.lowestScoringCaretaker = lowestScoringCaretaker;
	}

	public List<PropertyData> getPropertyDatas() {
		return propertyDatas;
	}

	public void setPropertyDatas(List<PropertyData> propertyDatas) {
		this.propertyDatas = propertyDatas;
	}

	public AppInstances getInstance() {
		return instance;
	}

	public void setInstance(AppInstances instance) {
		this.instance = instance;
	}

	public BigDecimal getAveMontlyIncome() {
		return aveMontlyIncome;
	}

	public void setAveMontlyIncome(BigDecimal aveMontlyIncome) {
		this.aveMontlyIncome = aveMontlyIncome;
	}

	public BigDecimal getAveMontlyExpence() {
		return aveMontlyExpence;
	}

	public void setAveMontlyExpence(BigDecimal aveMontlyExpence) {
		this.aveMontlyExpence = aveMontlyExpence;
	}

	public String getMostValuableProperty() {
		return mostValuableProperty;
	}

	public void setMostValuableProperty(String mostValuableProperty) {
		this.mostValuableProperty = mostValuableProperty;
	}

	public String getLeastValuableProperty() {
		return leastValuableProperty;
	}

	public void setLeastValuableProperty(String leastValuableProperty) {
		this.leastValuableProperty = leastValuableProperty;
	}



}
