package org.itkamer.controlpoint.exceptions;

import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.utils.Message;
import org.itkamer.controlpoint.utils.MessageBuilder;
import org.itkamer.controlpoint.utils.MessageHolder;
import org.itkamer.controlpoint.utils.MessageSeverity;

// TODO: Auto-generated Javadoc

/**
 * The Class CPExeception.
 * 
 * @author clovisgakam
 */
public class CPExeception  extends  Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2480710935516475930L;
	
	/** The http status. */
	private int httpStatus = Status.INTERNAL_SERVER_ERROR.getStatusCode(); 

	/** The severity. */
	private MessageSeverity severity = MessageSeverity.error;
	
	/**
	 * Gets the message holder.
	 * 
	 * @return the message holder
	 */
	public MessageHolder getMessageHolder() {
		return	new MessageBuilder().create().addMessage(new Message(getMessage(), severity)).build();
	}
	
	/**
	 * Instantiates a new Cp exeception.
	 * 
	 * @param message
	 *            the message
	 */
	public CPExeception(String message) {
		super(message);
	}
	
	/**
	 * Instantiates a new Cp exeception.
	 * 
	 * @param message
	 *            the message
	 * @param severity
	 *            the severity
	 */
	public CPExeception(String message, MessageSeverity severity) {
		super(message);
		this.severity = severity;
	}

	/**
	 * Gets the http status.
	 * 
	 * @return the http status
	 */
	public int getHttpStatus() {
		return httpStatus;
	}

	/**
	 * Sets the http status.
	 * 
	 * @param httpStatus
	 *            the new http status
	 */
	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}
}
