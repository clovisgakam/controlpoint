package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.model.Vendors;
import org.itkamer.controlpoint.model.VendorsSearchInput;
import org.itkamer.controlpoint.model.VendorsSearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/vendors")
public class VendorsEndpoint
{

   @Inject
   private VendorsEJB ejb;

   @Inject
   private AddressMerger addressMerger;

   @Inject
   private CompanyMerger companyMerger;

   @Inject
   private VendorCategorysMerger vendorCategorysMerger;

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public Vendors create(Vendors entity)
   {
      return ejb.create(entity);
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      Vendors deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public Vendors update(Vendors entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      Vendors found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(found).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public VendorsSearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<Vendors> resultList = ejb.listAll(start, max);
      VendorsSearchInput searchInput = new VendorsSearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new VendorsSearchResult((long) resultList.size(),
           resultList, searchInput);
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }

//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public VendorsSearchResult findBy(VendorsSearchInput searchInput)
//   {
//      SingularAttribute<Vendors, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<Vendors> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new VendorsSearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(VendorsSearchInput searchInput)
//   {
//      SingularAttribute<Vendors, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public VendorsSearchResult findByLike(VendorsSearchInput searchInput)
//   {
//      SingularAttribute<Vendors, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<Vendors> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new VendorsSearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(VendorsSearchInput searchInput)
//   {
//      SingularAttribute<Vendors, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<Vendors, ?>[] readSeachAttributes(
//         VendorsSearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<Vendors, ?>> result = new ArrayList<SingularAttribute<Vendors, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = Vendors_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<Vendors, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private static final List<String> companyFields = emptyList;

   private static final List<String> addressFields = emptyList;

   private static final List<String> categoryFields = emptyList;

   private Vendors detach(Vendors entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setCompany(companyMerger.unbind(entity.getCompany(), companyFields));

      // aggregated
      entity.setAddress(addressMerger.unbind(entity.getAddress(), addressFields));

      // aggregated
      entity.setCategory(vendorCategorysMerger.unbind(entity.getCategory(), categoryFields));

      return entity;
   }

   private List<Vendors> detach(List<Vendors> list)
   {
      if (list == null)
         return list;
      List<Vendors> result = new ArrayList<Vendors>();
      for (Vendors entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private VendorsSearchInput detach(VendorsSearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}