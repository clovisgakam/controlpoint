package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.model.TaskComment;
import org.itkamer.controlpoint.model.TaskCommentSearchInput;
import org.itkamer.controlpoint.model.TaskCommentSearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/taskcomments")
public class TaskCommentEndpoint
{

   @Inject
   private TaskCommentEJB ejb;

   @Inject
   private TasksMerger tasksMerger;

   @Inject
   private LoginsMerger loginsMerger;

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public TaskComment create(TaskComment entity)
   {
      return detach(ejb.create(entity));
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      TaskComment deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public TaskComment update(TaskComment entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      TaskComment found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(detach(found)).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public TaskCommentSearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<TaskComment> resultList = ejb.listAll(start, max);
      TaskCommentSearchInput searchInput = new TaskCommentSearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new TaskCommentSearchResult((long) resultList.size(),
            detach(resultList), detach(searchInput));
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }

//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public TaskCommentSearchResult findBy(TaskCommentSearchInput searchInput)
//   {
//      SingularAttribute<TaskComment, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<TaskComment> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new TaskCommentSearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(TaskCommentSearchInput searchInput)
//   {
//      SingularAttribute<TaskComment, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public TaskCommentSearchResult findByLike(TaskCommentSearchInput searchInput)
//   {
//      SingularAttribute<TaskComment, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<TaskComment> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new TaskCommentSearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(TaskCommentSearchInput searchInput)
//   {
//      SingularAttribute<TaskComment, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<TaskComment, ?>[] readSeachAttributes(
//         TaskCommentSearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<TaskComment, ?>> result = new ArrayList<SingularAttribute<TaskComment, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = TaskComment_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<TaskComment, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private static final List<String> taskFields = emptyList;

   private static final List<String> loginFields = emptyList;

   private TaskComment detach(TaskComment entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setTask(tasksMerger.unbind(entity.getTask(), taskFields));

      // aggregated
      entity.setLogin(loginsMerger.unbind(entity.getLogin(), loginFields));

      return entity;
   }

   private List<TaskComment> detach(List<TaskComment> list)
   {
      if (list == null)
         return list;
      List<TaskComment> result = new ArrayList<TaskComment>();
      for (TaskComment entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private TaskCommentSearchInput detach(TaskCommentSearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}