package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.PropertyType;
import org.itkamer.controlpoint.repository.PropertyTypeRepository;

public class PropertyTypeMerger
{

   @Inject
   private PropertyTypeRepository repository;

   public PropertyType bindComposed(PropertyType entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public PropertyType bindAggregated(PropertyType entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<PropertyType> entities)
   {
      if (entities == null)
         return;
      HashSet<PropertyType> oldCol = new HashSet<PropertyType>(entities);
      entities.clear();
      for (PropertyType entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<PropertyType> entities)
   {
      if (entities == null)
         return;
      HashSet<PropertyType> oldCol = new HashSet<PropertyType>(entities);
      entities.clear();
      for (PropertyType entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public PropertyType unbind(final PropertyType entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      PropertyType newEntity = new PropertyType();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<PropertyType> unbind(final Set<PropertyType> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<PropertyType>();
      //       HashSet<PropertyType> cache = new HashSet<PropertyType>(entities);
      //       entities.clear();
      //       for (PropertyType entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
