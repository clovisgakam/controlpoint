package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.MeterReadings;
import org.itkamer.controlpoint.repository.MeterReadingsRepository;

public class MeterReadingsMerger
{

   @Inject
   private MeterReadingsRepository repository;

   public MeterReadings bindComposed(MeterReadings entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public MeterReadings bindAggregated(MeterReadings entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<MeterReadings> entities)
   {
      if (entities == null)
         return;
      HashSet<MeterReadings> oldCol = new HashSet<MeterReadings>(entities);
      entities.clear();
      for (MeterReadings entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<MeterReadings> entities)
   {
      if (entities == null)
         return;
      HashSet<MeterReadings> oldCol = new HashSet<MeterReadings>(entities);
      entities.clear();
      for (MeterReadings entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public MeterReadings unbind(final MeterReadings entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      MeterReadings newEntity = new MeterReadings();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<MeterReadings> unbind(final Set<MeterReadings> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<MeterReadings>();
      //       HashSet<MeterReadings> cache = new HashSet<MeterReadings>(entities);
      //       entities.clear();
      //       for (MeterReadings entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
