package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.model.Address;
import org.itkamer.controlpoint.model.AddressSearchInput;
import org.itkamer.controlpoint.model.AddressSearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/addresss")
public class AddressEndpoint
{

   @Inject
   private AddressEJB ejb;

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public Address create(Address entity)
   {
      return detach(ejb.create(entity));
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      Address deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public Address update(Address entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      Address found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(detach(found)).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public AddressSearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<Address> resultList = ejb.listAll(start, max);
      AddressSearchInput searchInput = new AddressSearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new AddressSearchResult((long) resultList.size(),
            detach(resultList), detach(searchInput));
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }

//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public AddressSearchResult findBy(AddressSearchInput searchInput)
//   {
//      SingularAttribute<Address, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<Address> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new AddressSearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(AddressSearchInput searchInput)
//   {
//      SingularAttribute<Address, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public AddressSearchResult findByLike(AddressSearchInput searchInput)
//   {
//      SingularAttribute<Address, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<Address> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new AddressSearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(AddressSearchInput searchInput)
//   {
//      SingularAttribute<Address, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<Address, ?>[] readSeachAttributes(
//         AddressSearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<Address, ?>> result = new ArrayList<SingularAttribute<Address, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = Address_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<Address, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private Address detach(Address entity)
   {
      if (entity == null)
         return null;

      return entity;
   }

   private List<Address> detach(List<Address> list)
   {
      if (list == null)
         return list;
      List<Address> result = new ArrayList<Address>();
      for (Address entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private AddressSearchInput detach(AddressSearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}