package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.Address;
import org.itkamer.controlpoint.repository.AddressRepository;

public class AddressMerger
{

   @Inject
   private AddressRepository repository;

   public Address bindComposed(Address entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public Address bindAggregated(Address entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<Address> entities)
   {
      if (entities == null)
         return;
      HashSet<Address> oldCol = new HashSet<Address>(entities);
      entities.clear();
      for (Address entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<Address> entities)
   {
      if (entities == null)
         return;
      HashSet<Address> oldCol = new HashSet<Address>(entities);
      entities.clear();
      for (Address entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public Address unbind(final Address entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      Address newEntity = new Address();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<Address> unbind(final Set<Address> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<Address>();
      //       HashSet<Address> cache = new HashSet<Address>(entities);
      //       entities.clear();
      //       for (Address entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
