package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.Tasks;
import org.itkamer.controlpoint.repository.TasksRepository;

public class TasksMerger
{

   @Inject
   private TasksRepository repository;

   public Tasks bindComposed(Tasks entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public Tasks bindAggregated(Tasks entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<Tasks> entities)
   {
      if (entities == null)
         return;
      HashSet<Tasks> oldCol = new HashSet<Tasks>(entities);
      entities.clear();
      for (Tasks entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<Tasks> entities)
   {
      if (entities == null)
         return;
      HashSet<Tasks> oldCol = new HashSet<Tasks>(entities);
      entities.clear();
      for (Tasks entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public Tasks unbind(final Tasks entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      Tasks newEntity = new Tasks();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<Tasks> unbind(final Set<Tasks> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<Tasks>();
      //       HashSet<Tasks> cache = new HashSet<Tasks>(entities);
      //       entities.clear();
      //       for (Tasks entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
