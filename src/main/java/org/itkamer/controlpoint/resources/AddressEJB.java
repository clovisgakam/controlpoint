package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.Address;
import org.itkamer.controlpoint.repository.AddressRepository;

@Stateless
public class AddressEJB
{

   @Inject
   private AddressRepository repository;

   public Address create(Address entity)
   {
      return repository.save(attach(entity));
   }

   public Address deleteById(Long id)
   {
      Address entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public Address update(Address entity)
   {
      return repository.save(attach(entity));
   }

   public Address findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<Address> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<Address> findBy(Address entity, int start, int max, SingularAttribute<Address, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(Address entity, SingularAttribute<Address, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<Address> findByLike(Address entity, int start, int max, SingularAttribute<Address, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(Address entity, SingularAttribute<Address, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private Address attach(Address entity)
   {
      if (entity == null)
         return null;

      return entity;
   }
}
