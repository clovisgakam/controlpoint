package org.itkamer.controlpoint.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.dto.PortefolioModelViewDto;
import org.itkamer.controlpoint.dto.PropertyCreateModelViewDto;
import org.itkamer.controlpoint.dto.PropertyData;
import org.itkamer.controlpoint.model.AppInstances;
import org.itkamer.controlpoint.model.LoginStatus;
import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.Propertys;
import org.itkamer.controlpoint.model.UserRole;
import org.itkamer.controlpoint.repository.PropertysRepository;
import org.itkamer.controlpoint.security.SecurityUtil;

@Stateless
public class PropertysEJB
{

	@Inject
	private PropertysRepository repository;

	@Inject
	private AppInstancesMerger appInstancesMerger;

	@Inject
	private PropertyTypeMerger propertyTypeMerger;

	@Inject
	private AddressMerger addressMerger;

	@Inject
	private SecurityUtil securityUtil ;

	@Inject
	private UserRoleEJB userRoleEJB ;

	public Propertys create(Propertys entity)
	{
		//	   Property entity = data.getProperty();
		//		List<Login> users = data.getUsers();
		//		String instanceId = securityUtil.getInstanceId();
		//		String userName = securityUtil.getUserName();
		//		entity = attach(entity);
		//		entity.setInstanceId(instanceId);
		//		entity.setCreateBy(userName);
		Propertys saved = repository.save(entity);

		//		for (Login login : users) {
		//			PropertyLogin propertyLogin = new PropertyLogin(saved, login);
		//			propertyLogin.setInstanceId(instanceId);
		//			propertyLogin.setCreateBy(userName);
		//			propertyLoginEJB.create(propertyLogin);
		//
		//		}
		//		for (Trustees trustee : trustees) {
		//			PropertyTrustees propertyTrustee = new PropertyTrustees(saved, trustee);
		//			propertyTrustee.setInstanceId(instanceId);
		//			propertyTrustee.setCreateBy(userName);
		//			propertyTrusteesEJB.create(propertyTrustee);
		//
		//		}
		return saved ;
	}

	public Propertys newProperty(PropertyCreateModelViewDto data){
		Propertys entity = data.getProperty();
		List<UserRole> users = data.getUsers();
		Logins login = securityUtil.getConnectedUser();
		entity.setAppInstance(login.getAppInstance());
		entity = repository.save(entity);

		for (UserRole ur : users) {
			ur.setProperty(entity);
			userRoleEJB.create(ur);
		}
		return entity ;
	}
	
	public Propertys removeProperty(Propertys propertys){
		List<UserRole> userRoles = userRoleEJB.findUserRoleByProperty(propertys);
		for (UserRole userRole : userRoles) {
			userRoleEJB.deleteById(userRole.getId());
		}
		repository.remove(propertys);
		return propertys;
	}

	/**
	 * @author clovisgakam
	 * provide data for portefolio view
	 * @return
	 */
	public PortefolioModelViewDto getPropertyPortefolio(){
		Logins login = securityUtil.getConnectedUser();
		List<Propertys> properties = new ArrayList<Propertys>();
		List<PropertyData> propertyDatas = new ArrayList<PropertyData>();
		if(login.getRegistered()){
			properties = repository.findPropertyByAppInstance(login.getAppInstance()).orderAsc("name").getResultList();
		}else {
			properties = repository.findPropertyByUser(login, LoginStatus.ACTIVE, new Date()).orderAsc("property.name").getResultList();
			
		}

		for (Propertys property : properties) {
			propertyDatas.add(computeViewData(property));
		}
		PortefolioModelViewDto portefolio = new PortefolioModelViewDto(propertyDatas,login.getAppInstance());
		return portefolio ;
	}

	public PropertyData computeViewData(Propertys property){
		PropertyData propertyData = new PropertyData();
		List<UserRole> userRoles = userRoleEJB.findActiveUserRoleByProperty(property) ;
		propertyData.setUsers(userRoles);
		propertyData.setProperty(property);

		//TODO :
		//1- calculate incomme 
		//2- calculate expense
		//3- calculate overall score
		//4- calculate opened task nbr
		//5- calculate closed task nbr
		//6- calculate critical task nbr
		//7- calculate overdue task nbr 

		return propertyData ;
	}

	public Propertys deleteById(Long id)
	{
		Propertys entity = repository.findBy(id);
		if (entity != null)
		{
			removeProperty(entity);
		}
		return entity;
	}

	public Propertys update(Propertys entity)
	{
		return repository.save(attach(entity));
	}

	public Propertys findById(Long id)
	{
		return repository.findBy(id);
	}

	public List<Propertys> listAll(int start, int max)
	{
		return repository.findAll(start, max);
	}

	public Long count()
	{
		return repository.count();
	}

	public List<Propertys> findBy(Propertys entity, int start, int max, SingularAttribute<Propertys, ?>[] attributes)
	{
		return repository.findBy(entity, start, max, attributes);
	}

	public Long countBy(Propertys entity, SingularAttribute<Propertys, ?>[] attributes)
	{
		return repository.count(entity, attributes);
	}

	public List<Propertys> findByLike(Propertys entity, int start, int max, SingularAttribute<Propertys, ?>[] attributes)
	{
		return repository.findByLike(entity, start, max, attributes);
	}

	public Long countByLike(Propertys entity, SingularAttribute<Propertys, ?>[] attributes)
	{
		return repository.countLike(entity, attributes);
	}

	private Propertys attach(Propertys entity)
	{
		if (entity == null)
			return null;

		// aggregated
		entity.setAddress(addressMerger.bindAggregated(entity.getAddress()));

		// aggregated
		entity.setAppInstance(appInstancesMerger.bindAggregated(entity.getAppInstance()));

		// aggregated
		entity.setType(propertyTypeMerger.bindAggregated(entity.getType()));

		return entity;
	}
}
