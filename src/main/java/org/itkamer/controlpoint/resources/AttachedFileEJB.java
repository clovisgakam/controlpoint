package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.AttachedFile;
import org.itkamer.controlpoint.repository.AttachedFileRepository;

@Stateless
public class AttachedFileEJB
{

   @Inject
   private AttachedFileRepository repository;

   @Inject
   private TasksMerger tasksMerger;

   public AttachedFile create(AttachedFile entity)
   {
      return repository.save(attach(entity));
   }

   public AttachedFile deleteById(Long id)
   {
      AttachedFile entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public AttachedFile update(AttachedFile entity)
   {
      return repository.save(attach(entity));
   }

   public AttachedFile findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<AttachedFile> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<AttachedFile> findBy(AttachedFile entity, int start, int max, SingularAttribute<AttachedFile, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(AttachedFile entity, SingularAttribute<AttachedFile, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<AttachedFile> findByLike(AttachedFile entity, int start, int max, SingularAttribute<AttachedFile, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(AttachedFile entity, SingularAttribute<AttachedFile, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private AttachedFile attach(AttachedFile entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setTask(tasksMerger.bindAggregated(entity.getTask()));

      return entity;
   }
}
