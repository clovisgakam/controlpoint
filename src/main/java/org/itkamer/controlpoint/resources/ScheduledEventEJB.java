package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.ScheduledEvent;
import org.itkamer.controlpoint.repository.ScheduledEventRepository;

@Stateless
public class ScheduledEventEJB
{

   @Inject
   private ScheduledEventRepository repository;

   @Inject
   private PropertysMerger propertysMerger;

   public ScheduledEvent create(ScheduledEvent entity)
   {
      return repository.save(attach(entity));
   }

   public ScheduledEvent deleteById(Long id)
   {
      ScheduledEvent entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public ScheduledEvent update(ScheduledEvent entity)
   {
      return repository.save(attach(entity));
   }

   public ScheduledEvent findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<ScheduledEvent> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<ScheduledEvent> findBy(ScheduledEvent entity, int start, int max, SingularAttribute<ScheduledEvent, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(ScheduledEvent entity, SingularAttribute<ScheduledEvent, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<ScheduledEvent> findByLike(ScheduledEvent entity, int start, int max, SingularAttribute<ScheduledEvent, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(ScheduledEvent entity, SingularAttribute<ScheduledEvent, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private ScheduledEvent attach(ScheduledEvent entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setProperty(propertysMerger.bindAggregated(entity.getProperty()));

      return entity;
   }
}
