package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.model.Quotations;
import org.itkamer.controlpoint.model.QuotationsSearchInput;
import org.itkamer.controlpoint.model.QuotationsSearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/quotationss")
public class QuotationsEndpoint
{

   @Inject
   private QuotationsEJB ejb;

   @Inject
   private TasksMerger tasksMerger;

   @Inject
   private VendorsMerger vendorsMerger;

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public Quotations create(Quotations entity)
   {
      return detach(ejb.create(entity));
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      Quotations deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public Quotations update(Quotations entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      Quotations found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(detach(found)).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public QuotationsSearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<Quotations> resultList = ejb.listAll(start, max);
      QuotationsSearchInput searchInput = new QuotationsSearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new QuotationsSearchResult((long) resultList.size(),
            detach(resultList), detach(searchInput));
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }

//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public QuotationsSearchResult findBy(QuotationsSearchInput searchInput)
//   {
//      SingularAttribute<Quotations, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<Quotations> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new QuotationsSearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(QuotationsSearchInput searchInput)
//   {
//      SingularAttribute<Quotations, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public QuotationsSearchResult findByLike(QuotationsSearchInput searchInput)
//   {
//      SingularAttribute<Quotations, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<Quotations> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new QuotationsSearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(QuotationsSearchInput searchInput)
//   {
//      SingularAttribute<Quotations, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<Quotations, ?>[] readSeachAttributes(
//         QuotationsSearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<Quotations, ?>> result = new ArrayList<SingularAttribute<Quotations, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = Quotations_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<Quotations, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private static final List<String> vendorFields = emptyList;

   private static final List<String> taskFields = emptyList;

   private Quotations detach(Quotations entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setVendor(vendorsMerger.unbind(entity.getVendor(), vendorFields));

      // aggregated
      entity.setTask(tasksMerger.unbind(entity.getTask(), taskFields));

      return entity;
   }

   private List<Quotations> detach(List<Quotations> list)
   {
      if (list == null)
         return list;
      List<Quotations> result = new ArrayList<Quotations>();
      for (Quotations entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private QuotationsSearchInput detach(QuotationsSearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}