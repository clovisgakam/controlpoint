package org.itkamer.controlpoint.resources;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.itkamer.controlpoint.dto.PrincipalDto;
import org.itkamer.controlpoint.model.LoginStatus;
import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.UserRole;
import org.itkamer.controlpoint.repository.LoginsRepository;
import org.itkamer.controlpoint.security.SecurityUtil;

import sun.util.logging.resources.logging;

@Stateless
public class LoginsEJB
{
	/** The salt. */
	public final String SALT = "!+@_#)$(%*%&^" ;

	@Inject
	private SecurityUtil securityUtil ;

	@Inject
	private LoginsRepository repository;

	@Inject
	private AppInstancesMerger appInstancesMerger;

	@Inject
	private AddressMerger addressMerger;

	@Inject
	private UserRoleEJB userRoleEJB;

	public Logins create(Logins entity)
	{
		entity.setRecordDate(new Date());
		entity.setStatus(LoginStatus.ACTIVE);
		entity.setPassword(encodePassword(entity.getPassword()));
		entity.setAppInstance(securityUtil.getInstances());
		return repository.save(attach(entity));
	}
	
	public Logins registerNewUser(Logins entity)
	{
		entity.setRecordDate(new Date());
		entity.setStatus(LoginStatus.ACTIVE);
		entity.setPassword(encodePassword(entity.getPassword()));
		return repository.save(attach(entity));
	}

	/**
	 * Encode password.
	 * 
	 * @param password
	 *            the password
	 * @return the string
	 */
	public String encodePassword(String password){
		return DigestUtils.md5Hex(password+SALT);
	}

	public Logins findByEmail(String email){
		List<Logins> logins = repository.findByEmail(email).maxResults(1).getResultList();
		if(!logins.isEmpty()) return  logins.iterator().next();
		return null ;

	}

	/**
	 * Check password.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @return true, if successful
	 */
	public boolean checkPassword(String source, String target){
		String encodedSource = encodePassword(source);
		return StringUtils.equals(encodedSource, target);
	}

	public Boolean isValidConnectedUserPassword(String target){
		Logins connectedUser = securityUtil.getConnectedUser();
		String source = connectedUser.getPassword();
		return checkPassword(source, target);
	}
	public Boolean updateCurrentUserPass(String newPassword){
		Logins login = securityUtil.getConnectedUser();
		login.setPassword(encodePassword(newPassword));
		repository.save(login);
		return true ;
	}
	public PrincipalDto getPrincipal(){
		Logins login = securityUtil.getConnectedUser();
		if(login!=null){
			List<UserRole> activeUserRoles = userRoleEJB.findActiveUserRoleByLogin(login);
			return new PrincipalDto(login,activeUserRoles);
		}

		return null;
	}
	
	public List<Logins> findByAppInstance(){
		return repository.findByAppInstance(securityUtil.getInstances()).getResultList();
	}
	
	public List<Logins> findActiveLogin(){
		return repository.findByAppInstanceAndStatus(securityUtil.getInstances(),LoginStatus.ACTIVE).getResultList();
	}
	public Logins deleteById(Long id)
	{
		Logins entity = repository.findBy(id);
		if (entity != null)
		{
			repository.remove(entity);
		}
		return entity;
	}

	public Logins update(Logins entity)
	{
		return repository.save(attach(entity));
	}

	public Logins findById(Long id)
	{
		return repository.findBy(id);
	}

	public List<Logins> listAll(int start, int max)
	{
		return repository.findAll(start, max);
	}

	public Long count()
	{
		return repository.count();
	}

	public List<Logins> findBy(Logins entity, int start, int max, SingularAttribute<Logins, ?>[] attributes)
	{
		return repository.findBy(entity, start, max, attributes);
	}

	public Long countBy(Logins entity, SingularAttribute<Logins, ?>[] attributes)
	{
		return repository.count(entity, attributes);
	}

	public List<Logins> findByLike(Logins entity, int start, int max, SingularAttribute<Logins, ?>[] attributes)
	{
		return repository.findByLike(entity, start, max, attributes);
	}

	public Long countByLike(Logins entity, SingularAttribute<Logins, ?>[] attributes)
	{
		return repository.countLike(entity, attributes);
	}

	private Logins attach(Logins entity)
	{
		if (entity == null)
			return null;

		// aggregated
		entity.setAppInstance(appInstancesMerger.bindAggregated(entity.getAppInstance()));

		// aggregated
		entity.setAddress(addressMerger.bindAggregated(entity.getAddress()));

		return entity;
	}
}
