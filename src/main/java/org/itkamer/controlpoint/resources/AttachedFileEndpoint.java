package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.model.AttachedFile;
import org.itkamer.controlpoint.model.AttachedFileSearchInput;
import org.itkamer.controlpoint.model.AttachedFileSearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/attachedfiles")
public class AttachedFileEndpoint
{

   @Inject
   private AttachedFileEJB ejb;

   @Inject
   private TasksMerger tasksMerger;

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public AttachedFile create(AttachedFile entity)
   {
      return detach(ejb.create(entity));
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      AttachedFile deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public AttachedFile update(AttachedFile entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      AttachedFile found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(detach(found)).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public AttachedFileSearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<AttachedFile> resultList = ejb.listAll(start, max);
      AttachedFileSearchInput searchInput = new AttachedFileSearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new AttachedFileSearchResult((long) resultList.size(),
            detach(resultList), detach(searchInput));
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }

//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public AttachedFileSearchResult findBy(AttachedFileSearchInput searchInput)
//   {
//      SingularAttribute<AttachedFile, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<AttachedFile> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new AttachedFileSearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(AttachedFileSearchInput searchInput)
//   {
//      SingularAttribute<AttachedFile, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public AttachedFileSearchResult findByLike(AttachedFileSearchInput searchInput)
//   {
//      SingularAttribute<AttachedFile, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<AttachedFile> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new AttachedFileSearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(AttachedFileSearchInput searchInput)
//   {
//      SingularAttribute<AttachedFile, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<AttachedFile, ?>[] readSeachAttributes(
//         AttachedFileSearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<AttachedFile, ?>> result = new ArrayList<SingularAttribute<AttachedFile, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = AttachedFile_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<AttachedFile, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private static final List<String> taskFields = emptyList;

   private AttachedFile detach(AttachedFile entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setTask(tasksMerger.unbind(entity.getTask(), taskFields));

      return entity;
   }

   private List<AttachedFile> detach(List<AttachedFile> list)
   {
      if (list == null)
         return list;
      List<AttachedFile> result = new ArrayList<AttachedFile>();
      for (AttachedFile entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private AttachedFileSearchInput detach(AttachedFileSearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}