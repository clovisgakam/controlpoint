package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.Unit;
import org.itkamer.controlpoint.repository.UnitRepository;

public class UnitMerger
{

   @Inject
   private UnitRepository repository;

   public Unit bindComposed(Unit entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public Unit bindAggregated(Unit entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<Unit> entities)
   {
      if (entities == null)
         return;
      HashSet<Unit> oldCol = new HashSet<Unit>(entities);
      entities.clear();
      for (Unit entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<Unit> entities)
   {
      if (entities == null)
         return;
      HashSet<Unit> oldCol = new HashSet<Unit>(entities);
      entities.clear();
      for (Unit entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public Unit unbind(final Unit entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      Unit newEntity = new Unit();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<Unit> unbind(final Set<Unit> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<Unit>();
      //       HashSet<Unit> cache = new HashSet<Unit>(entities);
      //       entities.clear();
      //       for (Unit entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
