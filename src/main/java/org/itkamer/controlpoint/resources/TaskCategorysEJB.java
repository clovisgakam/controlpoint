package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.TaskCategorys;
import org.itkamer.controlpoint.repository.TaskCategorysRepository;

@Stateless
public class TaskCategorysEJB
{

   @Inject
   private TaskCategorysRepository repository;

   public TaskCategorys create(TaskCategorys entity)
   {
      return repository.save(attach(entity));
   }

   public TaskCategorys deleteById(Long id)
   {
      TaskCategorys entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public TaskCategorys update(TaskCategorys entity)
   {
      return repository.save(attach(entity));
   }

   public TaskCategorys findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<TaskCategorys> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<TaskCategorys> findBy(TaskCategorys entity, int start, int max, SingularAttribute<TaskCategorys, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(TaskCategorys entity, SingularAttribute<TaskCategorys, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<TaskCategorys> findByLike(TaskCategorys entity, int start, int max, SingularAttribute<TaskCategorys, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(TaskCategorys entity, SingularAttribute<TaskCategorys, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private TaskCategorys attach(TaskCategorys entity)
   {
      if (entity == null)
         return null;

      return entity;
   }
}
