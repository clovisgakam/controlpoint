package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.TaskComment;
import org.itkamer.controlpoint.repository.TaskCommentRepository;

public class TaskCommentMerger
{

   @Inject
   private TaskCommentRepository repository;

   public TaskComment bindComposed(TaskComment entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public TaskComment bindAggregated(TaskComment entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<TaskComment> entities)
   {
      if (entities == null)
         return;
      HashSet<TaskComment> oldCol = new HashSet<TaskComment>(entities);
      entities.clear();
      for (TaskComment entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<TaskComment> entities)
   {
      if (entities == null)
         return;
      HashSet<TaskComment> oldCol = new HashSet<TaskComment>(entities);
      entities.clear();
      for (TaskComment entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public TaskComment unbind(final TaskComment entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      TaskComment newEntity = new TaskComment();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<TaskComment> unbind(final Set<TaskComment> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<TaskComment>();
      //       HashSet<TaskComment> cache = new HashSet<TaskComment>(entities);
      //       entities.clear();
      //       for (TaskComment entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
