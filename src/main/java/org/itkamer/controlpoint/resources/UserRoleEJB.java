package org.itkamer.controlpoint.resources;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.LoginStatus;
import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.Propertys;
import org.itkamer.controlpoint.model.RoleNames;
import org.itkamer.controlpoint.model.UserPermission;
import org.itkamer.controlpoint.model.UserRole;
import org.itkamer.controlpoint.repository.UserRoleRepository;

@Stateless
public class UserRoleEJB {

	@Inject
	private UserRoleRepository repository;

	@Inject
	private UnitMerger unitMerger;

	@Inject
	private PropertysMerger propertysMerger;

	@Inject
	private LoginsMerger loginsMerger;

	public List<UserRole> findActiveUserRoleByLogin(Logins login) {
		return repository.findActiveUserRoleByLogin(login, LoginStatus.ACTIVE,
				new Date()).getResultList();
	}

	public UserRole create(UserRole entity) {
		return repository.save(attach(entity));
	}

	public UserRole deleteById(Long id) {
		UserRole entity = repository.findBy(id);
		if (entity != null) {
			repository.remove(entity);
		}
		return entity;
	}

	public UserRole update(UserRole entity) {
		return repository.save(attach(entity));
	}

	public UserRole findById(Long id) {
		return repository.findBy(id);
	}

	public List<UserRole> listAll(int start, int max) {
		return repository.findAll(start, max);
	}

	public Long count() {
		return repository.count();
	}

	public List<UserRole> findBy(UserRole entity, int start, int max,
			SingularAttribute<UserRole, ?>[] attributes) {
		return repository.findBy(entity, start, max, attributes);
	}

	public Long countBy(UserRole entity,
			SingularAttribute<UserRole, ?>[] attributes) {
		return repository.count(entity, attributes);
	}

	public List<UserRole> findByLike(UserRole entity, int start, int max,
			SingularAttribute<UserRole, ?>[] attributes) {
		return repository.findByLike(entity, start, max, attributes);
	}

	public Long countByLike(UserRole entity,
			SingularAttribute<UserRole, ?>[] attributes) {
		return repository.countLike(entity, attributes);
	}

	public List<UserRole> findPropertyByAdminLogin(Logins login) {
		return repository.findPropertyByAdminLogin(login, RoleNames.PROPERTY_MANAGER,
				LoginStatus.ACTIVE).getResultList();
	}
	
	public UserPermission findPermissionByLogin(Logins login, Propertys property){
		List<UserRole> userRole = repository.findPermissionByLogin(login, property).getResultList();
		return userRole.get(0).getPermission();
	}
	
	public List<UserRole> findActiveUserRoleByProperty(Propertys propertys) {
		return repository.findActiveUserRoleByProperty(propertys, LoginStatus.ACTIVE).getResultList();
	}
	public List<UserRole> findUserRoleByProperty(Propertys propertys) {
		return repository.findUserRoleByProperty(propertys).getResultList();
	}

	private UserRole attach(UserRole entity) {
		if (entity == null)
			return null;

		// aggregated
		entity.setLogin(loginsMerger.bindAggregated(entity.getLogin()));

		// aggregated
		entity.setUnit(unitMerger.bindAggregated(entity.getUnit()));

		// aggregated
		entity.setProperty(propertysMerger.bindAggregated(entity.getProperty()));

		return entity;
	}
}
