package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.TaskRole;
import org.itkamer.controlpoint.repository.TaskRoleRepository;

@Stateless
public class TaskRoleEJB
{

   @Inject
   private TaskRoleRepository repository;

   public TaskRole create(TaskRole entity)
   {
      return repository.save(attach(entity));
   }

   public TaskRole deleteById(Long id)
   {
      TaskRole entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public TaskRole update(TaskRole entity)
   {
      return repository.save(attach(entity));
   }

   public TaskRole findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<TaskRole> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<TaskRole> findBy(TaskRole entity, int start, int max, SingularAttribute<TaskRole, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(TaskRole entity, SingularAttribute<TaskRole, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<TaskRole> findByLike(TaskRole entity, int start, int max, SingularAttribute<TaskRole, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(TaskRole entity, SingularAttribute<TaskRole, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private TaskRole attach(TaskRole entity)
   {
      if (entity == null)
         return null;

      return entity;
   }
}
