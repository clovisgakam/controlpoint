package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.dto.PrincipalDto;
import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.LoginsSearchInput;
import org.itkamer.controlpoint.model.LoginsSearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/logins")
public class LoginsEndpoint
{

   @Inject
   private LoginsEJB ejb;

   @Inject
   private AppInstancesMerger appInstancesMerger;

   @Inject
   private AddressMerger addressMerger;
   
   @GET
   @Path("/current-user")
   @Produces({ "application/json"})
   public PrincipalDto currentUser()
   {
      return ejb.getPrincipal();
   }
   
   @GET
   @Path("/isValidConnectedUserPassword")
   @Produces({ "application/json"})
   @Consumes({ "application/json"})
   public boolean update(String password)
   {
      return ejb.isValidConnectedUserPassword(password);
   }

   @PUT
   @Path("/updateCurrentUserPass")
   @Produces({ "application/json"})
   @Consumes({ "application/json"})
   public boolean updateCurrentUserPass(String newPassword)
   {
      return ejb.updateCurrentUserPass(newPassword);
   }

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public Logins create(Logins entity)
   {
      return detach(ejb.create(entity));
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      Logins deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public Logins update(Logins entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      Logins found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(detach(found)).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public LoginsSearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<Logins> resultList = ejb.listAll(start, max);
      LoginsSearchInput searchInput = new LoginsSearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new LoginsSearchResult((long) resultList.size(),
        
    		  detach(resultList), detach(searchInput));
   }
   
   @GET
   @Path("/findActiveLogin")
   @Produces({ "application/json", "application/xml" })
   public List<Logins> findActiveLogin()
   {
      List<Logins> resultList = ejb.findActiveLogin();
      return resultList ;
   }

   @GET
   @Path("/findByAppInstance")
   @Produces({ "application/json", "application/xml" })
   public List<Logins> findByAppInstance()
   {
      List<Logins> resultList = ejb.findByAppInstance();
      return resultList ;
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }

//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public LoginsSearchResult findBy(LoginsSearchInput searchInput)
//   {
//      SingularAttribute<Logins, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<Logins> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new LoginsSearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(LoginsSearchInput searchInput)
//   {
//      SingularAttribute<Logins, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public LoginsSearchResult findByLike(LoginsSearchInput searchInput)
//   {
//      SingularAttribute<Logins, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<Logins> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new LoginsSearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(LoginsSearchInput searchInput)
//   {
//      SingularAttribute<Logins, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<Logins, ?>[] readSeachAttributes(
//         LoginsSearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<Logins, ?>> result = new ArrayList<SingularAttribute<Logins, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = Logins_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<Logins, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private static final List<String> appInstanceFields = emptyList;

   private static final List<String> addressFields = emptyList;

   private Logins detach(Logins entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setAppInstance(appInstancesMerger.unbind(entity.getAppInstance(), appInstanceFields));

      // aggregated
      entity.setAddress(addressMerger.unbind(entity.getAddress(), addressFields));

      return entity;
   }

   private List<Logins> detach(List<Logins> list)
   {
      if (list == null)
         return list;
      List<Logins> result = new ArrayList<Logins>();
      for (Logins entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private LoginsSearchInput detach(LoginsSearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}