package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.ScheduledEvent;
import org.itkamer.controlpoint.repository.ScheduledEventRepository;

public class ScheduledEventMerger
{

   @Inject
   private ScheduledEventRepository repository;

   public ScheduledEvent bindComposed(ScheduledEvent entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public ScheduledEvent bindAggregated(ScheduledEvent entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<ScheduledEvent> entities)
   {
      if (entities == null)
         return;
      HashSet<ScheduledEvent> oldCol = new HashSet<ScheduledEvent>(entities);
      entities.clear();
      for (ScheduledEvent entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<ScheduledEvent> entities)
   {
      if (entities == null)
         return;
      HashSet<ScheduledEvent> oldCol = new HashSet<ScheduledEvent>(entities);
      entities.clear();
      for (ScheduledEvent entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public ScheduledEvent unbind(final ScheduledEvent entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      ScheduledEvent newEntity = new ScheduledEvent();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<ScheduledEvent> unbind(final Set<ScheduledEvent> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<ScheduledEvent>();
      //       HashSet<ScheduledEvent> cache = new HashSet<ScheduledEvent>(entities);
      //       entities.clear();
      //       for (ScheduledEvent entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
