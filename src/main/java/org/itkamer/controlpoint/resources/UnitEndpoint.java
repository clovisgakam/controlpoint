package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.model.Unit;
import org.itkamer.controlpoint.model.UnitSearchInput;
import org.itkamer.controlpoint.model.UnitSearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/units")
public class UnitEndpoint
{

   @Inject
   private UnitEJB ejb;

   @Inject
   private PropertysMerger propertysMerger;

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public Unit create(Unit entity)
   {
      return detach(ejb.create(entity));
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      Unit deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public Unit update(Unit entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      Unit found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(detach(found)).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public UnitSearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<Unit> resultList = ejb.listAll(start, max);
      UnitSearchInput searchInput = new UnitSearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new UnitSearchResult((long) resultList.size(),
            detach(resultList), detach(searchInput));
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }

//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public UnitSearchResult findBy(UnitSearchInput searchInput)
//   {
//      SingularAttribute<Unit, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<Unit> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new UnitSearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(UnitSearchInput searchInput)
//   {
//      SingularAttribute<Unit, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public UnitSearchResult findByLike(UnitSearchInput searchInput)
//   {
//      SingularAttribute<Unit, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<Unit> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new UnitSearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(UnitSearchInput searchInput)
//   {
//      SingularAttribute<Unit, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<Unit, ?>[] readSeachAttributes(
//         UnitSearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<Unit, ?>> result = new ArrayList<SingularAttribute<Unit, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = Unit_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<Unit, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private static final List<String> propertyFields = emptyList;

   private Unit detach(Unit entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setProperty(propertysMerger.unbind(entity.getProperty(), propertyFields));

      return entity;
   }

   private List<Unit> detach(List<Unit> list)
   {
      if (list == null)
         return list;
      List<Unit> result = new ArrayList<Unit>();
      for (Unit entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private UnitSearchInput detach(UnitSearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}