package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.UserRole;
import org.itkamer.controlpoint.repository.UserRoleRepository;

public class UserRoleMerger
{

   @Inject
   private UserRoleRepository repository;

   public UserRole bindComposed(UserRole entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public UserRole bindAggregated(UserRole entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<UserRole> entities)
   {
      if (entities == null)
         return;
      HashSet<UserRole> oldCol = new HashSet<UserRole>(entities);
      entities.clear();
      for (UserRole entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<UserRole> entities)
   {
      if (entities == null)
         return;
      HashSet<UserRole> oldCol = new HashSet<UserRole>(entities);
      entities.clear();
      for (UserRole entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public UserRole unbind(final UserRole entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      UserRole newEntity = new UserRole();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<UserRole> unbind(final Set<UserRole> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<UserRole>();
      //       HashSet<UserRole> cache = new HashSet<UserRole>(entities);
      //       entities.clear();
      //       for (UserRole entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
