package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.model.PropertyType;
import org.itkamer.controlpoint.model.PropertyTypeSearchInput;
import org.itkamer.controlpoint.model.PropertyTypeSearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/propertytypes")
public class PropertyTypeEndpoint
{

   @Inject
   private PropertyTypeEJB ejb;

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public PropertyType create(PropertyType entity)
   {
      return detach(ejb.create(entity));
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      PropertyType deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public PropertyType update(PropertyType entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      PropertyType found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(detach(found)).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public PropertyTypeSearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<PropertyType> resultList = ejb.listAll(start, max);
      PropertyTypeSearchInput searchInput = new PropertyTypeSearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new PropertyTypeSearchResult((long) resultList.size(),
            detach(resultList), detach(searchInput));
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }

//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public PropertyTypeSearchResult findBy(PropertyTypeSearchInput searchInput)
//   {
//      SingularAttribute<PropertyType, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<PropertyType> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new PropertyTypeSearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(PropertyTypeSearchInput searchInput)
//   {
//      SingularAttribute<PropertyType, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public PropertyTypeSearchResult findByLike(PropertyTypeSearchInput searchInput)
//   {
//      SingularAttribute<PropertyType, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<PropertyType> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new PropertyTypeSearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(PropertyTypeSearchInput searchInput)
//   {
//      SingularAttribute<PropertyType, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<PropertyType, ?>[] readSeachAttributes(
//         PropertyTypeSearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<PropertyType, ?>> result = new ArrayList<SingularAttribute<PropertyType, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = PropertyType_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<PropertyType, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private PropertyType detach(PropertyType entity)
   {
      if (entity == null)
         return null;

      return entity;
   }

   private List<PropertyType> detach(List<PropertyType> list)
   {
      if (list == null)
         return list;
      List<PropertyType> result = new ArrayList<PropertyType>();
      for (PropertyType entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private PropertyTypeSearchInput detach(PropertyTypeSearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}