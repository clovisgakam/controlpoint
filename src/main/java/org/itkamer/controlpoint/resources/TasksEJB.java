package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.Tasks;
import org.itkamer.controlpoint.repository.TasksRepository;

@Stateless
public class TasksEJB
{

   @Inject
   private TasksRepository repository;

   @Inject
   private TaskCategorysMerger taskCategorysMerger;

   public Tasks create(Tasks entity)
   {
      return repository.save(attach(entity));
   }

   public Tasks deleteById(Long id)
   {
      Tasks entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public Tasks update(Tasks entity)
   {
      return repository.save(attach(entity));
   }

   public Tasks findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<Tasks> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<Tasks> findBy(Tasks entity, int start, int max, SingularAttribute<Tasks, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(Tasks entity, SingularAttribute<Tasks, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<Tasks> findByLike(Tasks entity, int start, int max, SingularAttribute<Tasks, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(Tasks entity, SingularAttribute<Tasks, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private Tasks attach(Tasks entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setCategory(taskCategorysMerger.bindAggregated(entity.getCategory()));

      return entity;
   }
}
