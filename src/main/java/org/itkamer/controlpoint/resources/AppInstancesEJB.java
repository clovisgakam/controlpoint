package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.AppInstances;
import org.itkamer.controlpoint.repository.AppInstancesRepository;

@Stateless
public class AppInstancesEJB
{

   @Inject
   private AppInstancesRepository repository;

   @Inject
   private AddressMerger addressMerger;

   public AppInstances create(AppInstances entity)
   {
      return repository.save(attach(entity));
   }

   public AppInstances deleteById(Long id)
   {
      AppInstances entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public AppInstances update(AppInstances entity)
   {
      return repository.save(attach(entity));
   }

   public AppInstances findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<AppInstances> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<AppInstances> findBy(AppInstances entity, int start, int max, SingularAttribute<AppInstances, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(AppInstances entity, SingularAttribute<AppInstances, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<AppInstances> findByLike(AppInstances entity, int start, int max, SingularAttribute<AppInstances, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(AppInstances entity, SingularAttribute<AppInstances, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private AppInstances attach(AppInstances entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setAddress(addressMerger.bindAggregated(entity.getAddress()));

      return entity;
   }
}
