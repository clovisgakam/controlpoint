package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.Vendors;
import org.itkamer.controlpoint.repository.VendorsRepository;

@Stateless
public class VendorsEJB
{

   @Inject
   private VendorsRepository repository;

   @Inject
   private AddressMerger addressMerger;

   @Inject
   private CompanyMerger companyMerger;

   @Inject
   private VendorCategorysMerger vendorCategorysMerger;

   public Vendors create(Vendors entity)
   {
      return repository.save(entity);
   }

   public Vendors deleteById(Long id)
   {
      Vendors entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public Vendors update(Vendors entity)
   {
      return repository.save(attach(entity));
   }

   public Vendors findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<Vendors> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<Vendors> findBy(Vendors entity, int start, int max, SingularAttribute<Vendors, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(Vendors entity, SingularAttribute<Vendors, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<Vendors> findByLike(Vendors entity, int start, int max, SingularAttribute<Vendors, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(Vendors entity, SingularAttribute<Vendors, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private Vendors attach(Vendors entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setCompany(companyMerger.bindAggregated(entity.getCompany()));

      // aggregated
      entity.setAddress(addressMerger.bindAggregated(entity.getAddress()));

      // aggregated
      entity.setCategory(vendorCategorysMerger.bindAggregated(entity.getCategory()));

      return entity;
   }
}
