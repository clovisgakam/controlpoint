package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.Finances;
import org.itkamer.controlpoint.repository.FinancesRepository;

@Stateless
public class FinancesEJB
{

   @Inject
   private FinancesRepository repository;

   @Inject
   private PropertysMerger propertysMerger;

   @Inject
   private UnitMerger unitMerger;

   public Finances create(Finances entity)
   {
      return repository.save(attach(entity));
   }

   public Finances deleteById(Long id)
   {
      Finances entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public Finances update(Finances entity)
   {
      return repository.save(attach(entity));
   }

   public Finances findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<Finances> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<Finances> findBy(Finances entity, int start, int max, SingularAttribute<Finances, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(Finances entity, SingularAttribute<Finances, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<Finances> findByLike(Finances entity, int start, int max, SingularAttribute<Finances, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(Finances entity, SingularAttribute<Finances, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private Finances attach(Finances entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setProperty(propertysMerger.bindAggregated(entity.getProperty()));

      // aggregated
      entity.setUnit(unitMerger.bindAggregated(entity.getUnit()));

      return entity;
   }
}
