package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.model.AppInstances;
import org.itkamer.controlpoint.model.AppInstancesSearchInput;
import org.itkamer.controlpoint.model.AppInstancesSearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/appinstances")
public class AppInstancesEndpoint
{

   @Inject
   private AppInstancesEJB ejb;

   @Inject
   private AddressMerger addressMerger;

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public AppInstances create(AppInstances entity)
   {
      return detach(ejb.create(entity));
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      AppInstances deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public AppInstances update(AppInstances entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      AppInstances found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(detach(found)).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public AppInstancesSearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<AppInstances> resultList = ejb.listAll(start, max);
      AppInstancesSearchInput searchInput = new AppInstancesSearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new AppInstancesSearchResult((long) resultList.size(),
            resultList, searchInput);
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }

//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public AppInstancesSearchResult findBy(AppInstancesSearchInput searchInput)
//   {
//      SingularAttribute<AppInstances, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<AppInstances> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new AppInstancesSearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(AppInstancesSearchInput searchInput)
//   {
//      SingularAttribute<AppInstances, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public AppInstancesSearchResult findByLike(AppInstancesSearchInput searchInput)
//   {
//      SingularAttribute<AppInstances, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<AppInstances> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new AppInstancesSearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(AppInstancesSearchInput searchInput)
//   {
//      SingularAttribute<AppInstances, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<AppInstances, ?>[] readSeachAttributes(
//         AppInstancesSearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<AppInstances, ?>> result = new ArrayList<SingularAttribute<AppInstances, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = AppInstances_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<AppInstances, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private static final List<String> addressFields = emptyList;

   private AppInstances detach(AppInstances entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setAddress(addressMerger.unbind(entity.getAddress(), addressFields));

      return entity;
   }

   private List<AppInstances> detach(List<AppInstances> list)
   {
      if (list == null)
         return list;
      List<AppInstances> result = new ArrayList<AppInstances>();
      for (AppInstances entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private AppInstancesSearchInput detach(AppInstancesSearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}