package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.VendorCategorys;
import org.itkamer.controlpoint.repository.VendorCategorysRepository;

@Stateless
public class VendorCategorysEJB
{

   @Inject
   private VendorCategorysRepository repository;

   public VendorCategorys create(VendorCategorys entity)
   {
      return repository.save(attach(entity));
   }

   public VendorCategorys deleteById(Long id)
   {
      VendorCategorys entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public VendorCategorys update(VendorCategorys entity)
   {
      return repository.save(attach(entity));
   }

   public VendorCategorys findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<VendorCategorys> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<VendorCategorys> findBy(VendorCategorys entity, int start, int max, SingularAttribute<VendorCategorys, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(VendorCategorys entity, SingularAttribute<VendorCategorys, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<VendorCategorys> findByLike(VendorCategorys entity, int start, int max, SingularAttribute<VendorCategorys, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(VendorCategorys entity, SingularAttribute<VendorCategorys, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private VendorCategorys attach(VendorCategorys entity)
   {
      if (entity == null)
         return null;

      return entity;
   }
}
