package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.model.Warranty;
import org.itkamer.controlpoint.model.WarrantySearchInput;
import org.itkamer.controlpoint.model.WarrantySearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/warrantys")
public class WarrantyEndpoint
{

   @Inject
   private WarrantyEJB ejb;

   @Inject
   private ScheduledEventMerger scheduledEventMerger;

   @Inject
   private VendorsMerger vendorsMerger;

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public Warranty create(Warranty entity)
   {
      return detach(ejb.create(entity));
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      Warranty deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public Warranty update(Warranty entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      Warranty found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(detach(found)).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public WarrantySearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<Warranty> resultList = ejb.listAll(start, max);
      WarrantySearchInput searchInput = new WarrantySearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new WarrantySearchResult((long) resultList.size(),
            detach(resultList), detach(searchInput));
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }

//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public WarrantySearchResult findBy(WarrantySearchInput searchInput)
//   {
//      SingularAttribute<Warranty, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<Warranty> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new WarrantySearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(WarrantySearchInput searchInput)
//   {
//      SingularAttribute<Warranty, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public WarrantySearchResult findByLike(WarrantySearchInput searchInput)
//   {
//      SingularAttribute<Warranty, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<Warranty> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new WarrantySearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(WarrantySearchInput searchInput)
//   {
//      SingularAttribute<Warranty, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<Warranty, ?>[] readSeachAttributes(
//         WarrantySearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<Warranty, ?>> result = new ArrayList<SingularAttribute<Warranty, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = Warranty_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<Warranty, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private static final List<String> scheduledEventFields = emptyList;

   private static final List<String> vendorFields = emptyList;

   private Warranty detach(Warranty entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setScheduledEvent(scheduledEventMerger.unbind(entity.getScheduledEvent(), scheduledEventFields));

      // aggregated
      entity.setVendor(vendorsMerger.unbind(entity.getVendor(), vendorFields));

      return entity;
   }

   private List<Warranty> detach(List<Warranty> list)
   {
      if (list == null)
         return list;
      List<Warranty> result = new ArrayList<Warranty>();
      for (Warranty entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private WarrantySearchInput detach(WarrantySearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}