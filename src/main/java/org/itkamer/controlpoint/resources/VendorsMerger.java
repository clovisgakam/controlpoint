package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.Vendors;
import org.itkamer.controlpoint.repository.VendorsRepository;

public class VendorsMerger
{

   @Inject
   private VendorsRepository repository;

   public Vendors bindComposed(Vendors entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public Vendors bindAggregated(Vendors entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<Vendors> entities)
   {
      if (entities == null)
         return;
      HashSet<Vendors> oldCol = new HashSet<Vendors>(entities);
      entities.clear();
      for (Vendors entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<Vendors> entities)
   {
      if (entities == null)
         return;
      HashSet<Vendors> oldCol = new HashSet<Vendors>(entities);
      entities.clear();
      for (Vendors entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public Vendors unbind(final Vendors entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      Vendors newEntity = new Vendors();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<Vendors> unbind(final Set<Vendors> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<Vendors>();
      //       HashSet<Vendors> cache = new HashSet<Vendors>(entities);
      //       entities.clear();
      //       for (Vendors entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
