package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.VendorCategorys;
import org.itkamer.controlpoint.repository.VendorCategorysRepository;

public class VendorCategorysMerger
{

   @Inject
   private VendorCategorysRepository repository;

   public VendorCategorys bindComposed(VendorCategorys entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public VendorCategorys bindAggregated(VendorCategorys entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<VendorCategorys> entities)
   {
      if (entities == null)
         return;
      HashSet<VendorCategorys> oldCol = new HashSet<VendorCategorys>(entities);
      entities.clear();
      for (VendorCategorys entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<VendorCategorys> entities)
   {
      if (entities == null)
         return;
      HashSet<VendorCategorys> oldCol = new HashSet<VendorCategorys>(entities);
      entities.clear();
      for (VendorCategorys entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public VendorCategorys unbind(final VendorCategorys entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      VendorCategorys newEntity = new VendorCategorys();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<VendorCategorys> unbind(final Set<VendorCategorys> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<VendorCategorys>();
      //       HashSet<VendorCategorys> cache = new HashSet<VendorCategorys>(entities);
      //       entities.clear();
      //       for (VendorCategorys entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
