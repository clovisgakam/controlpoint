package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.TaskCategorys;
import org.itkamer.controlpoint.repository.TaskCategorysRepository;

public class TaskCategorysMerger
{

   @Inject
   private TaskCategorysRepository repository;

   public TaskCategorys bindComposed(TaskCategorys entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public TaskCategorys bindAggregated(TaskCategorys entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<TaskCategorys> entities)
   {
      if (entities == null)
         return;
      HashSet<TaskCategorys> oldCol = new HashSet<TaskCategorys>(entities);
      entities.clear();
      for (TaskCategorys entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<TaskCategorys> entities)
   {
      if (entities == null)
         return;
      HashSet<TaskCategorys> oldCol = new HashSet<TaskCategorys>(entities);
      entities.clear();
      for (TaskCategorys entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public TaskCategorys unbind(final TaskCategorys entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      TaskCategorys newEntity = new TaskCategorys();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<TaskCategorys> unbind(final Set<TaskCategorys> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<TaskCategorys>();
      //       HashSet<TaskCategorys> cache = new HashSet<TaskCategorys>(entities);
      //       entities.clear();
      //       for (TaskCategorys entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
