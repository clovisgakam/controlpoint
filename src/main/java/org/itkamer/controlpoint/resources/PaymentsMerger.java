package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.Payments;
import org.itkamer.controlpoint.repository.PaymentsRepository;

public class PaymentsMerger
{

   @Inject
   private PaymentsRepository repository;

   public Payments bindComposed(Payments entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public Payments bindAggregated(Payments entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<Payments> entities)
   {
      if (entities == null)
         return;
      HashSet<Payments> oldCol = new HashSet<Payments>(entities);
      entities.clear();
      for (Payments entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<Payments> entities)
   {
      if (entities == null)
         return;
      HashSet<Payments> oldCol = new HashSet<Payments>(entities);
      entities.clear();
      for (Payments entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public Payments unbind(final Payments entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      Payments newEntity = new Payments();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<Payments> unbind(final Set<Payments> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<Payments>();
      //       HashSet<Payments> cache = new HashSet<Payments>(entities);
      //       entities.clear();
      //       for (Payments entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
