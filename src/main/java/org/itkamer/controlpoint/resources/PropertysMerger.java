package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.Propertys;
import org.itkamer.controlpoint.repository.PropertysRepository;

public class PropertysMerger
{

   @Inject
   private PropertysRepository repository;

   public Propertys bindComposed(Propertys entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public Propertys bindAggregated(Propertys entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<Propertys> entities)
   {
      if (entities == null)
         return;
      HashSet<Propertys> oldCol = new HashSet<Propertys>(entities);
      entities.clear();
      for (Propertys entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<Propertys> entities)
   {
      if (entities == null)
         return;
      HashSet<Propertys> oldCol = new HashSet<Propertys>(entities);
      entities.clear();
      for (Propertys entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public Propertys unbind(final Propertys entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      Propertys newEntity = new Propertys();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<Propertys> unbind(final Set<Propertys> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<Propertys>();
      //       HashSet<Propertys> cache = new HashSet<Propertys>(entities);
      //       entities.clear();
      //       for (Propertys entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
