package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.Warranty;
import org.itkamer.controlpoint.repository.WarrantyRepository;

public class WarrantyMerger
{

   @Inject
   private WarrantyRepository repository;

   public Warranty bindComposed(Warranty entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public Warranty bindAggregated(Warranty entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<Warranty> entities)
   {
      if (entities == null)
         return;
      HashSet<Warranty> oldCol = new HashSet<Warranty>(entities);
      entities.clear();
      for (Warranty entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<Warranty> entities)
   {
      if (entities == null)
         return;
      HashSet<Warranty> oldCol = new HashSet<Warranty>(entities);
      entities.clear();
      for (Warranty entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public Warranty unbind(final Warranty entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      Warranty newEntity = new Warranty();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<Warranty> unbind(final Set<Warranty> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<Warranty>();
      //       HashSet<Warranty> cache = new HashSet<Warranty>(entities);
      //       entities.clear();
      //       for (Warranty entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
