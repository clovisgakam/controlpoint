package org.itkamer.controlpoint.resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.dto.LoginsPermDto;
import org.itkamer.controlpoint.dto.PortefolioModelViewDto;
import org.itkamer.controlpoint.dto.PropertyCreateModelViewDto;
import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.Propertys;
import org.itkamer.controlpoint.model.PropertysSearchInput;
import org.itkamer.controlpoint.model.PropertysSearchResult;
import org.itkamer.controlpoint.model.UserPermission;
import org.itkamer.controlpoint.model.UserRole;
import org.itkamer.controlpoint.security.SecurityUtil;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/propertys")
public class PropertysEndpoint {

	@Inject
	private PropertysEJB ejb;

	@Inject
	private PropertysEJB propertyEjb;

	@Inject
	private UserRoleEJB userRoleEJB;

	@Inject
	private SecurityUtil securityUtil;

	@Inject
	private AppInstancesMerger appInstancesMerger;

	@Inject
	private PropertyTypeMerger propertyTypeMerger;

	@Inject
	private AddressMerger addressMerger;

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public Propertys create(Propertys entity) {
		return detach(ejb.create(entity));
	}

	@POST
	@Path("/newProperty")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public Propertys newProperty(PropertyCreateModelViewDto data) {
		Propertys newProperty = ejb.newProperty(data);
		return newProperty;
	}

	
	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteById(@PathParam("id") Long id) {
		Propertys deleted = ejb.deleteById(id);
		if (deleted == null)
			return Response.status(Status.NOT_FOUND).build();

		return Response.ok(deleted).build();
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Produces({ "application/json", "application/xml" })
	@Consumes({ "application/json", "application/xml" })
	public Propertys update(Propertys entity) {
		return detach(ejb.update(entity));
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces({ "application/json", "application/xml" })
	public Response findById(@PathParam("id") Long id) {
		Propertys found = ejb.findById(id);
		if (found == null)
			return Response.status(Status.NOT_FOUND).build();
		return Response.ok(detach(found)).build();
	}

	@GET
	@Produces({ "application/json", "application/xml" })
	public PropertysSearchResult listAll(@QueryParam("start") int start,
			@QueryParam("max") int max) {
		List<Propertys> resultList = ejb.listAll(start, max);
		PropertysSearchInput searchInput = new PropertysSearchInput();
		searchInput.setStart(start);
		searchInput.setMax(max);
		return new PropertysSearchResult((long) resultList.size(),
				resultList, searchInput);
	}

	@GET
	@Path("/loginPropertysActive")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public List<Logins> findActiveUserRoleByLogin(Propertys propertys) {
		List<UserRole> userRole = userRoleEJB.findActiveUserRoleByProperty(propertys);
		List<Logins> logins = new ArrayList<Logins>();
		for (UserRole userR : userRole) {
			logins.add(userR.getLogin());
		}
		return logins;
	}
	
	@GET
	@Path("/propertyPortefollio")
	@Produces({ "application/json", "application/xml" })
	public PortefolioModelViewDto propertyPortefollio() {
		PortefolioModelViewDto portefolio = ejb.getPropertyPortefolio();
		return portefolio ;
	}
	
	@GET
	@Path("/loginsAdminProp")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public List<Propertys> findLoginsPropertys(Logins login) {
		List<UserRole> userRoles = userRoleEJB.findPropertyByAdminLogin(login);
		List<Propertys> propertysLogins = new ArrayList<Propertys>(); 

		for (UserRole userRole : userRoles) {
			propertysLogins.add(userRole.getProperty());
		}
		return propertysLogins;
	}
	
	@GET
	@Path("/loginPermission")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public LoginsPermDto findLoginsPropertysPermission(LoginsPermDto loginsRoleAndPermDto) {
		UserPermission userPermission = userRoleEJB.findPermissionByLogin(loginsRoleAndPermDto.getLogin(), loginsRoleAndPermDto.getProperty());
		loginsRoleAndPermDto.setPerm(userPermission);
		return loginsRoleAndPermDto;
	}

	@GET
	@Path("/count")
	public Long count() {
		return ejb.count();
	}

	// @POST
	// @Path("/findBy")
	// @Produces({ "application/json", "application/xml" })
	// @Consumes({ "application/json", "application/xml" })
	// public PropertysSearchResult findBy(PropertysSearchInput searchInput)
	// {
	// SingularAttribute<Propertys, ?>[] attributes =
	// readSeachAttributes(searchInput);
	// Long count = ejb.countBy(searchInput.getEntity(), attributes);
	// List<Propertys> resultList = ejb.findBy(searchInput.getEntity(),
	// searchInput.getStart(), searchInput.getMax(), attributes);
	// return new PropertysSearchResult(count, detach(resultList),
	// detach(searchInput));
	// }
	//
	// @POST
	// @Path("/countBy")
	// @Consumes({ "application/json", "application/xml" })
	// public Long countBy(PropertysSearchInput searchInput)
	// {
	// SingularAttribute<Propertys, ?>[] attributes =
	// readSeachAttributes(searchInput);
	// return ejb.countBy(searchInput.getEntity(), attributes);
	// }
	//
	// @POST
	// @Path("/findByLike")
	// @Produces({ "application/json", "application/xml" })
	// @Consumes({ "application/json", "application/xml" })
	// public PropertysSearchResult findByLike(PropertysSearchInput searchInput)
	// {
	// SingularAttribute<Propertys, ?>[] attributes =
	// readSeachAttributes(searchInput);
	// Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
	// List<Propertys> resultList = ejb.findByLike(searchInput.getEntity(),
	// searchInput.getStart(), searchInput.getMax(), attributes);
	// return new PropertysSearchResult(countLike, detach(resultList),
	// detach(searchInput));
	// }
	//
	// @POST
	// @Path("/countByLike")
	// @Consumes({ "application/json", "application/xml" })
	// public Long countByLike(PropertysSearchInput searchInput)
	// {
	// SingularAttribute<Propertys, ?>[] attributes =
	// readSeachAttributes(searchInput);
	// return ejb.countByLike(searchInput.getEntity(), attributes);
	// }
	//
	// @SuppressWarnings("unchecked")
	// private SingularAttribute<Propertys, ?>[] readSeachAttributes(
	// PropertysSearchInput searchInput)
	// {
	// List<String> fieldNames = searchInput.getFieldNames();
	// List<SingularAttribute<Propertys, ?>> result = new
	// ArrayList<SingularAttribute<Propertys, ?>>();
	// for (String fieldName : fieldNames)
	// {
	// Field[] fields = Propertys_.class.getFields();
	// for (Field field : fields)
	// {
	// if (field.getName().equals(fieldName))
	// {
	// try
	// {
	// result.add((SingularAttribute<Propertys, ?>) field.get(null));
	// }
	// catch (IllegalArgumentException e)
	// {
	// throw new IllegalStateException(e);
	// }
	// catch (IllegalAccessException e)
	// {
	// throw new IllegalStateException(e);
	// }
	// }
	// }
	// }
	// return result.toArray(new SingularAttribute[result.size()]);
	// }

	private static final List<String> emptyList = Collections.emptyList();

	private static final List<String> addressFields = emptyList;

	private static final List<String> appInstanceFields = emptyList;

	private static final List<String> typeFields = emptyList;

	private Propertys detach(Propertys entity) {
		if (entity == null)
			return null;

		// aggregated
		entity.setAddress(addressMerger.unbind(entity.getAddress(),
				addressFields));

		// aggregated
		entity.setAppInstance(appInstancesMerger.unbind(
				entity.getAppInstance(), appInstanceFields));

		// aggregated
		entity.setType(propertyTypeMerger.unbind(entity.getType(), typeFields));

		return entity;
	}

	private List<Propertys> detach(List<Propertys> list) {
		if (list == null)
			return list;
		List<Propertys> result = new ArrayList<Propertys>();
		for (Propertys entity : list) {
			result.add(detach(entity));
		}
		return result;
	}

	private PropertysSearchInput detach(PropertysSearchInput searchInput) {
		searchInput.setEntity(detach(searchInput.getEntity()));
		return searchInput;
	}
}