package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.PropertyType;
import org.itkamer.controlpoint.repository.PropertyTypeRepository;

@Stateless
public class PropertyTypeEJB
{

   @Inject
   private PropertyTypeRepository repository;

   public PropertyType create(PropertyType entity)
   {
      return repository.save(attach(entity));
   }

   public PropertyType deleteById(Long id)
   {
      PropertyType entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public PropertyType update(PropertyType entity)
   {
      return repository.save(attach(entity));
   }

   public PropertyType findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<PropertyType> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<PropertyType> findBy(PropertyType entity, int start, int max, SingularAttribute<PropertyType, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(PropertyType entity, SingularAttribute<PropertyType, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<PropertyType> findByLike(PropertyType entity, int start, int max, SingularAttribute<PropertyType, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(PropertyType entity, SingularAttribute<PropertyType, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private PropertyType attach(PropertyType entity)
   {
      if (entity == null)
         return null;

      return entity;
   }
}
