package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.AppInstances;
import org.itkamer.controlpoint.repository.AppInstancesRepository;

public class AppInstancesMerger
{

   @Inject
   private AppInstancesRepository repository;

   public AppInstances bindComposed(AppInstances entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public AppInstances bindAggregated(AppInstances entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<AppInstances> entities)
   {
      if (entities == null)
         return;
      HashSet<AppInstances> oldCol = new HashSet<AppInstances>(entities);
      entities.clear();
      for (AppInstances entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<AppInstances> entities)
   {
      if (entities == null)
         return;
      HashSet<AppInstances> oldCol = new HashSet<AppInstances>(entities);
      entities.clear();
      for (AppInstances entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public AppInstances unbind(final AppInstances entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      AppInstances newEntity = new AppInstances();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<AppInstances> unbind(final Set<AppInstances> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<AppInstances>();
      //       HashSet<AppInstances> cache = new HashSet<AppInstances>(entities);
      //       entities.clear();
      //       for (AppInstances entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
