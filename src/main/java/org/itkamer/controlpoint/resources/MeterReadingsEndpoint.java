package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.model.MeterReadings;
import org.itkamer.controlpoint.model.MeterReadingsSearchInput;
import org.itkamer.controlpoint.model.MeterReadingsSearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/meterreadingss")
public class MeterReadingsEndpoint
{

   @Inject
   private MeterReadingsEJB ejb;

   @Inject
   private UnitMerger unitMerger;

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public MeterReadings create(MeterReadings entity)
   {
      return detach(ejb.create(entity));
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      MeterReadings deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public MeterReadings update(MeterReadings entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      MeterReadings found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(detach(found)).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public MeterReadingsSearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<MeterReadings> resultList = ejb.listAll(start, max);
      MeterReadingsSearchInput searchInput = new MeterReadingsSearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new MeterReadingsSearchResult((long) resultList.size(),
            detach(resultList), detach(searchInput));
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }

//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public MeterReadingsSearchResult findBy(MeterReadingsSearchInput searchInput)
//   {
//      SingularAttribute<MeterReadings, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<MeterReadings> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new MeterReadingsSearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(MeterReadingsSearchInput searchInput)
//   {
//      SingularAttribute<MeterReadings, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public MeterReadingsSearchResult findByLike(MeterReadingsSearchInput searchInput)
//   {
//      SingularAttribute<MeterReadings, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<MeterReadings> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new MeterReadingsSearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(MeterReadingsSearchInput searchInput)
//   {
//      SingularAttribute<MeterReadings, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<MeterReadings, ?>[] readSeachAttributes(
//         MeterReadingsSearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<MeterReadings, ?>> result = new ArrayList<SingularAttribute<MeterReadings, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = MeterReadings_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<MeterReadings, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private static final List<String> unitFields = emptyList;

   private MeterReadings detach(MeterReadings entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setUnit(unitMerger.unbind(entity.getUnit(), unitFields));

      return entity;
   }

   private List<MeterReadings> detach(List<MeterReadings> list)
   {
      if (list == null)
         return list;
      List<MeterReadings> result = new ArrayList<MeterReadings>();
      for (MeterReadings entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private MeterReadingsSearchInput detach(MeterReadingsSearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}