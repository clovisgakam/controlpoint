package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.TaskComment;
import org.itkamer.controlpoint.repository.TaskCommentRepository;

@Stateless
public class TaskCommentEJB
{

   @Inject
   private TaskCommentRepository repository;

   @Inject
   private TasksMerger tasksMerger;

   @Inject
   private LoginsMerger loginsMerger;

   public TaskComment create(TaskComment entity)
   {
      return repository.save(attach(entity));
   }

   public TaskComment deleteById(Long id)
   {
      TaskComment entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public TaskComment update(TaskComment entity)
   {
      return repository.save(attach(entity));
   }

   public TaskComment findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<TaskComment> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<TaskComment> findBy(TaskComment entity, int start, int max, SingularAttribute<TaskComment, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(TaskComment entity, SingularAttribute<TaskComment, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<TaskComment> findByLike(TaskComment entity, int start, int max, SingularAttribute<TaskComment, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(TaskComment entity, SingularAttribute<TaskComment, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private TaskComment attach(TaskComment entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setTask(tasksMerger.bindAggregated(entity.getTask()));

      // aggregated
      entity.setLogin(loginsMerger.bindAggregated(entity.getLogin()));

      return entity;
   }
}
