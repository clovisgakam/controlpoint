package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.Unit;
import org.itkamer.controlpoint.repository.UnitRepository;

@Stateless
public class UnitEJB
{

   @Inject
   private UnitRepository repository;

   @Inject
   private PropertysMerger propertysMerger;

   public Unit create(Unit entity)
   {
      return repository.save(attach(entity));
   }

   public Unit deleteById(Long id)
   {
      Unit entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public Unit update(Unit entity)
   {
      return repository.save(attach(entity));
   }

   public Unit findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<Unit> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<Unit> findBy(Unit entity, int start, int max, SingularAttribute<Unit, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(Unit entity, SingularAttribute<Unit, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<Unit> findByLike(Unit entity, int start, int max, SingularAttribute<Unit, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(Unit entity, SingularAttribute<Unit, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private Unit attach(Unit entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setProperty(propertysMerger.bindAggregated(entity.getProperty()));

      return entity;
   }
}
