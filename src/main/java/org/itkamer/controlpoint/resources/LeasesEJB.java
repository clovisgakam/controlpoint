package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.Leases;
import org.itkamer.controlpoint.repository.LeasesRepository;

@Stateless
public class LeasesEJB
{

   @Inject
   private LeasesRepository repository;

   @Inject
   private UnitMerger unitMerger;

   @Inject
   private LoginsMerger loginsMerger;

   public Leases create(Leases entity)
   {
      return repository.save(attach(entity));
   }

   public Leases deleteById(Long id)
   {
      Leases entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public Leases update(Leases entity)
   {
      return repository.save(attach(entity));
   }

   public Leases findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<Leases> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<Leases> findBy(Leases entity, int start, int max, SingularAttribute<Leases, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(Leases entity, SingularAttribute<Leases, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<Leases> findByLike(Leases entity, int start, int max, SingularAttribute<Leases, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(Leases entity, SingularAttribute<Leases, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private Leases attach(Leases entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setUnit(unitMerger.bindAggregated(entity.getUnit()));

      // aggregated
      entity.setLogin(loginsMerger.bindAggregated(entity.getLogin()));

      return entity;
   }
}
