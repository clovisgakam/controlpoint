package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.AttachedFile;
import org.itkamer.controlpoint.repository.AttachedFileRepository;

public class AttachedFileMerger
{

   @Inject
   private AttachedFileRepository repository;

   public AttachedFile bindComposed(AttachedFile entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public AttachedFile bindAggregated(AttachedFile entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<AttachedFile> entities)
   {
      if (entities == null)
         return;
      HashSet<AttachedFile> oldCol = new HashSet<AttachedFile>(entities);
      entities.clear();
      for (AttachedFile entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<AttachedFile> entities)
   {
      if (entities == null)
         return;
      HashSet<AttachedFile> oldCol = new HashSet<AttachedFile>(entities);
      entities.clear();
      for (AttachedFile entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public AttachedFile unbind(final AttachedFile entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      AttachedFile newEntity = new AttachedFile();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<AttachedFile> unbind(final Set<AttachedFile> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<AttachedFile>();
      //       HashSet<AttachedFile> cache = new HashSet<AttachedFile>(entities);
      //       entities.clear();
      //       for (AttachedFile entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
