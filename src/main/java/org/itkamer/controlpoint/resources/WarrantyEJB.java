package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.Warranty;
import org.itkamer.controlpoint.repository.WarrantyRepository;

@Stateless
public class WarrantyEJB
{

   @Inject
   private WarrantyRepository repository;

   @Inject
   private ScheduledEventMerger scheduledEventMerger;

   @Inject
   private VendorsMerger vendorsMerger;

   public Warranty create(Warranty entity)
   {
      return repository.save(attach(entity));
   }

   public Warranty deleteById(Long id)
   {
      Warranty entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public Warranty update(Warranty entity)
   {
      return repository.save(attach(entity));
   }

   public Warranty findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<Warranty> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<Warranty> findBy(Warranty entity, int start, int max, SingularAttribute<Warranty, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(Warranty entity, SingularAttribute<Warranty, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<Warranty> findByLike(Warranty entity, int start, int max, SingularAttribute<Warranty, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(Warranty entity, SingularAttribute<Warranty, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private Warranty attach(Warranty entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setScheduledEvent(scheduledEventMerger.bindAggregated(entity.getScheduledEvent()));

      // aggregated
      entity.setVendor(vendorsMerger.bindAggregated(entity.getVendor()));

      return entity;
   }
}
