package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.Quotations;
import org.itkamer.controlpoint.repository.QuotationsRepository;

@Stateless
public class QuotationsEJB
{

   @Inject
   private QuotationsRepository repository;

   @Inject
   private TasksMerger tasksMerger;

   @Inject
   private VendorsMerger vendorsMerger;

   public Quotations create(Quotations entity)
   {
      return repository.save(attach(entity));
   }

   public Quotations deleteById(Long id)
   {
      Quotations entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public Quotations update(Quotations entity)
   {
      return repository.save(attach(entity));
   }

   public Quotations findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<Quotations> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<Quotations> findBy(Quotations entity, int start, int max, SingularAttribute<Quotations, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(Quotations entity, SingularAttribute<Quotations, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<Quotations> findByLike(Quotations entity, int start, int max, SingularAttribute<Quotations, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(Quotations entity, SingularAttribute<Quotations, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private Quotations attach(Quotations entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setVendor(vendorsMerger.bindAggregated(entity.getVendor()));

      // aggregated
      entity.setTask(tasksMerger.bindAggregated(entity.getTask()));

      return entity;
   }
}
