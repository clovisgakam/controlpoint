package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.Finances;
import org.itkamer.controlpoint.repository.FinancesRepository;

public class FinancesMerger
{

   @Inject
   private FinancesRepository repository;

   public Finances bindComposed(Finances entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public Finances bindAggregated(Finances entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<Finances> entities)
   {
      if (entities == null)
         return;
      HashSet<Finances> oldCol = new HashSet<Finances>(entities);
      entities.clear();
      for (Finances entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<Finances> entities)
   {
      if (entities == null)
         return;
      HashSet<Finances> oldCol = new HashSet<Finances>(entities);
      entities.clear();
      for (Finances entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public Finances unbind(final Finances entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      Finances newEntity = new Finances();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<Finances> unbind(final Set<Finances> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<Finances>();
      //       HashSet<Finances> cache = new HashSet<Finances>(entities);
      //       entities.clear();
      //       for (Finances entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
