package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.repository.LoginsRepository;

public class LoginsMerger
{

   @Inject
   private LoginsRepository repository;

   public Logins bindComposed(Logins entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public Logins bindAggregated(Logins entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<Logins> entities)
   {
      if (entities == null)
         return;
      HashSet<Logins> oldCol = new HashSet<Logins>(entities);
      entities.clear();
      for (Logins entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<Logins> entities)
   {
      if (entities == null)
         return;
      HashSet<Logins> oldCol = new HashSet<Logins>(entities);
      entities.clear();
      for (Logins entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public Logins unbind(final Logins entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      Logins newEntity = new Logins();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<Logins> unbind(final Set<Logins> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<Logins>();
      //       HashSet<Logins> cache = new HashSet<Logins>(entities);
      //       entities.clear();
      //       for (Logins entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
