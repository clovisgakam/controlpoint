package org.itkamer.controlpoint.resources;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.itkamer.controlpoint.model.UserRole;
import org.itkamer.controlpoint.model.UserRoleSearchInput;
import org.itkamer.controlpoint.model.UserRoleSearchResult;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/userroles")
public class UserRoleEndpoint
{

   @Inject
   private UserRoleEJB ejb;

   @Inject
   private UnitMerger unitMerger;

   @Inject
   private PropertysMerger propertysMerger;

   @Inject
   private LoginsMerger loginsMerger;

   @POST
   @Consumes({ "application/json", "application/xml" })
   @Produces({ "application/json", "application/xml" })
   public UserRole create(UserRole entity)
   {
      return detach(ejb.create(entity));
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      UserRole deleted = ejb.deleteById(id);
      if (deleted == null)
         return Response.status(Status.NOT_FOUND).build();

      return Response.ok(detach(deleted)).build();
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   @Consumes({ "application/json", "application/xml" })
   public UserRole update(UserRole entity)
   {
      return detach(ejb.update(entity));
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces({ "application/json", "application/xml" })
   public Response findById(@PathParam("id") Long id)
   {
      UserRole found = ejb.findById(id);
      if (found == null)
         return Response.status(Status.NOT_FOUND).build();
      return Response.ok(detach(found)).build();
   }

   @GET
   @Produces({ "application/json", "application/xml" })
   public UserRoleSearchResult listAll(@QueryParam("start") int start,
         @QueryParam("max") int max)
   {
      List<UserRole> resultList = ejb.listAll(start, max);
      UserRoleSearchInput searchInput = new UserRoleSearchInput();
      searchInput.setStart(start);
      searchInput.setMax(max);
      return new UserRoleSearchResult((long) resultList.size(),
            detach(resultList), detach(searchInput));
   }

   @GET
   @Path("/count")
   public Long count()
   {
      return ejb.count();
   }
//
//   @POST
//   @Path("/findBy")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public UserRoleSearchResult findBy(UserRoleSearchInput searchInput)
//   {
//      SingularAttribute<UserRole, ?>[] attributes = readSeachAttributes(searchInput);
//      Long count = ejb.countBy(searchInput.getEntity(), attributes);
//      List<UserRole> resultList = ejb.findBy(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new UserRoleSearchResult(count, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countBy")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countBy(UserRoleSearchInput searchInput)
//   {
//      SingularAttribute<UserRole, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countBy(searchInput.getEntity(), attributes);
//   }
//
//   @POST
//   @Path("/findByLike")
//   @Produces({ "application/json", "application/xml" })
//   @Consumes({ "application/json", "application/xml" })
//   public UserRoleSearchResult findByLike(UserRoleSearchInput searchInput)
//   {
//      SingularAttribute<UserRole, ?>[] attributes = readSeachAttributes(searchInput);
//      Long countLike = ejb.countByLike(searchInput.getEntity(), attributes);
//      List<UserRole> resultList = ejb.findByLike(searchInput.getEntity(),
//            searchInput.getStart(), searchInput.getMax(), attributes);
//      return new UserRoleSearchResult(countLike, detach(resultList),
//            detach(searchInput));
//   }
//
//   @POST
//   @Path("/countByLike")
//   @Consumes({ "application/json", "application/xml" })
//   public Long countByLike(UserRoleSearchInput searchInput)
//   {
//      SingularAttribute<UserRole, ?>[] attributes = readSeachAttributes(searchInput);
//      return ejb.countByLike(searchInput.getEntity(), attributes);
//   }
//
//   @SuppressWarnings("unchecked")
//   private SingularAttribute<UserRole, ?>[] readSeachAttributes(
//         UserRoleSearchInput searchInput)
//   {
//      List<String> fieldNames = searchInput.getFieldNames();
//      List<SingularAttribute<UserRole, ?>> result = new ArrayList<SingularAttribute<UserRole, ?>>();
//      for (String fieldName : fieldNames)
//      {
//         Field[] fields = UserRole_.class.getFields();
//         for (Field field : fields)
//         {
//            if (field.getName().equals(fieldName))
//            {
//               try
//               {
//                  result.add((SingularAttribute<UserRole, ?>) field.get(null));
//               }
//               catch (IllegalArgumentException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//               catch (IllegalAccessException e)
//               {
//                  throw new IllegalStateException(e);
//               }
//            }
//         }
//      }
//      return result.toArray(new SingularAttribute[result.size()]);
//   }

   private static final List<String> emptyList = Collections.emptyList();

   private static final List<String> loginFields = emptyList;

   private static final List<String> unitFields = emptyList;

   private static final List<String> propertyFields = emptyList;

   private UserRole detach(UserRole entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setLogin(loginsMerger.unbind(entity.getLogin(), loginFields));

      // aggregated
      entity.setUnit(unitMerger.unbind(entity.getUnit(), unitFields));

      // aggregated
      entity.setProperty(propertysMerger.unbind(entity.getProperty(), propertyFields));

      return entity;
   }

   private List<UserRole> detach(List<UserRole> list)
   {
      if (list == null)
         return list;
      List<UserRole> result = new ArrayList<UserRole>();
      for (UserRole entity : list)
      {
         result.add(detach(entity));
      }
      return result;
   }

   private UserRoleSearchInput detach(UserRoleSearchInput searchInput)
   {
      searchInput.setEntity(detach(searchInput.getEntity()));
      return searchInput;
   }
}