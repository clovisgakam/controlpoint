package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.MeterReadings;
import org.itkamer.controlpoint.repository.MeterReadingsRepository;

@Stateless
public class MeterReadingsEJB
{

   @Inject
   private MeterReadingsRepository repository;

   @Inject
   private UnitMerger unitMerger;

   public MeterReadings create(MeterReadings entity)
   {
      return repository.save(attach(entity));
   }

   public MeterReadings deleteById(Long id)
   {
      MeterReadings entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public MeterReadings update(MeterReadings entity)
   {
      return repository.save(attach(entity));
   }

   public MeterReadings findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<MeterReadings> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<MeterReadings> findBy(MeterReadings entity, int start, int max, SingularAttribute<MeterReadings, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(MeterReadings entity, SingularAttribute<MeterReadings, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<MeterReadings> findByLike(MeterReadings entity, int start, int max, SingularAttribute<MeterReadings, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(MeterReadings entity, SingularAttribute<MeterReadings, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private MeterReadings attach(MeterReadings entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setUnit(unitMerger.bindAggregated(entity.getUnit()));

      return entity;
   }
}
