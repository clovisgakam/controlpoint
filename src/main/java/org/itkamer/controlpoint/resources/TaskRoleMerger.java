package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.TaskRole;
import org.itkamer.controlpoint.repository.TaskRoleRepository;

public class TaskRoleMerger
{

   @Inject
   private TaskRoleRepository repository;

   public TaskRole bindComposed(TaskRole entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public TaskRole bindAggregated(TaskRole entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<TaskRole> entities)
   {
      if (entities == null)
         return;
      HashSet<TaskRole> oldCol = new HashSet<TaskRole>(entities);
      entities.clear();
      for (TaskRole entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<TaskRole> entities)
   {
      if (entities == null)
         return;
      HashSet<TaskRole> oldCol = new HashSet<TaskRole>(entities);
      entities.clear();
      for (TaskRole entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public TaskRole unbind(final TaskRole entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      TaskRole newEntity = new TaskRole();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<TaskRole> unbind(final Set<TaskRole> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<TaskRole>();
      //       HashSet<TaskRole> cache = new HashSet<TaskRole>(entities);
      //       entities.clear();
      //       for (TaskRole entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
