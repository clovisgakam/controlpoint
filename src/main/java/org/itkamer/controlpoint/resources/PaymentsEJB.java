package org.itkamer.controlpoint.resources;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.metamodel.SingularAttribute;

import org.itkamer.controlpoint.model.Payments;
import org.itkamer.controlpoint.repository.PaymentsRepository;

@Stateless
public class PaymentsEJB
{

   @Inject
   private PaymentsRepository repository;

   @Inject
   private QuotationsMerger quotationsMerger;

   public Payments create(Payments entity)
   {
      return repository.save(attach(entity));
   }

   public Payments deleteById(Long id)
   {
      Payments entity = repository.findBy(id);
      if (entity != null)
      {
         repository.remove(entity);
      }
      return entity;
   }

   public Payments update(Payments entity)
   {
      return repository.save(attach(entity));
   }

   public Payments findById(Long id)
   {
      return repository.findBy(id);
   }

   public List<Payments> listAll(int start, int max)
   {
      return repository.findAll(start, max);
   }

   public Long count()
   {
      return repository.count();
   }

   public List<Payments> findBy(Payments entity, int start, int max, SingularAttribute<Payments, ?>[] attributes)
   {
      return repository.findBy(entity, start, max, attributes);
   }

   public Long countBy(Payments entity, SingularAttribute<Payments, ?>[] attributes)
   {
      return repository.count(entity, attributes);
   }

   public List<Payments> findByLike(Payments entity, int start, int max, SingularAttribute<Payments, ?>[] attributes)
   {
      return repository.findByLike(entity, start, max, attributes);
   }

   public Long countByLike(Payments entity, SingularAttribute<Payments, ?>[] attributes)
   {
      return repository.countLike(entity, attributes);
   }

   private Payments attach(Payments entity)
   {
      if (entity == null)
         return null;

      // aggregated
      entity.setQuotation(quotationsMerger.bindAggregated(entity.getQuotation()));

      return entity;
   }
}
