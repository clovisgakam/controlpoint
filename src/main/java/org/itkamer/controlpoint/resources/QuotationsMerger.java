package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.Quotations;
import org.itkamer.controlpoint.repository.QuotationsRepository;

public class QuotationsMerger
{

   @Inject
   private QuotationsRepository repository;

   public Quotations bindComposed(Quotations entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public Quotations bindAggregated(Quotations entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<Quotations> entities)
   {
      if (entities == null)
         return;
      HashSet<Quotations> oldCol = new HashSet<Quotations>(entities);
      entities.clear();
      for (Quotations entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<Quotations> entities)
   {
      if (entities == null)
         return;
      HashSet<Quotations> oldCol = new HashSet<Quotations>(entities);
      entities.clear();
      for (Quotations entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public Quotations unbind(final Quotations entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      Quotations newEntity = new Quotations();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<Quotations> unbind(final Set<Quotations> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<Quotations>();
      //       HashSet<Quotations> cache = new HashSet<Quotations>(entities);
      //       entities.clear();
      //       for (Quotations entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
