package org.itkamer.controlpoint.resources;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.itkamer.controlpoint.model.Leases;
import org.itkamer.controlpoint.repository.LeasesRepository;

public class LeasesMerger
{

   @Inject
   private LeasesRepository repository;

   public Leases bindComposed(Leases entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return entity;
      return repository.save(entity);
   }

   public Leases bindAggregated(Leases entity)
   {
      if (entity == null)
         return null;
      if (entity.getId() == null)
         return null;
      return repository.findBy(entity.getId());
   }

   public void bindComposed(final Set<Leases> entities)
   {
      if (entities == null)
         return;
      HashSet<Leases> oldCol = new HashSet<Leases>(entities);
      entities.clear();
      for (Leases entity : oldCol)
      {
         entity = bindComposed(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public void bindAggregated(final Set<Leases> entities)
   {
      if (entities == null)
         return;
      HashSet<Leases> oldCol = new HashSet<Leases>(entities);
      entities.clear();
      for (Leases entity : oldCol)
      {
         entity = bindAggregated(entity);
         if (entity != null)
            entities.add(entity);
      }
   }

   public Leases unbind(final Leases entity, List<String> fieldList)
   {
      if (entity == null)
         return null;
      Leases newEntity = new Leases();
      newEntity.setId(entity.getId());
      newEntity.setVersion(entity.getVersion());
      MergerUtils.copyFields(entity, newEntity, fieldList);
      return newEntity;
   }

   public Set<Leases> unbind(final Set<Leases> entities, List<String> fieldList)
   {
      if (entities == null)
         return null;
      return new HashSet<Leases>();
      //       HashSet<Leases> cache = new HashSet<Leases>(entities);
      //       entities.clear();
      //       for (Leases entity : cache) {
      //  		entities.add(unbind(entity, fieldList));
      //       }
      //      return entities;
   }
}
