package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.AttachedFile;

@Repository(forEntity = AttachedFile.class)
public interface AttachedFileRepository extends EntityRepository<AttachedFile, Long>
{
}
