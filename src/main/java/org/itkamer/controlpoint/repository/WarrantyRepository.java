package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.Warranty;

@Repository(forEntity = Warranty.class)
public interface WarrantyRepository extends EntityRepository<Warranty, Long>
{
}
