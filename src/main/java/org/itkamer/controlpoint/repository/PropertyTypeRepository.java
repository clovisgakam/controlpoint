package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.PropertyType;

@Repository(forEntity=PropertyType.class)
public interface PropertyTypeRepository extends EntityRepository<PropertyType, Long>{
}
