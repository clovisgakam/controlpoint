package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.MeterReadings;

@Repository(forEntity = MeterReadings.class)
public interface MeterReadingsRepository extends EntityRepository<MeterReadings, Long>
{
}
