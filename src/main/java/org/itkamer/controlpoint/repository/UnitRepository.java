package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.Unit;

@Repository(forEntity = Unit.class)
public interface UnitRepository extends EntityRepository<Unit, Long>
{
}
