package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.Vendors;

@Repository(forEntity = Vendors.class)
public interface VendorsRepository extends EntityRepository<Vendors, Long>
{
}
