package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.TaskRole;

@Repository(forEntity = TaskRole.class)
public interface TaskRoleRepository extends EntityRepository<TaskRole, Long>
{
}
