package org.itkamer.controlpoint.repository;

import java.util.Date;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.AppInstances;
import org.itkamer.controlpoint.model.LoginStatus;
import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.Propertys;
import org.itkamer.controlpoint.model.UserRole;

@Repository(forEntity = Propertys.class)
public interface PropertysRepository extends EntityRepository<Propertys, Long>
{
	@Query("SELECT e.property FROM  UserRole AS e WHERE e.login = ?1 AND e.status = ?2 AND (e.end IS NULL OR e.end >= ?3)")
	QueryResult<Propertys> findPropertyByUser(Logins login,
			LoginStatus loginStatus, Date endDate);
	
	@Query("SELECT e FROM  Propertys AS e WHERE e.appInstance = ?1 ")
	QueryResult<Propertys> findPropertyByAppInstance(AppInstances appInstance);
	
	
}
