package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.TaskComment;

@Repository(forEntity = TaskComment.class)
public interface TaskCommentRepository extends EntityRepository<TaskComment, Long>
{
}
