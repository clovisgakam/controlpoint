package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.Tasks;

@Repository(forEntity = Tasks.class)
public interface TasksRepository extends EntityRepository<Tasks, Long>
{
	QueryResult<Tasks> findByRefNumber(String refNumber);
}
