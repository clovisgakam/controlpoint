package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.Company;

@Repository(forEntity = Company.class)
public interface CompanyRepository extends EntityRepository<Company, Long>
{
}
