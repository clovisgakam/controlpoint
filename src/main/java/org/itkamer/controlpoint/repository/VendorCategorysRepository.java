package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.VendorCategorys;

@Repository(forEntity=VendorCategorys.class)
public interface VendorCategorysRepository extends EntityRepository<VendorCategorys, Long>{
}
