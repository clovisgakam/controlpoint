package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.AppInstances;
import org.itkamer.controlpoint.model.LoginStatus;
import org.itkamer.controlpoint.model.Logins;

@Repository(forEntity=Logins.class)
public interface LoginsRepository extends EntityRepository<Logins, Long>{
	
	QueryResult<Logins> findByEmail(String email);
	
	QueryResult<Logins> findByAppInstance(AppInstances appInstances);
	
	QueryResult<Logins> findByAppInstanceAndStatus(AppInstances appInstances,LoginStatus status);
	
	
}
