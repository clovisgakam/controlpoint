package org.itkamer.controlpoint.repository;

import java.util.Date;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.LoginStatus;
import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.model.Propertys;
import org.itkamer.controlpoint.model.RoleNames;
import org.itkamer.controlpoint.model.UserPermission;
import org.itkamer.controlpoint.model.UserRole;

@Repository(forEntity = UserRole.class)
public interface UserRoleRepository extends EntityRepository<UserRole, Long> {
	@Query("SELECT e FROM  UserRole AS e WHERE e.login = ?1 AND e.status = ?2 AND (e.end IS NULL OR e.end >= ?3)")
	QueryResult<UserRole> findActiveUserRoleByLogin(Logins login,
			LoginStatus loginStatus, Date endDate);

	@Query("SELECT e FROM UserRole AS e WHERE e.login = ?1 AND e.role = ?2 AND e.status = ?3")
	public QueryResult<UserRole> findPropertyByAdminLogin(Logins login,
			RoleNames role, LoginStatus status);
	
	@Query("SELECT e FROM UserRole AS e WHERE e.login = ?1 AND e.property = ?2")
	public QueryResult<UserRole> findPermissionByLogin(Logins login, Propertys property);
	
	@Query("SELECT e FROM UserRole AS e WHERE e.property = ?1 AND e.status = ?2")
	public QueryResult<UserRole> findActiveUserRoleByProperty(Propertys property, LoginStatus loginStatus);

	@Query("SELECT e FROM UserRole AS e WHERE e.property = ?1 ")
	public QueryResult<UserRole> findUserRoleByProperty(Propertys property);

}
