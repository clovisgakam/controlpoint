package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.Payments;

@Repository(forEntity=Payments.class)
public interface PaymentsRepository extends EntityRepository<Payments, Long>{
}
