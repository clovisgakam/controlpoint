package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.Address;

@Repository(forEntity = Address.class)
public interface AddressRepository extends EntityRepository<Address, Long>
{
}
