package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.Finances;

@Repository(forEntity=Finances.class)
public interface FinancesRepository extends EntityRepository<Finances, Long>{
}
