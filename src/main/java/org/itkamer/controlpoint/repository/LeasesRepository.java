package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.Leases;

@Repository(forEntity=Leases.class)
public interface LeasesRepository extends EntityRepository<Leases, Long>{
}
