package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.TaskCategorys;

@Repository(forEntity=TaskCategorys.class)
public interface TaskCategorysRepository extends EntityRepository<TaskCategorys, Long>{
}
