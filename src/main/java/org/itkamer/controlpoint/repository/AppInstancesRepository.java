package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.AppInstances;

@Repository(forEntity = AppInstances.class)
public interface AppInstancesRepository extends EntityRepository<AppInstances, Long>
{
}
