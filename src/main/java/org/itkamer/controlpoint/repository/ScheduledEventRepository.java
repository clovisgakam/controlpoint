package org.itkamer.controlpoint.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.itkamer.controlpoint.model.ScheduledEvent;

@Repository(forEntity = ScheduledEvent.class)
public interface ScheduledEventRepository extends EntityRepository<ScheduledEvent, Long>
{
}
