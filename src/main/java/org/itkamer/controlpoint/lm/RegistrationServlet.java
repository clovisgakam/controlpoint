package org.itkamer.controlpoint.lm;

import java.io.IOException;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.time.DateUtils;
import org.itkamer.controlpoint.model.Address;
import org.itkamer.controlpoint.model.AppInstances;
import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.resources.AddressEJB;
import org.itkamer.controlpoint.resources.AppInstancesEJB;
import org.itkamer.controlpoint.resources.LoginsEJB;
import org.itkamer.controlpoint.security.SecurityUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class RegistrationServlet.
 */
@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet
{

	@Inject
	private LoginsEJB loginEJB ;

	@Inject
	private AppInstancesEJB appInstanceEJB ;

	@Inject 
	private SecurityUtil  securityUtil ;

	@Inject
	private AddressEJB addressEJB ;

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6646314403682818472L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException
			{
		// retreive information 
		String company = req.getParameter("company");
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String mobile = req.getParameter("mobile");
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		String country = req.getParameter("country");
		String city = req.getParameter("city");
		if(loginEJB.findByEmail(email)!=null){
			resp.sendError(resp.SC_INTERNAL_SERVER_ERROR);
			return;
		};

		//create Addresse
		Address address = new Address();
		address.setCountry(country);
		address.setCity(city);
		address = addressEJB.create(address);

		// create AppInstance
		AppInstances instance = new AppInstances();
		instance.setCompany(company);
		instance.setStart(new Date());
		instance.setEnd(DateUtils.addDays(new Date(), 30));
		instance.setTrial(true);
		instance.setAddress(address);
		instance = appInstanceEJB.create(instance);

		// create Manager 
		Logins login = new Logins();
		login.setAppInstance(instance);
		login.setAddress(address);
		login.setEmail(email);
		login.setPassword(password);
		login.setFirstName(firstname);
		login.setLastName(lastname);
		login.setMobilePhone(mobile);
		login.setRegistered(Boolean.TRUE);
		loginEJB.registerNewUser(login);
		resp.setStatus(resp.SC_OK);

			}
}
