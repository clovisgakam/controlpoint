package org.itkamer.controlpoint.lm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginFailledServlet.
 */
@WebServlet("/access-failed")
public class AccessFailledServelt extends HttpServlet
{

   /** The Constant serialVersionUID. */
   private static final long serialVersionUID = 394802460939755622L;

   /* (non-Javadoc)
    * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
    */
   @Override
   protected void service(HttpServletRequest req, HttpServletResponse resp)
         throws ServletException, IOException
   {
	   String loginError = (String) req.getAttribute("LOGIN_ERROR");
	   resp.addHeader("X-LOGIN_ERROR", loginError);
       resp.sendError(HttpServletResponse.SC_FORBIDDEN);
   }

}