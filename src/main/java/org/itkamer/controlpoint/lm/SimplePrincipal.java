package org.itkamer.controlpoint.lm;

import java.io.Serializable;
import java.security.Principal;

/**
 * The Class SimplePrincipal.
 */
public class SimplePrincipal implements Principal, Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2221888790136398728L;
	
	/** The name. */
	private final String name;

	/** The workspace id. */
	private String workspaceId;
	
	private String terminalId;

	/**
	 * Instantiates a new simple principal.
	 * 
	 * @param name
	 *            the name
	 */
	public SimplePrincipal(String name) {
		this.name = name;
	}

	/**
	 * Gets the workspace id.
	 * 
	 * @return the workspace id
	 */
	public String getWorkspaceId() {
		return workspaceId;
	}

	/**
	 * Sets the workspace id.
	 * 
	 * @param workspaceId
	 *            the new workspace id
	 */
	public void setWorkspaceId(String workspaceId) {
		this.workspaceId = workspaceId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	/* (non-Javadoc)
	 * @see java.security.Principal#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		/*
		 * We accept any principal with the same name.
		 * 
		 * We do not allow principal with name==null.
		 */
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (name == null)
			return false;
		if (!(obj instanceof Principal))
			return false;
		Principal other = Principal.class.cast(obj);
		return name.equals(other.getName());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SimplePrincipal [name=" + name + "]";
	}

}
