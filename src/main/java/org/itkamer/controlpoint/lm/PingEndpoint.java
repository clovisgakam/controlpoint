package org.itkamer.controlpoint.lm;

import java.util.Date;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

// TODO: Auto-generated Javadoc
/**
 * Ping authenticated.
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path("/ping")
public class PingEndpoint
{

   /**
	 * Authping.
	 * 
	 * @return the string
	 */
   @GET
   @Produces({ "application/json"})
   public String authping()
   {
      return new Date().toString();
   }
}