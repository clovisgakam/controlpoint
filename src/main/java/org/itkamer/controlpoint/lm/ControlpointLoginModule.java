

package org.itkamer.controlpoint.lm;

import java.io.IOException;
import java.security.Principal;
import java.security.acl.Group;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import org.itkamer.controlpoint.model.LoginStatus;
import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.resources.LoginsEJB;



/**
 * The Class CaretakerLoginModule.
 */
public class ControlpointLoginModule implements LoginModule
{

	/** The subject. */
	protected Subject subject;

	/** The callback handler. */
	protected CallbackHandler callbackHandler;

	/** The shared state. */
	protected Map<String, ?> sharedState;

	/** The options. */
	protected Map<String, ?> options;

	/** The log. */
	protected Logger log;

	/** The trace. */
	protected boolean trace = false;


	/**
	 * Flag indicating if the login phase succeeded. Subclasses that override
	 * the login method must set this to true on successful completion of login
	 */
	protected boolean loginOk;

	/** the principal to use when a null username and password are seen. */
	protected Principal unauthenticatedIdentity;

	/** The login ejb. */
	private LoginsEJB loginEJB;

	/** The account. */
	private Logins account;

	/* (non-Javadoc)
	 * @see javax.security.auth.spi.LoginModule#initialize(javax.security.auth.Subject, javax.security.auth.callback.CallbackHandler, java.util.Map, java.util.Map)
	 */
	public void initialize(Subject subject, CallbackHandler callbackHandler,
			Map<String, ?> sharedState, Map<String, ?> options)
	{

		InitialContext initialContext;
		try{
			initialContext = new InitialContext();
			loginEJB = (LoginsEJB) initialContext.lookup("java:module/LoginsEJB");
		}catch (NamingException e1){

			throw new IllegalStateException(e1);

		}

		this.subject = subject;
		this.callbackHandler = callbackHandler;
		this.sharedState = sharedState;
		this.options = options;
		log = Logger.getLogger(getClass().getName());
		trace = log.isLoggable(Level.FINER);
		if (trace)
		{
			log.finer("initialize");

			// log securityDomain, if set.
			log.finer("Security domain: "
					+ (String) options
					.get(SecurityConstants.SECURITY_DOMAIN_OPTION));
		}

		// Check for unauthenticatedIdentity option.
		String name = (String) options.get("unauthenticatedIdentity");
		if (name != null)
		{
			try
			{
				unauthenticatedIdentity = new SimplePrincipal(name);
				if (trace)
					log.finer("Saw unauthenticatedIdentity = " + name);
			}
			catch (Exception e)
			{
				log.warning("Failed to create custom unauthenticatedIdentity: "
						+ e.getMessage());
			}
		}
	}

	/* (non-Javadoc)
	 * @see javax.security.auth.spi.LoginModule#login()
	 */
	public boolean login() throws LoginException
	{

		//		HttpServletRequest   = null ;
		String ipAdresse  = null ;
		String sessionId = null ;
		String termName = "" ;
		//		try {
		//			request  = (HttpServletRequest) PolicyContext.getContext(HttpServletRequest.class.getName());
		//		} catch (PolicyContextException e1) {
		//			e1.printStackTrace();
		//		}

		NameCallback nameCallback = new NameCallback("Enter your user name: ");
		PasswordCallback passwordCallback = new PasswordCallback(
				"Enter your password", false);
		try
		{
			callbackHandler.handle(new Callback[] { nameCallback,
					passwordCallback });
		}
		catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
		catch (UnsupportedCallbackException e)
		{
			throw new IllegalStateException(e);
		}
		Date currentDate = new Date();
		Logins retrievedAccount = retrieveAccount(nameCallback.getName());
		//		if(retrievedAccount==null){
		//			if(request!=null)
		//				request.setAttribute("LOGIN_ERROR", MessagesKeys.LOGIN_FAILLED_ERROR.name());
		//			return false ;
		//		}
		//		if(retrievedAccount.getAccountExpiration() !=null && currentDate.after(retrievedAccount.getAccountExpiration())){
		//			if(request!=null)
		//				request.setAttribute("LOGIN_ERROR", MessagesKeys.ACCOUNT_EXPIRATION_ERROR.name());
		//			return false ;
		//		}
		//		if(retrievedAccount.getCredentialExpiration()!=null && currentDate.after(retrievedAccount.getCredentialExpiration())){
		//			if(request!=null)
		//				request.setAttribute("LOGIN_ERROR", MessagesKeys.CREDENTIAL_EXPIRATION_ERROR.name());
		//			return false ;
		//		}
		//		if(retrievedAccount.getDisableLogin()){
		//			if(request!=null)
		//				request.setAttribute("LOGIN_ERROR", MessagesKeys.DISEABLE_LOGIN_ERROR.name());
		//			return false ;
		//		}
		//		if(retrievedAccount.getAccountLocked()){
		//			if(request!=null)
		//				request.setAttribute("LOGIN_ERROR", MessagesKeys.ACCOUNT_LOCKED_ERROR.name());
		//			return false ;
		//		}

		if(retrievedAccount.getAccountLocked()|| retrievedAccount.getDisableLogin()||
				(retrievedAccount.getCredentialExpiration()!=null && currentDate.after(retrievedAccount.getCredentialExpiration())) ||
				(retrievedAccount.getAccountExpiration() !=null && currentDate.after(retrievedAccount.getAccountExpiration()))) 
			return false ;
		if(LoginStatus.CLOSED.equals(retrievedAccount.getStatus()))
			return false ;

		char[] password = passwordCallback.getPassword();
		if (!loginEJB.checkPassword(new String(password), retrievedAccount.getPassword())){
			//			if(request!=null)
			//				request.setAttribute("LOGIN_ERROR", MessagesKeys.LOGIN_FAILLED_ERROR.name());
			return false ;
		}

		account=retrievedAccount;
		return true;
	}


	/* (non-Javadoc)
	 * @see javax.security.auth.spi.LoginModule#commit()
	 */
	public boolean commit() throws LoginException
	{
		if (account == null)
			return false;

		/*
		 * The set of principals of this subject. We will add the 
		 * SecurityConstants.CALLER_PRINCIPAL_GROUP and the 
		 * SecurityConstants.ROLES_GROUP to this set.
		 */
		Set<Principal> principals = subject.getPrincipals();

		/*
		 * The user identity.
		 */
		Principal identity = new SimplePrincipal(account.getEmail());
		principals.add(identity);

		// get the CallerPrincipal group
		Group callerGroup = findGroup(SecurityConstants.CALLER_PRINCIPAL_GROUP, principals);
		if (callerGroup == null)
		{
			callerGroup = new SimpleGroup(SecurityConstants.CALLER_PRINCIPAL_GROUP);
			principals.add(callerGroup);
		}
		// Add this principal to the group.
		callerGroup.addMember(identity);

		// get the Roles group
		Group[] roleSets = getRoleSets();
		for (Group group : roleSets)
		{
			Group sunjectGroup = findGroup(group.getName(), principals);
			if (sunjectGroup == null)
			{
				sunjectGroup = new SimpleGroup(group.getName());
				principals.add(sunjectGroup);
			}
			// Copy the group members to the Subject group
			Enumeration<? extends Principal> members = group.members();
			while (members.hasMoreElements())
			{
				Principal role = (Principal) members.nextElement();
				sunjectGroup.addMember(role);
			}
		}

		return true;

	}

	/* (non-Javadoc)
	 * @see javax.security.auth.spi.LoginModule#abort()
	 */
	public boolean abort() throws LoginException
	{
		if (trace)
			log.finer("abort");
		account = null;
		return true;
	}

	/* (non-Javadoc)
	 * @see javax.security.auth.spi.LoginModule#logout()
	 */
	public boolean logout() throws LoginException
	{
		if (trace)
			log.finer("logout");

		// Remove all principals and groups of classes defined here.
		Set<Principal> principals = subject.getPrincipals();
		Set<SimplePrincipal> principals2Remove = subject.getPrincipals(SimplePrincipal.class);
		principals.removeAll(principals2Remove);

		return true;
	}

	/**
	 * Retrieve account.
	 * 
	 * @param loginName
	 *            the login name
	 * @return the login
	 * @throws FailedLoginException
	 *             the failed login exception
	 */
	private Logins retrieveAccount(String email) throws FailedLoginException
	{
		return loginEJB.findByEmail(email);
	}

	/**
	 * Gets the role sets.
	 * 
	 * @return the role sets
	 */
	private Group[] getRoleSets()
	{
		SimpleGroup simpleGroup = new SimpleGroup(SecurityConstants.ROLES_GROUP);
		simpleGroup.addMember(new SimplePrincipal("PUBLIC"));
		return new Group[] { simpleGroup };
	}

	/**
	 * Find group.
	 * 
	 * @param name
	 *            the name
	 * @param principals
	 *            the principals
	 * @return the group
	 */
	private Group findGroup(String name, Set<Principal> principals)
	{
		for (Principal principal : principals)
		{
			if (!(principal instanceof Group))
				continue;
			Group group = Group.class.cast(principal);
			if (name.equals(group.getName()))
				return group;
		}
		return null;
	}


}
