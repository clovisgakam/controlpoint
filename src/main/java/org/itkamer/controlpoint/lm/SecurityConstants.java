package org.itkamer.controlpoint.lm;

// TODO: Auto-generated Javadoc
/**
 * The Class SecurityConstants.
 */
public class SecurityConstants
{

   /**
    * The String option name used to pass in the security-domain name the
    * LoginModule was configured in.
    */
   public static String SECURITY_DOMAIN_OPTION = "jboss.security.security_domain";
   
   /** The caller principal group. */
   public static String CALLER_PRINCIPAL_GROUP = "CallerPrincipal";
   
   /** The roles group. */
   public static String ROLES_GROUP = "Roles";
}
