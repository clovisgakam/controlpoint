package org.itkamer.controlpoint.lm;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.itkamer.controlpoint.model.RoleNames;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving declarativeRolesContext events. The
 * class that is interested in processing a declarativeRolesContext event
 * implements this interface, and the object created with that class is
 * registered with a component using the component's
 * <code>addDeclarativeRolesContextListener</code> method. When
 * the declarativeRolesContext event occurs, that object's appropriate
 * method is invoked.
 * 
 */
@WebListener
public class DeclarativeRolesContextListener implements ServletContextListener
{

   /* (non-Javadoc)
    * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
    */
   public void contextInitialized(ServletContextEvent sce)
   {
      ServletContext servletContext = sce.getServletContext();
      RoleNames[] roleEnums = RoleNames.values();
      for (RoleNames roleEnum : roleEnums)
      {
         servletContext.declareRoles(roleEnum.name());
      }
   }

   /* (non-Javadoc)
    * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
    */
   public void contextDestroyed(ServletContextEvent sce)
   {
      // TODO Auto-generated method stub

   }

}
