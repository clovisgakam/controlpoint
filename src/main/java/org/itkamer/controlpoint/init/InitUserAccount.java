package org.itkamer.controlpoint.init;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.itkamer.controlpoint.model.LoginStatus;
import org.itkamer.controlpoint.model.Logins;
import org.itkamer.controlpoint.resources.LoginsEJB;

@Singleton
@Startup
public class InitUserAccount {

	@Inject
	private LoginsEJB loginEJB ;

	@PostConstruct
	public void onPostconstruct(){
		if(loginEJB.count()<1){
			Logins login = new Logins();
			login.setEmail("clovis.gakam@itkamer.com");
			login.setPassword(loginEJB.encodePassword("admin"));
			login.setFirstName("Gakam");
			login.setLastName("Clovis");
			login.setAdmin(Boolean.TRUE);
			login.setStatus(LoginStatus.ACTIVE);
			loginEJB.registerNewUser(login);
		}
	}
}
