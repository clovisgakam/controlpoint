/**
 * @author Fouomene Boris
 * @desc taskNote note services
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('TaskNote',TaskNote);
    TaskNote.$inject = ['$http','$q','$log','AppContext'];
    function TaskNote($http,$q,$log,AppContext){
        var urlBase = AppContext+'/rest/tasknotes';

        var taskNote ={};

        taskNote.update = function(entity){
            return $http.put(urlBase+'/'+entity.id,entity);
        };
        taskNote.create = function(entity){
            return $http.post(urlBase,entity);
        };
        taskNote.findById = function(id){
            return  $http.get(urlBase+'/'+id);
        }
        taskNote.findBy = function(searchInput){
            return  $http.post(urlBase+'/findBy',searchInput);
        }
        taskNote.findByLike = function(searchInput){
            return  $http.post(urlBase+'/findByLike',searchInput);
        }
        taskNote.deleteById = function(id){
            return  $http.delete(urlBase+'/'+id);
        }

        taskNote.listAll = function(){
            return  $http.get(urlBase);
        }

        return taskNote ;
    }

})();
