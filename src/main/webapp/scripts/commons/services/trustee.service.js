/**
 * @author clovis.gakam
 * @name Trustee Service
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('Trustee',Trustee);
    Trustee.$inject = ['$http','$q','$log','AppContext'];
    function Trustee($http,$q,$log,AppContext){
        var urlBase = AppContext+'/rest/trustees';

        var trustee ={};

        trustee.update = function(entity){
            return $http.put(urlBase+'/'+entity.id,entity);
        };
        trustee.create = function(entity){
            return $http.post(urlBase,entity);
        };
        trustee.findById = function(id){
            return  $http.get(urlBase+'/'+id);
        }
        trustee.findBy = function(searchInput){
            return  $http.post(urlBase+'/findBy',searchInput);
        }
        trustee.findByLike = function(searchInput){
            return  $http.post(urlBase+'/findByLike',searchInput);
        }
        trustee.deleteById = function(id){
            return  $http.delete(urlBase+'/'+id);
        }
        trustee.findAll = function(){
            return  $http.get(urlBase);
        }




        return trustee ;
    }

})();
