/**
 * @author clovis.gakam
 * @name Vendor Service
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('Vendor',Vendor);
    Vendor.$inject = ['$http','$q','$log','AppContext'];
    function Vendor($http,$q,$log,AppContext){
        var urlBase = AppContext+'/rest/vendors';

        var vendor ={};

        vendor.update = function(entity){
            return $http.put(urlBase+'/'+entity.id,entity);
        };
        vendor.create = function(entity){
            return $http.post(urlBase,entity);
        };
        vendor.findById = function(id){
            return  $http.get(urlBase+'/'+id);
        }
        vendor.findBy = function(searchInput){
            return  $http.post(urlBase+'/findBy',searchInput);
        }
        vendor.findByLike = function(searchInput){
            return  $http.post(urlBase+'/findByLike',searchInput);
        }
        vendor.deleteById = function(id){
            return  $http.delete(urlBase+'/'+id);
        }
        vendor.findAll = function(){
            return  $http.get(urlBase);
        }




        return vendor ;
    }

})();
