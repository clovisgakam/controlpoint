

/**
 * @author clovis.gakam
 * @name Vendor Service
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('Property',Property);
    Property.$inject = ['$http','$q','$log','AppContext'];

    function Property($http,$q,$log,AppContext){

        var urlBase = AppContext+'/rest/propertys';

        var property = {};

        property.update = function(entity){
            return $http.put(urlBase+'/'+entity.id,entity);
        };
        property.newProperty = function(entity){
            return $http.post(urlBase+"/newProperty",entity);
        };
        property.findById = function(id){
            return  $http.get(urlBase+'/'+id);
        };
        property.deleteById = function(id){
            return  $http.delete(urlBase+'/'+id);
        };
        property.findByAppInstance = function(searchInput){
            return  $http.get(urlBase+'/findByAppInstance',searchInput);
        };
        property.propertyPortefollio = function(){
            return  $http.get(urlBase+'/propertyPortefollio');
        };

        //add to retrieve all property By Hermann Tchimkap
        property.listAll = function(){
            return  $http.get(urlBase);
        }
        property.findLoginsPropertys = function(searchInput){
            return  $http.get(urlBase+'/loginsAdminProp',searchInput);
        };
        property.findLoginsPropertysPermission = function(searchInput){
            return  $http.get(urlBase+'/loginPermission',searchInput);
        };
        property.findLoginsPropertysPermission = function(searchInput){
            return  $http.get(urlBase+'/loginPropertysActive',searchInput);
        };

        return property ;
    }

})();

