/**
 * @author clovis.gakam
 * @name Vendor Service
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('VendorCategory',VendorCategory);
    VendorCategory.$inject = ['$http','$q','$log','AppContext'];
    function VendorCategory($http,$q,$log,AppContext){
        var urlBase = AppContext+'/rest/vendorcategorys';

        var vendorCat ={};

        vendorCat.create = function(entity){
            return $http.post(urlBase,entity);
        };
        vendorCat.listAll = function(){
            return  $http.get(urlBase);
        }
        return vendorCat ;
    }

})();
