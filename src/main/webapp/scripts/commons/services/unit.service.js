/**
 *@author: hermann Tchimkap 
 */
(function(){
    'use strict'
    angular.module('caretaker').factory('Unit',Unit);
    Unit.$inject = ['$http','$q','$log','AppContext'];
    function Unit($http,$q,$log,AppContext){
        var urlBase = AppContext+'/rest/propertyunits';

        var unit ={};

        unit.update = function(entity){
            return $http.put(urlBase+'/'+entity.id,entity);
        };
        unit.create = function(entity){
            return $http.post(urlBase,entity);
        };
        unit.findById = function(id){
            return  $http.get(urlBase+'/'+id);
        }
        unit.findBy = function(searchInput){
            return  $http.post(urlBase+'/findBy',searchInput);
        }
        unit.findByLike = function(searchInput){
            return  $http.post(urlBase+'/findByLike',searchInput);
        }
        unit.deleteById = function(id){
            return  $http.delete(urlBase+'/'+id);
        }
        unit.listAll = function(){
            return  $http.get(urlBase);
        }


        return unit ;
    }

})();
