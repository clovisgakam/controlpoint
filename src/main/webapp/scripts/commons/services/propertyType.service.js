/**
 * @author clovis.gakam
 * @name PropertyType Service
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('PropertyType',PropertyType);
    PropertyType.$inject = ['$http','$q','$log','AppContext'];

    function PropertyType($http,$q,$log,AppContext){

        var urlBase = AppContext+'/rest/propertytypes';

        var propertyType = {};

        propertyType.update = function(entity){
            return $http.put(urlBase+'/'+entity.id,entity);
        };
        propertyType.create = function(entity){
            return $http.post(urlBase,entity);
        };
        propertyType.findById = function(id){
            return  $http.get(urlBase+'/'+id);
        };
        propertyType.listAll = function(){
            return  $http.get(urlBase);
        }

        return propertyType ;
    }

})();

