/**
 * @author dassi orleando
 * @name Address Service
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('Address',Address);
    Address.$inject = ['$http','$q','$log','AppContext'];
    function Address($http,$q,$log,AppContext){
        var urlBase = AppContext+'/rest/addresss';

        var addresse ={};

        addresse.create = function(entity){
            return $http.post(urlBase,entity);
        };
        addresse.listAll = function(){
            return  $http.get(urlBase);
        }
        return addresse ;
    }

})();
