/**
 * @author Boris fouomene
 * @name User Service
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('User',User);
    angular.module('caretaker').factory('UserRole',UserRole);
    User.$inject = ['$http','$q','$log','AppContext'];
    UserRole.$inject = ['$http','$q','$log','AppContext'];
    
    function User($http,$q,$log,AppContext){
    	
        var urlBase = AppContext+'/rest/logins';

        var user = {};

        user.update = function(entity) {
            return $http.put(urlBase+'/'+entity.id,entity);
        };
        user.create = function(entity){
            return $http.post(urlBase,entity);
        };
        user.findById = function(id){
            return  $http.get(urlBase+'/'+id);
        }
        user.findBy = function(searchInput){
            return  $http.post(urlBase+'/findBy',searchInput);
        }
        user.findByLike = function(searchInput){
            return  $http.post(urlBase+'/findByLike',searchInput);
        }
        
        user.deleteById = function (id) {
            //return  $http.delete(urlBase+'/'+id);
        }
        
        user.listAll = function(){
            return  $http.get(urlBase);
        }
        user.findActiveLogin = function(){
            return  $http.get(urlBase+'/findActiveLogin');
        }
        user.findByAppInstance = function(){
            return  $http.get(urlBase+'/findByAppInstance');
        }

        return user ;
    }
    
    
	function UserRole($http,$q,$log,AppContext){
    	
        var urlBase = AppContext+'/rest/userRolerole';

        var userRole = {};

        userRole.update = function(entity) {
            return $http.put(urlBase+'/'+entity.id,entity);
        };
        
        userRole.create = function(entity){
            return $http.post(urlBase,entity);
        };
        userRole.findById = function(id) {
            return  $http.get(urlBase+'/'+id);
        }
        userRole.findBy = function(searchInput){
            return  $http.post(urlBase+'/findBy',searchInput);
        }
        userRole.findByLike = function(searchInput){
            return  $http.post(urlBase+'/findByLike',searchInput);
        }
        
        userRole.deleteById = function (id) {
            //return  $http.delete(urlBase+'/'+id);
        }
        
        userRole.listAll = function(){
            return  $http.get(urlBase);
        }

        return userRole ;
    }


})();
