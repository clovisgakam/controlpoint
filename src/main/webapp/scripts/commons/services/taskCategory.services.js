/**
 * @author ibrahima.yaya
 * @desc task category services
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('TaskCategory',TaskCategory);
    TaskCategory.$inject = ['$http','$q','$log','AppContext'];
    function TaskCategory($http,$q,$log,AppContext){
        var urlBase = AppContext+'/rest/taskcategorys';

        var taskCategory ={};

        taskCategory.update = function(entity){
            return $http.put(urlBase+'/'+entity.id,entity);
        };
        taskCategory.create = function(entity){
        	/*Response res = taskCategory.findById(entity.id);
        	if (res.getStatus() != -1){
        		return $http.post(urlBase,entity);
        	} else {
        		return $http.post(urlBase,entity);
        	} */
        	return $http.post(urlBase,entity);
        };
        taskCategory.findById = function(id){
            return  $http.get(urlBase+'/'+id);
        }
        taskCategory.findBy = function(searchInput){
            return  $http.post(urlBase+'/findBy',searchInput);
        }
        taskCategory.findByLike = function(searchInput){
            return  $http.post(urlBase+'/findByLike',searchInput);
        }
        taskCategory.deleteById = function(id){
            return  $http.delete(urlBase+'/'+id);
        }
        
        taskCategory.listAll = function(){
            return  $http.get(urlBase);
        }

        return taskCategory ;
    }

})();