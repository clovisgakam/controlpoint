/**
 * @author clovis.gakam
 * @name Instance Service
 * @desc
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('Instance',Instance);
    Instance.$inject = ['$http','$q','$log','AppContext'];
    function Instance($http,$q,$log,AppContext){
    	
        var urlBase = AppContext+'/rest/appinstances',
        	searchInput = {
                entity:{},
                start:0,
                max:100,
                fieldNames:[]
            };
            
        var instance ={};
        
        instance.update = function(entity){
            return $http.put(urlBase+'/'+entity.id,entity);
        };
        instance.findById = function(id){
            return  $http.get(urlBase+'/'+id);
        }
        instance.findBy = function(searchInput){
            return  $http.get(urlBase+'/findBy',searchInput);
        }
        instance.findByLike = function(searchInput){
            return  $http.post(urlBase+'/findByLike',searchInput);
        }
        
        instance.findAll = function(start,max){
            return  $http.get(urlBase, {params: {start: start, max: max}});
        }

        return instance ;
    }

})();
