/**
 * @author clovis.gakam
 * @name Login Service
 * @desc
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('Login',Login);
    Login.$inject = ['$http','$q','$log','AppContext'];
    function Login($http,$q,$log,AppContext){
        var urlBase = AppContext+'/rest/logins';
        var login ={};
        login.currentUser = function(){
            return $http.get(urlBase+'/current-user');
        };
        login.ping = function(){
            return  $http.get(AppContext+'/resources/ping');
        };
        login.isValidConnectedUserPassword = function(password){
            return $http.get(urlBase+'/isValidConnectedUserPassword',password);
        };
        login.updateCurrentUserPass = function(newPassword){
            return $http.get(urlBase+'/updateCurrentUserPass',newPassword);
        };
        return login ;
    }

})();
