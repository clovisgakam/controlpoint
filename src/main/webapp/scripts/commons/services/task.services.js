/**
 * @author ibrahima.yaya
 * @desc task services
 */
(function(){
    'use strict';
    angular.module('caretaker').factory('Task',Task);
    Task.$inject = ['$http','$q','$log','AppContext'];
    function Task($http,$q,$log,AppContext){
        var urlBase = AppContext+'/rest/tasks';

        var task ={};
       
        task.update = function(entity){
            return $http.put(urlBase+'/'+entity.id,entity);
        };
        task.create = function(entity){
            return $http.post(urlBase,entity);
        };
        task.findById = function(id){
            return  $http.get(urlBase+'/'+id);
        }
        task.findBy = function(searchInput){
            return  $http.post(urlBase+'/findBy',searchInput);
        }
        task.findByLike = function(searchInput){
            return  $http.post(urlBase+'/findByLike',searchInput);
        }
        task.deleteById = function(id){
            return  $http.delete(urlBase+'/'+id);
        }
        

        return task ;
    }

})();
