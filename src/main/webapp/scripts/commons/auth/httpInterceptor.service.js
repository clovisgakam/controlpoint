/**
 * @author clovis.gakam
 * @name HttpInterceptor
 *
 */

(function () {
    'use strict';
  var   httpInterceptor = function ($q, $state) {
        return function (promise) {
            var success = function (response) {
                return response;
            };

            var error = function (response) {
                // caretaker server react on 403 forbiden and not on 4 a01. We can keep both.
                if (response.status === 401 || response.status === 403) {
                    // Forward to login page and return response after setting the location, if not
                    // the application will display an error.
                    $state.go('public.login');
                    return response;
                }else{
                    return $q.reject(response);
                }

            };
            return promise.then(success, error);
        };
    };
    httpInterceptor.$inject = ['$q','$state'];
    angular.module('caretaker').factory('httpInterceptor', httpInterceptor);
})();