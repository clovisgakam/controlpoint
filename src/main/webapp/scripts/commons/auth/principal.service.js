/**
 * @author clovis.gakam
 */

(function(){
   'use strict';
    angular.module('caretaker').factory('Principal',Principal);
    Principal.$inject = ['$q','Login'];
        function Principal($q, Login) {
            var _identity,
                _authenticated = false;

            return {
                isIdentityResolved: function () {
                    return angular.isDefined(_identity);
                },
                isAuthenticated: function () {
                    return _authenticated;
                },
                isInRole: function (role) {
                    if (!_authenticated || !_identity || !_identity.roleAndPerms) {
                        return false;
                    }
                 var rap = _identity.roleAndPerms ;
                    for (var i = 0; i < rap.length; i++) {
                        if(rap[i].role === role)
                        return true ;
                    }
                    //return _identity.roles.indexOf(role) !== -1;
                    return false ;
                },
                isInRoleAndPerm: function (roleAndPerm) {
                    if (!_authenticated || !_identity || !_identity.roleAndPerms) {
                        return false;
                    }
                    var rap = _identity.roleAndPerms ;
                    for (var i = 0; i < rap.length; i++) {
                        var rolePerm =rap[i].role+":"+rap[i].perm ;
                        if(rolePerm === roleAndPerm)
                            return true ;
                    }
                    //return _identity.roles.indexOf(role) !== -1;
                    return false ;
                },
                isInAnyRole: function (roles) {
                    if (!_authenticated || !_identity.roleAndPerms) {
                        return false;
                    }

                    for (var i = 0; i < roles.length; i++) {
                        if (this.isInRole(roles[i])) {
                            return true;
                        }
                    }

                    return false;
                },
                isInAnyRoleAndPerms: function (roleAndPerms) {
                    if (!_authenticated || !_identity.roleAndPerms) {
                        return false;
                    }

                    for (var i = 0; i < roleAndPerms.length; i++) {
                        if (this.isInRoleAndPerm(roleAndPerms[i])) {
                            return true;
                        }
                    }

                    return false;
                },
                authenticate: function (identity) {
                    var deferred= $q.defer();
                    _identity = identity;
                    _authenticated = identity !== null;
                    deferred.resolve(_identity);
                    return deferred.promise;
                },
                clearIdentity: function(){
                    _identity = undefined;
                    _authenticated = false;

                },
                resolveHomePage: function(){
                    var homeSate = 'public.login' ;
                    if(this.isInRole('ADMINISTRATOR'))
                        homeSate = 'instanceList';
                    if(this.isInRole('PROPERTY_MANAGER'))
                        homeSate = 'propertyPortefolio';
                    return homeSate ;
                },
                getUser: function(){
                    return _identity ;
                } ,
                identity: function (force) {
                    var deferred = $q.defer();

                    if (force === true) {
                        _identity = undefined;
                    }

                    // check and see if we have retrieved the identity data from the server.
                    // if we have, reuse it by immediately resolving
                    if (angular.isDefined(_identity)) {
                        deferred.resolve(_identity);

                        return deferred.promise;
                    }

                    // retrieve the identity data from the server, update the identity object, and then resolve.
                    Login.currentUser().$promise
                        .then(function (principal) {
                            _identity = principal;
                            _authenticated = true;
                            deferred.resolve(_identity);
                        }).catch(function() {
                            _identity = null;
                            _authenticated = false;
                            deferred.resolve(_identity);
                        });
                    return deferred.promise;
                }

            };
        };
})();
