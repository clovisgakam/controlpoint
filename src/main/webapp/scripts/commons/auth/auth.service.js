
(function(){
    'use strict';
    angular.module('caretaker').factory('Auth',Auth);
    Auth.$inject = ['$http','$q','$rootScope','Login','Principal','$state','AppContext','growl'];
    function Auth($http,$q,$rootScope,Login,Principal,$state,AppContext,growl){
        var auth = {};
        var urlBase = AppContext;
        auth.login = function(credential){
            return  $http({
                method: 'POST',
                url: urlBase+'/j_security_check',
                data: jQuery.param(credential),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        }
        auth.register = function(data){
            return  $http({
                method: 'POST',
                url: urlBase+'/registration',
                data: jQuery.param(data),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        }
        auth.logout = function(){
          return   $http.get(urlBase+'/logout') ;
        }

        auth.currentUser = function currentUser(){
            return Login.currentUser();
        }
        auth.ping = function(){
            return Login.ping();
        }
        auth.authorize= function(force) {
            return Principal.identity(force)
                .then(function() {
                    var isAuthenticated = Principal.isAuthenticated();

                    if ($rootScope.toState.data.roles && $rootScope.toState.data.roles.length > 0 && !Principal.isInAnyRole($rootScope.toState.data.roles)) {
                        if (isAuthenticated) {
                            // user is signed in but not authorized for desired state
                            growl.error('Access denied !');
                            $state.go($rootScope.previousStateName,$rootScope.previousStateParams);
                        }
                        else {
                           auth.authentificate();
                        }
                    }
                });
        }
        auth.authentificate = function(){
            // user is not authenticated. stow the state they wanted before you
            // send them to the signin state, so you can return them when you're done
            $rootScope.returnToState = $rootScope.toState;
            $rootScope.returnToStateParams = $rootScope.toStateParams;
            // now, send them to the signin state so they can log in
            $state.go('public.login');
        }
        return auth ;
    }
})();
