/**
 * @author clovis.gakam
 * @desMainCtrl - controller
 */
(function(){
function MainCtrl(Principal,Auth,$state) {

    this.userName = Principal.identity.username;
    this.logout = function() {
        Auth.logout().then(function(data){
           Principal.clearIdentity();
            $state.go('public.login');
        });
	}
    this.hasAnyRole = function(roles){
        if(angular.isArray(roles))
           return Principal.isInAnyRole(roles);
        return Principal.isInRole(roles);
    };


};
MainCtrl.$inject = ['Principal','Auth','$state'];

angular
    .module('caretaker')
    .controller('MainCtrl', MainCtrl);
})();