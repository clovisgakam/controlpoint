/**
 * @author ibrahima.yaya
 * @desc tasks config
 */
(function() {
	'use strict';
	angular.module('caretaker').config(function($stateProvider) {
		$stateProvider.state('taskList', {
			parent : 'index',
			url : '/tasks-list',
			data : {
				roles : [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
				pageTitle : 'Task list'
			},
			controller : "TaskListCtrl as taskList",
			templateUrl : "scripts/app/tasks/task.list.html"

		}).state('taskOverview', {
			parent : 'index',
			url : '/task-overview:taskId',
			data : {
				roles : [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
				pageTitle : 'Task Overview'
			},
			controller : "TaskOverviewCtrl as taskOverview",
			templateUrl : "scripts/app/tasks/task.overview.html"

		}).state('taskDetails', {
			parent : 'index',
			url : '/task-details:taskId',
			data : {
				roles : [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
				pageTitle : 'Task Details'
			},
			controller : "TaskDetailsCtrl as taskDetails",
			templateUrl : "scripts/app/tasks/task.form.html"

		}).state('taskCategoryList', {
			parent : 'index',
			url : '/task-categories-list',
			data : {
				roles : [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
				pageTitle : 'Task categories'
			},
			controller : "TaskCategoryListCtrl as taskCategoryList",
			templateUrl : "scripts/app/tasks/category.list.html"

		}).state('taskCategoryForm', {
			parent : 'index',
			url : '/task-categories-form:taskCategoryId',
			data : {
				roles : [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
				pageTitle : 'Task categories form'
			},
			controller : "TaskCategoryFormCtrl as taskCategoryForm",
			templateUrl : "scripts/app/tasks/category.form.html"

		})
		.state('taskNoteList', {
			parent : 'index',
			url : '/tasks-notes-list',
			data : {
				roles : [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
				pageTitle : 'Task Notes list'
			},
			controller : "TaskNoteCtrl as taskNoteList",
			templateUrl : "scripts/app/tasks/task.notes.list.html"

		})
		
		.state('taskNoteOverview', {
			parent : 'taskNoteList',
			url : '/task-note-overview:taskId',
			data : {
				roles : [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
				pageTitle : 'Task Note Overview'
			},
			controller : "TaskNoteCtrl as taskNote",
			templateUrl : "scripts/app/tasks/task.notes.form.html"

		})
		
		
	});

})();
