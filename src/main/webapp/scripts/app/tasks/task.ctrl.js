/**
 * @author ibrahima.yaya
 * @desc tasks controller
 */
(function() {
	'use strict'
	angular.module('caretaker').controller('TaskListCtrl', TaskListCtrl);
	angular.module('caretaker').controller('TaskDetailsCtrl', TaskDetailsCtrl);
	angular.module('caretaker').controller('TaskOverviewCtrl', 	TaskOverviewCtrl);
	angular.module('caretaker').controller('TaskCategoryListCtrl',
			TaskCategoryListCtrl);
	angular.module('caretaker').controller('TaskCategoryFormCtrl',
			TaskCategoryFormCtrl);
	TaskListCtrl.$inject = [ 'Task', '$state', '$stateParams', 'growl' ];
	
	TaskDetailsCtrl.$inject = [ 'Task', '$state', '$stateParams', 'growl',
			'TaskCategory',"User", "TaskNote",'Unit','ModelValidator' ];

	TaskOverviewCtrl.$inject = [ 'Task', '$state', '$stateParams', 'growl',
	                			'TaskCategory',"User", "TaskNote",'Unit','ModelValidator' ];

	
	TaskCategoryListCtrl.$inject = [ 'TaskCategory', '$state', '$stateParams',
			'growl' ];
	TaskCategoryFormCtrl.$inject = [ 'TaskCategory', '$state', '$stateParams',
			'growl' ];

	// tasks List controller
	function TaskListCtrl(Task, $state, $stateParams, growl) {
		var vm = this;
		vm.searchInput = {
			start : 0,
			max : 50,
			entity : {},
			fieldNames : []
		};
		vm.tasks = [];

		var searchTaskSuccess = function(response) {
			vm.tasks = response.data.resultList;
		}
		var searchTaskError = function(response) {
            growl.error("no data found");
		}

		vm.searchTasks = function() {
			Task.findByLike(vm.searchInput).then(searchTaskSuccess,
					searchTaskError)
		}
		vm.editTask = function(task) {
			$state.go('taskDetails', {
				taskId : task.id
			});
		}
		
		vm.overviewOfTask = function(task) {
			$state.go('taskOverview', {
				taskId : task.id
			});
		}
		vm.removeTask = function(task) {
			Task.deleteById(task.id).then(function(response) {
				vm.searchTasks();
			});
		}
		vm.searchTasks();
	}

	// Task Details Controller
	function TaskDetailsCtrl(Task, $state, $stateParams, growl, TaskCategory, User, TaskNote,Unit, ModelValidator) {
		var vm = this;
		vm.taskId = $stateParams.taskId;
		vm.task = {};
		vm.taskNotes = {}
		vm.users = {}
		vm.isClosed = false;
		vm.isCompleted = false;
		
		vm.isUpdate = false;
		vm.saveButton = "Save";

		//updae Title
		$state.current.data.pageTitle = "New Task"
		
		vm.backToList = function () {
			$state.go("taskList")
		}
		
		vm.taskCategories = [];
		var success = function(response) {
			$state.go('taskList');
		};
		var error = function(response) {
            growl.error("error durring task creation");
		};

		vm.save = function() {
			ModelValidator.validateAll(vm.task, vm.rules).then(function(){
				if (angular.isNumber(vm.taskId)) {
					Task.update(vm.task).then(success, error)
				} else {
					Task.create(vm.task).then(success, error)
				}
			})
		}
		
		vm.cancel = function() {
			$state.go('taskCategoryList');
		}

		vm.addTaskCat = function() {
			var name = prompt("Task category");
			var category = {};
			category.name = name;

			TaskCategory.create(category).then(function(response) {
				vm.task.category = response.data;
				vm.populateCategory();
			})
		}
		
		vm.populateCategory = function() {
			TaskCategory.listAll().then(function(response) {
				vm.taskCategories = response.data.resultList;
			})
		}

		vm.populateUsers = function() {
			User.listAll().then(function(response) {
				vm.users = response.data.resultList;
			})
		}
		
		vm.populateTaskNotes = function() {
			TaskNote.listAll().then(function(response) {
				vm.taskNotes = response.data.resultList;
			})
		}
		
		
		vm.populateUnits = function() {
			Unit.listAll().then(function(response) {
				vm.units = response.data.resultList;
			})
		}
		
		vm.canDisableComplateAt = function () {
			return (vm.isClosed == false | vm.isCompleted == false );
		}
		
		vm.addUser = function () {
			$state.go("userDetails")
		}
		
		vm.rules = {
				category : {required : true},
				subject : {required : true},
				refNumber : {required : true},
				assignTo : {required: true},
				//status 
				//description,
				//imagesPaths,
		}
		
		
		vm.init = function() {
			vm.populateCategory();
			vm.populateUsers();
			vm.populateUnits();
			vm.populateTaskNotes ();
			if (angular.isNumber(vm.taskId)) {
				Task.findById(vm.taskId).then(function(response) {
					vm.task = response.data;
					vm.isUpdate = true;
					vm.saveButton = "Update";
					$state.current.data.pageTitle = "Update Task [ "+response.subject+" ]"
				})
			}

		}
		vm.init();
	}
	
	
	
	// Task Overview Controller
	function TaskOverviewCtrl (Task, $state, $stateParams, growl, TaskCategory, User, TaskNote,Unit, ModelValidator) {
		var vm = this;
		vm.taskId = $stateParams.taskId;
		
		vm.progress = {}
		
		//state overview
		vm.detailCreated = false;
		
		vm.task = {};
		vm.taskNotes = {}
		vm.users = {}
		vm.isClosed = false;
		vm.isCompleted = false;
		
		vm.isUpdate = false;
		vm.saveButton = "Save";

		//updae Title
		$state.current.data.pageTitle = "Task Overview"
		
		vm.backToList = function () {
			$state.go("taskList")
		}
		
		vm.taskCategories = [];
		var success = function(response) {
			$state.go('taskList');
		};
		var error = function(response) {
            growl.error("error durring task creation");
		};

		vm.save = function() {
			ModelValidator.validateAll(vm.task, vm.rules).then(function(){
				if (angular.isNumber(vm.taskId)) {
					Task.update(vm.task).then(success, error)
				} else {
					Task.create(vm.task).then(success, error)
				}
			})
		}
		
		vm.cancel = function() {
			$state.go('taskCategoryList');
		}

		vm.addTaskCat = function() {
			var name = prompt("Task category");
			var category = {};
			category.name = name;

			TaskCategory.create(category).then(function(response) {
				vm.task.category = response.data;
				vm.populateCategory();
			})
		}
		
		vm.populateCategory = function() {
			TaskCategory.listAll().then(function(response) {
				vm.taskCategories = response.data.resultList;
			})
		}

		vm.populateUsers = function() {
			User.listAll().then(function(response) {
				vm.users = response.data.resultList;
			})
		}
		
		vm.populateTaskNotes = function() {
			TaskNote.listAll().then(function(response) {
				vm.taskNotes = response.data.resultList;
			})
		}
		
		
		vm.populateUnits = function() {
			Unit.listAll().then(function(response) {
				vm.units = response.data.resultList;
			})
		}
		
		vm.canDisableComplateAt = function () {
			return (vm.isClosed == false | vm.isCompleted == false );
		}
		
		vm.addUser = function () {
			$state.go("userDetails")
		}
		
		vm.rules = {
				category : {required : true},
				subject : {required : true},
				refNumber : {required : true},
				assignTo : {required: true},
				//status 
				//description,
				//imagesPaths,
		}
		
		
		vm.init = function() {
			vm.populateCategory();
			vm.populateUsers();
			vm.populateUnits();
			vm.populateTaskNotes ();
			if (angular.isNumber(vm.taskId)) {
				Task.findById(vm.taskId).then(function(response) {
					vm.task = response.data;
					vm.isUpdate = true;
					vm.saveButton = "Update";
					$state.current.data.pageTitle = "Update Task [ "+response.subject+" ]"
				})
			}

		}
		vm.init();
	}
	
	
	

	// task categories List controller
	function TaskCategoryListCtrl(TaskCategory, $state, $stateParams, growl) {
		var vm = this;
		vm.searchInput = {
			start : 0,
			max : 50,
			entity : {},
			fieldNames : []
		};
		vm.taskCategories = [];

		var searchTaskCategorySuccess = function(response) {
			vm.taskCategories = response.data.resultList;
		}
		var searchTaskCategoryError = function(response) {
            growl.error("no data found");
		}

		vm.searchTaskCategories = function() {
			TaskCategory.findByLike(vm.searchInput).then(
					searchTaskCategorySuccess, searchTaskCategoryError)
		}
		vm.editTaskCategory = function(taskCategory) {
			$state.go('taskCategoryForm', {
				taskCategoryId : taskCategory.id
			});
		}
		vm.removeTaskCategory = function(taskCategory) {
			TaskCategory.deleteById(taskCategory.id).then(function(response) {
				vm.searchTaskCategories();
			});
		}
		vm.searchTaskCategories();
	}

	// Task Categories Form Controller
	function TaskCategoryFormCtrl(TaskCategory, $state, $stateParams, growl) {
		var vm = this;
		vm.taskCategoryId = $stateParams.taskCategoryId;
		vm.taskCategory = {};
		var success = function(response) {
			$state.go('taskCategoryList');
		};
		var error = function(response) {
            growl.error("error durring task category save");
		};

		vm.save = function() {
			if (angular.isNumber(vm.taskCategoryId)) {
				TaskCategory.update(vm.taskCategory).then(success, error)
			} else {
				TaskCategory.create(vm.taskCategory).then(success, error)
			}
		}

		vm.cancel = function() {
			$state.go('taskCategoryList');
		}

		vm.init = function() {
			if (angular.isNumber(vm.taskCategoryId)) {
				TaskCategory.findById(vm.taskCategoryId).then(
						function(response) {
							vm.taskCategory = response.data;
						})
			}

		}
		vm.init();
	}

})();
