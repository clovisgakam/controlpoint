/**
 * @author clovis.gakam
 * @desc Properties config
 */
(function () {
    'use strict';
    angular.module('caretaker')
        .config(function ($stateProvider) {
            $stateProvider.state('propertyPortefolio', {
                    parent: 'index',
                    url: '/property-portfolio',
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    files: ['vendors/chartJs/Chart.min.js']
                                },
                                {
                                    name: 'angles',
                                    files: ['vendors/chartJs/angles.js']
                                },
                                {
                                    name: 'ui.knob',
                                    files: ['vendors/jsKnob/jquery.knob.js','vendors/jsKnob/angular-knob.js']
                                },
                                {
                                    name: 'datePicker',
                                    files: ['vendors/datapicker/angular-datapicker.css','vendors/datapicker/datePicker.js']
                                },
                                {
                                    files: ['vendors/dropzone/basic.css','vendors/dropzone/dropzone.css','vendors/dropzone/dropzone.js']
                                }
                            ]);
                        }
                    },
                    data: {
                        roles: [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
                        pageTitle: 'Property Portfolio'
                    },
                    templateUrl: "scripts/app/properties/portfolio.html",
                    controller: 'PropertyPortefolioCtrl as propertyCtrl'
                }).state('propertyCreate', {
                        parent: 'index',
                        url: '/property-create',
                        data: {
                            roles: [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
                            pageTitle: 'Property Creation'
                        },
                        templateUrl: "scripts/app/properties/property.create.html",
                        controller: 'PropertyCreateCtrl as propertyCreateCtrl'
                    }).state('propertyDetails', {
                        parent: 'index',
                        url: '/property-details/:propertyId',
                        data: {
                            roles: [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
                            pageTitle: 'Property Details'
                        },
                        templateUrl: "scripts/app/properties/property.details.html",
                        controller: 'PropertyDetailsCtrl as PropertyDetailsCtrl'
                    })
                    .state('parkWoodManor', {
                        parent: 'index',
                        url: '/parkwood-manor/:propertyId',
                        data: {
                            roles: [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
                            pageTitle: 'Control Point'
                        },
                        templateUrl: "scripts/app/properties/parkwood.details.html",
                        controller: 'PropertyDetailsCtrl as PropertyDetailsCtrl'
                    }).state('propertyShow', {
                        parent: 'index',
                        url: '/property-show/',
                        data: {
                            roles: [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
                            pageTitle: 'Control Point'
                        },
                        templateUrl: "scripts/app/properties/property.show.html",
                        controller: 'PropertyDetailsCtrl as PropertyDetailsCtrl'
                    })

        });
})();
