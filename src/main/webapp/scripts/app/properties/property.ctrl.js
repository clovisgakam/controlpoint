/**
 * Created by clovisgakam on Monday4/6/15.
 */

(function(){
    'use strict'


    /**
     * Portefolio controller
     * @param Principal
     * @param $state
     * @param Property
     * @constructor
     */
    function PropertyPortefolioCtrl(Principal,$state,Property,$rootScope,growl){
        var vm = this ;
        vm.portefolio = {} ;
        vm.dashboard = true ;
        vm.searchtext = "" ;
        vm.getPortefollio = function(){
            Property.propertyPortefollio().then(function(response){
            vm.portefolio = response.data ;
            })
        }
        vm.removeProperty  = function(property) {
            Property.deleteById(property.id).then(function (response) {
                vm.getPortefollio();
            }, function (response) {
                window.alert("erro");
            })
        }

        /**
         * Options for Bar chart
         */
        vm.barOptions = {
            scaleBeginAtZero : true,
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.05)",
            scaleGridLineWidth : 1,
            barShowStroke : true,
            barStrokeWidth : 2,
            barValueSpacing : 5,
            barDatasetSpacing : 1
        };

        /**
         * Data for Bar chart
         */
        vm.barData = {
            labels: ["J", "F", "M", "A", "M", "J", "Jl"],
            datasets: [
                {
                    label: "Profits",
                    fillColor: "rgba(220,220,220,0.5)",
                    strokeColor: "rgba(220,220,220,0.8)",
                    highlightFill: "rgba(220,220,220,0.75)",
                    highlightStroke: "rgba(220,220,220,1)",
                    data: [65, 59, 80, 81, 56, 55, 40]
                },
                {
                    label: "Lost",
                    fillColor: "rgba(26,179,148,0.5)",
                    strokeColor: "rgba(26,179,148,0.8)",
                    highlightFill: "rgba(26,179,148,0.75)",
                    highlightStroke: "rgba(26,179,148,1)",
                    data: [28, 48, 40, 19, 86, 27, 90]
                }
            ]
        };
        /**
         * Options for Bar chart
         */
        vm.barOptions = {
            scaleBeginAtZero : true,
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.05)",
            scaleGridLineWidth : 1,
            barShowStroke : true,
            barStrokeWidth : 2,
            barValueSpacing : 5,
            barDatasetSpacing : 1
        };

        /**
         * Data for Bar chart
         */
        vm.barData = {
            labels: ["J", "F", "M", "A", "M", "J", "Jl"],
            datasets: [
                {
                    label: "My Second dataset",
                    fillColor: "rgba(26,179,148,0.5)",
                    strokeColor: "rgba(26,179,148,0.8)",
                    highlightFill: "rgba(26,179,148,0.75)",
                    highlightStroke: "rgba(26,179,148,1)",
                    data: [28, 48, 40, 19, 86, 27, 90]
                },
                {
                    label: "My Second dataset",
                    fillColor: "rgba(220,220,220,0.5)",
                    strokeColor: "rgba(220,220,220,1)",
                    highlightFill: "rgba(26,179,148,0.75)",
                    highlightStroke: "rgba(26,179,148,1)",
                    data: [65, 59, 40, 51, 36, 25, 40]
                }
            ]
        };

        /**
         * Options for Line chart
         */
        vm.lineOptions = {
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.05)",
            scaleGridLineWidth : 1,
            bezierCurve : true,
            bezierCurveTension : 0.4,
            pointDot : true,
            pointDotRadius : 4,
            pointDotStrokeWidth : 1,
            pointHitDetectionRadius : 20,
            datasetStroke : true,
            datasetStrokeWidth : 2,
            datasetFill : true
        };
        vm.lineDataDashboard4 = {
            labels: ["J", "F", "M", "A", "M", "J", "Jl"],
            datasets: [
                {
                    label: "Example dataset",
                    fillColor: "rgba(220,220,220,0.5)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [65, 59, 40, 51, 36, 25, 40]
                },
                {
                    label: "Example dataset",
                    fillColor: "rgba(26,179,148,0.5)",
                    strokeColor: "rgba(26,179,148,0.7)",
                    pointColor: "rgba(26,179,148,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [48, 48, 60, 39, 56, 37, 30]
                }
            ]
        };

        $rootScope.$on("PROPERTY_CREATE_DONE_EVENT",function(event,data){
            vm.getPortefollio();
            event.stopPropagation();

        });

        vm.getPortefollio();
    }


    /**
     *  property details controller
     * @param $stateParams
     * @param $state
     * @param growl
     * @param Property
     * @constructor
     */
    function PropertyModalCtrl(growl, Property,PropertyType,$rootScope,$scope){
        var propMdl = this ;
        propMdl.sp = $rootScope ;
        propMdl.gl = growl ;
        propMdl.scope = $scope ;
        propMdl.ps = Property ;
        propMdl.pts = PropertyType ;
        propMdl.propertyTypes = [] ;

        propMdl.initData = function(){
            propMdl.propertyData = {
                property : {
                    address:{},
                    type:{}
                },
                users:[]
            }
        }
        propMdl.save = function(){
            Property.newProperty(propMdl.propertyData).then(function(response){
                $("#propertyModal").modal('hide');
                propMdl.initData();
                propMdl.scope.$emit("PROPERTY_CREATE_DONE_EVENT");
            })
        }
        propMdl.editPropType = function editPropType(){
            propMdl.sp.$broadcast("PROPERTY_TYPE_EDIT_EVENT",propMdl.propertyData.property.type);
        }
        var searchError =  function(response){
            growl.info("An error Append ");
        }
        propMdl.loadPropType = function(){
            propMdl.pts.listAll().then(function(response){
                propMdl.propertyTypes = response.data.resultList ;
            });
        }
            propMdl.sp.$on("PROPERTY_TYPE_CREATE_DONE_EVENT",function(event,propertyType){
                propMdl.propertyTypes.push(propertyType);
                event.stopPropagation();

            });
        propMdl.sp.$on("PROPERTY_USER_ADD_EVENT",function(event,userRole){
            propMdl.propertyData.users.push(userRole);
            event.stopPropagation();

        });

       propMdl.initData();
       propMdl.loadPropType();
    }

    //Property Type modal controller
    function PropertyTypeModalCtrl(growl, PropertyType,$scope){
        var propTypeMdl = this ;
        propTypeMdl.sp = $scope ;
        propTypeMdl.gl = growl ;
        propTypeMdl.pts = PropertyType ;
        propTypeMdl.propType = {} ;
        var saveSuccess =  function(response){
            propTypeMdl.sp.$emit("PROPERTY_TYPE_CREATE_DONE_EVENT",response.data);
            $("#propertyTypeModal").modal('hide');
            propTypeMdl.propType = {} ;
        }
        var saveError =  function(response){
            propTypeMdl.gl.error("Error During save !");
        }
        propTypeMdl.save = function(){
            propTypeMdl.pts.create(propTypeMdl.propType).then(saveSuccess,saveError);
        }
        propTypeMdl.sp.$on("PROPERTY_TYPE_EDIT_EVENT",function(event,propertyType){
            propTypeMdl.propType = propertyType;
            $("#propertyTypeModal").modal('show');
            event.stopPropagation();

        });



    }

    //Property User modal controller
    function PropertyUserModalCtrl(growl, PropertyType,$scope,User){
        var propUserMdl = this ;
        propUserMdl.sp = $scope ;
        propUserMdl.gl = growl ;
        propUserMdl.pts = PropertyType ;
        propUserMdl.Us = User ;
        propUserMdl.users = [] ;
        propUserMdl.user = {} ;
        propUserMdl.isCreateUser = false ;
        propUserMdl.userRole = {} ;
        var loadUserSuccess =  function(response){
            propUserMdl.users = response.data ;
        }
        var loadUserError =  function(response){
            propUserMdl.gl.error("Error During user loading !");
        }
        var createUserSuccess =  function(response){
            propUserMdl.users.push(response.data) ;
            propUserMdl.userRole.login = response.data ;
            propUserMdl.user = {} ;
           propUserMdl.cancelUserCreate();
        }
        var createUserError =  function(response){
            propUserMdl.gl.error("Error During user ceation !");
        }
        propUserMdl.save = function(){
            propUserMdl.sp.$emit("PROPERTY_USER_ADD_EVENT",propUserMdl.userRole);
            propUserMdl.userRole ={};
            $("#propertyUserModal").modal('hide');
        }
        propUserMdl.loadActiveUser = function(){
            propUserMdl.Us.findByAppInstance().then(loadUserSuccess,loadUserError);
        }
        propUserMdl.cancelUserCreate = function(){
            propUserMdl.isCreateUser = false ;
        }

        propUserMdl.activateUserCreate = function(){
            propUserMdl.isCreateUser = true ;
        }

        propUserMdl.saveUser = function(){
            propUserMdl.Us.create(propUserMdl.user).then(createUserSuccess,createUserError);
        }


        propUserMdl.getFullName = function(user){
            if(angular.isObject(user)){
                return user.firstName+" "+ user.lastName ;
            }else{
                return " "
            }

        }



        propUserMdl.sp.$on("PROPERTY_TYPE_EDIT_EVENT",function(event,propertyType){
            propUserMdl.propType = propertyType;
            $("#propertyTypeModal").modal('show');
            event.stopPropagation();

        });

        propUserMdl.loadActiveUser();

    }

    PropertyPortefolioCtrl.$inject = ['Principal','$state','Property','$rootScope','growl'];
    PropertyModalCtrl.$inject = ['growl', 'Property','PropertyType','$rootScope','$scope'];
    PropertyTypeModalCtrl.$inject = ['growl', 'PropertyType','$scope'];
    PropertyUserModalCtrl.$inject = ['growl', 'PropertyType','$scope','User'];

    angular.module('caretaker').controller('PropertyUserModalCtrl',PropertyUserModalCtrl);
    angular.module('caretaker').controller('PropertyTypeModalCtrl',PropertyTypeModalCtrl);
    angular.module('caretaker').controller('PropertyModalCtrl',PropertyModalCtrl);
    angular.module('caretaker').controller('PropertyPortefolioCtrl',PropertyPortefolioCtrl);


})();
    