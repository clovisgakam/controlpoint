/**
 * @author clovis.gakam
 * @desc vendors config
 */
(function(){
    'use strict';
    angular.module('caretaker')
        .config(function ($stateProvider) {
            $stateProvider
                .state('vendorList', {
                    parent: 'index',
                    url: '/vendors-list',
                    data: {
                        roles: [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
                        pageTitle: 'List of Vendors'
                    },
                    controller: "VendorListCtrl as vendorList",
                    templateUrl: "scripts/app/vendors/vendors.list.html"

                })
                .state('vendorDetails', {
                    parent: 'index',
                    url: '/vendors-details:vendorId',
                    data: {
                        roles: [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
                        pageTitle: 'Details of Vendor'
                    },
                    controller: "VendorDetailCtrl as vendorDetail",
                    templateUrl: "scripts/app/vendors/vendors.details.html"

                })

        });





})();
