/**
 * @author clovis.gakam
 */
(function(){
    'use strict'
    angular.module('caretaker').controller('VendorListCtrl',VendorListCtrl);
    angular.module('caretaker').controller('VendorDetailCtrl',VendorDetailCtrl);
    VendorListCtrl.$inject = ['Vendor','$state','$stateParams','growl'];
    VendorDetailCtrl.$inject = ['Vendor','$state','$stateParams','$window','VendorCategory','growl'];

    // vendor List controller
    function VendorListCtrl(Vendor,$state,$stateParams,growl){
        var vm = this ;
        vm.searchInput = {
            start:0,
            max:50,
            entity:{},
            fieldNames:[]
        };
        vm.vendors = [];

        var searchVendorSuccess =  function(response){
          vm.vendors = response.data.resultList ;
        }
        var searchVendorError =  function(response){
            growl.error("no data found");
        }

        vm.searchVendors = function(){
            Vendor.findAll().then(searchVendorSuccess,searchVendorError)
        }
        vm.editVendor = function(vendor){
            $state.go('vendorDetails',{vendorId:vendor.id});
        }
        vm.removeVendor = function(vendor){
           Vendor.deleteById(vendor.id).then(function(response){
               vm.searchVendors();
           });
        }
        vm.searchVendors();
    }

    // Vendor Details Controller
    function VendorDetailCtrl(Vendor,$state,$stateParams,$window,VendorCategory,growl){
        var vm = this ;
        vm.vendorId = $stateParams.vendorId ;
        vm.vendor = {};
        vm.vendorCategories = [];
        var success =  function(response){
            growl.success('Save with success ');
            vm.vendor = {};
            $state.go('vendorList');

        };
        var error =  function(response){
            growl.error("error durring vendor creations");
        };

        vm.save = function(){
            if(angular.isNumber(vm.vendorId)){
                Vendor.update(vm.vendor).then(success,error)
            }else{
                Vendor.create(vm.vendor).then(success,error)
            }

        }
        vm.addVendorCat = function(){
           var name = prompt("Category");
            var category = {};
            category.name = name ;

            VendorCategory.create(category).then(function(response){
                vm.vendor.category = response.data ;
                vm.populateCategory();
            })
        }

        vm.populateCategory = function(){
            VendorCategory.listAll().then(function(response){
                vm.vendorCategories =  response.data.resultList ;
            })
        }

        vm.init = function(){
            vm.populateCategory();
            if(angular.isNumber(vm.vendorId)){
                Vendor.findById(vm.vendorId).then(function(response){
                    vm.vendor = response.data ;
                })
            }

        }
        vm.init();
    }
})();
