/**
 *@author: hermann Tchimkap 
 */
(function(){
    'use strict'
    angular.module('caretaker').controller('UnitsDetailsCtrl',UnitsDetailsCtrl);
    angular.module('caretaker').controller('UnitViewCtrl',UnitViewCtrl);
    
    UnitsDetailsCtrl.$inject=['Unit','Property','$stateParams','$window','$state','growl'];
    UnitViewCtrl.$inject=['$state'];
    
    function UnitsDetailsCtrl(Unit,Property,$stateParams,$window,$state,growl){
    	 var vm=this;
   
    	 vm.block="true";
    	// retrieve Idproperty from property to persist unit
    	 vm.unit={};
    	 vm.property={}; //retrieve a line of properties in the sel
    	 vm.properties=[];//all properties registed in DataBase
    	 
    	 var success =  function(response){
    		 growl.success('Save with success ');
             vm.unit = {};
         };
         var error =  function(response){
             $window.alert("error durring units creations");
         };
         
         //for populating properties
         vm.populateProperty = function(){
             Property.listAll().then(function(response){
            	 vm.properties =  response.data.resultList ;
             });
         };

         vm.save = function(){       
        	 vm.populateProperty();
        	 vm.unit.propertyId=vm.property.id; // should be verified
        	 vm.unit.propertyName=vm.property.name;//set propertyName of propertyUnit class
        	 Unit.create(vm.unit).then(success,error);
         };
         
         vm.saveAndQuit = function(){
             vm.save();
             $state.go('unitView');
         };
         
         vm.showblock=function(enter){
 			if  (enter=="tab1"){
 				
     		vm.block="true";;}
 			
 			else{
 				
 				vm.block="false";
     	}}
    }

    // controller to manage unit overview
  
    function UnitViewCtrl($state){
    	var vm=this;
    	vm.block=1;
		vm.showblock=function(enter){
			if  (enter="tab1")
    		vm.block="true";
			else
				vm.block="false";
    	}
    	
    }
    
})();