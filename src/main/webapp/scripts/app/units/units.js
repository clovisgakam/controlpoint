/**
 * @author:hermann tchimkap
 */
(function(){
    'use strict';
    angular.module('caretaker')
        .config(function ($stateProvider) {
            $stateProvider
                .state('unitDetails', {
                    parent: 'index',
                    url: '/units-detail',
                    data: {
                        roles: ['PROPERTY_MANAGER','ADMINISTRATOR'],
                        pageTitle: 'Details of Units'
                    },
                    templateUrl: "scripts/app/units/units.details.html",
                    controller: "UnitsDetailsCtrl as unitDetail"
                })
                .state('unitView', {
                    parent: 'index',
                    url: '/units-view',
                    data: {
                        roles: ['PROPERTY_MANAGER','ADMINISTRATOR'],
                        pageTitle: 'Unit View'
                    },
                    templateUrl: "scripts/app/units/unit.view.html",
                    controller: "UnitsDetailsCtrl as unitCtrl"
                })
        });





})();