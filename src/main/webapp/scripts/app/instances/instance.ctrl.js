/**
 * Created by clovisgakam on Monday4/6/15.
 */

(function(){
    'use strict'
    angular.module('caretaker').controller('InstanceCtrl',InstanceCtrl);
    InstanceCtrl.$inject = ['Auth','Principal','$state','Instance','$scope'];
    function InstanceCtrl(Auth,Principal,$state,Instance, $scope){
   
    	var vm = this;
    	
    	vm.instances = {};
    	vm.searchInput = {};
    	vm.itemPerPage = 30;
    	vm.error = {};
    	
    	init();
    	
    	function init(){

    		vm.searchInput = {
                    entity:{},
                    fieldNames:[],
                    start: 0,
                    max: vm.itemPerPage
                }
    		
    		findInstances(vm.searchInput);
    	}
    	
    	function findInstances(searchInput){
    		
    		Instance.findAll().then(function(response){
    			vm.instances = response.data.resultList;
    		}, function(error){
    			vm.error = error;
    			console.log(error);
    		});
    		
    	}
    	
    }
})();
    