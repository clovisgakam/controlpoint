/**
 * @author clovis.gakam
 * @desc instances config
 */
(function(){
    'use strict';
    angular.module('caretaker')
        .config(function ($stateProvider) {
            $stateProvider
                .state('instanceList', {
                    parent: 'index',
                    url: '/instances-list',
                    data: {
                        roles: ['ADMINISTRATOR'],
                        pageTitle: 'List of Instances'
                    },
                    templateUrl: "scripts/app/instances/instance.list.html",
                    controller: 'InstanceCtrl as InstanceCtrl'

                })
                .state('instanceDetails', {
                    parent: 'index',
                    url: '/instance-details',
                    data: {
                        roles: ['ADMINISTRATOR'],
                        pageTitle: 'Details of Instances'
                    },
                    templateUrl: "scripts/app/instances/instance.details.html"

                })

        });





})();
