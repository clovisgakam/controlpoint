/**
 * @author achil.sowa
 * @desc finance
 */
(function() {
	'use strict';
	angular.module('caretaker').config(function($stateProvider) {
		$stateProvider.state('financeOverview', {
			parent : 'index',
			url : '/finance-overview',
			data : {
				roles : [ 'ADMINISTRATOR', 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
				pageTitle : 'Finance'
			},
			controller : "FinanceOverviewCtrl as financeOverview",
			templateUrl : "scripts/app/finance/finance.view.html"
		})
	});
})();
