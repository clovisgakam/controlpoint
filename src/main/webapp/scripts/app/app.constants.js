"use strict";
angular.module('caretaker')
.constant('AppName', 'Control Point')
.constant('VERSION', '1.0.0-SNAPSHOT')
.constant('AppContext', '/controlpoint');
