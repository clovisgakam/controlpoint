/**
 * @author clovis.gakam
 * @desc users config
 */
(function(){
    'use strict';
    angular.module('caretaker')
        .config(function ($stateProvider) {
            $stateProvider
                .state('usersList', {
                    parent: 'index',
                    url: '/users-list',
                    data: {
                        roles: [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
                        pageTitle: 'List of users'
                    },
                    controller: "UsersListCtrl as usersList",
                    templateUrl: "scripts/app/users/users.list.html"

                })
                .state('userDetails', {
                    parent: 'index',
                    url: '/users-details:userId',
                    data: {
                        roles: [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
                        pageTitle: 'Details of user'
                    },
                    controller: "UserCreateCtrl as userCreate",
                    templateUrl: "scripts/app/users/users.create.html"

                })
                .state('userCreate', {
                    parent: 'index',
                    url: '/users-create',
                    data: {
                        roles: [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
                        pageTitle: 'Create user'
                    },
                    controller: "UserCreateCtrl as userCreate",
                    templateUrl: "scripts/app/users/users.create.html"

                })
                 .state('userProfile', {
                    parent: 'index',
                    url: '/user-profile',
                    data: {
                        roles: [ 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
                        pageTitle: 'User Profile'
                    },
                    controller: "UserProfileCtrl as userProfile",
                    templateUrl: "scripts/app/users/users.profile.html"

                })

        });

})();
