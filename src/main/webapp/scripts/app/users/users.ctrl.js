/*
 * @author boris Fouomene
 */
(function(){
    'use strict'
    angular.module('caretaker').controller('UsersListCtrl',UserListCtrl);
    angular.module('caretaker').controller('UserDetailCtrl',UserDetailCtrl);
    angular.module('caretaker').controller('UserCreateCtrl',UserCreateCtrl);
    angular.module('caretaker').controller('UserProfileCtrl',UserProfileCtrl);
    UserListCtrl.$inject = ['User','$state','$stateParams','growl'];
    UserDetailCtrl.$inject = ['User','$state','$stateParams','growl','$q','ModelValidator'];
    UserCreateCtrl.$inject = ['User','$state','$stateParams','growl','$q','Address'];
    UserProfileCtrl.$inject = ['Auth','Principal','User','$state','$stateParams','growl','$q','$filter','$scope','Login'];

    // user List controller
    function UserListCtrl(User,$state,$stateParams,growl) {
        var me = this ;
        
        me.isUpdate = false;
        
        me.searchInput = {
            start:0,
            max:50,
            entity:{},
            fieldNames:[]
        };
        
        me.users = [];

        var searchUserSuccess =  function(response){
          me.users = response.data;
        }
        
        var searchUserError =  function(response){
            growl.info("no data found");
        }

        me.searchUsers = function(){
            User.findByAppInstance().then(searchUserSuccess,searchUserError);
        }
        
        me.editUser = function(user){
            $state.go('userDetails',{userId:user});
        }

        
        //this function is run the first time when usersList state is enabled 
        me.searchUsers();
    }

    // create user Controller
    function UserCreateCtrl(User, $state,$stateParams,growl,$q,Address){
    	var me = this;
    	
    	me.user = (angular.isObject($stateParams.userId)) ? $stateParams.userId :{};
    	me.addresses = [];
    	me.confPassword = "";
    	me.genders = [
                       {label: 'Male', value: 'MALE'},
                       {label: 'Female', value: 'FEMALE'},
                       {label: 'Neutral', value: 'NEUTRAL'}
                     ];
    	
    	var success =  function(response){
        	growl.success("Saved successfull")
            $state.go('usersList');
        };
        var error =  function(response){
            growl.error("error durring user creations");
        };
        
        me.save = function () {
            if(!(angular.isNumber(me.user.id))){
                User.create(me.user).then(success,error);
            }else{
                User.update(me.user).then(success,error);
            }

        }
        
        me.isEdit = angular.isNumber(me.user.id);


        me.populateAddresse = function(){
        	Address.listAll().then(function(response){
                me.addresses =  response.data.resultList ;
            })
        }
        
    	me.passwordValidator = function(password) {

    		if(!password){return;}
    		
    		if (password.length < 5) {
    			return "Password must be at least 5 characters";
    		}

    		if (!password.match(/[A-Z]/)) {
    			 return "Password must have at least one capital letter";
    		}

    		if (!password.match(/[0-9]/)) {
    			 return "Password must have at least one number";
    		}

    		return true;
    	};
        
        me.init = function(){
        	me.populateAddresse();
        }
        
        me.init();
    	
    }
    
    // User Details Controller
    function UserDetailCtrl(User, $state,$stateParams,growl,$q, ModelValidator){
        
    	var me = this ;
        
        
        me.genders = [
                    {label: 'Male', value: 'MALE'},
                    {label: 'Female', value: 'FEMALE'},
                    {label: 'Neutral', value: 'NEUTRAL'}
                  ];
        
        me.roles = [
                    {label: 'ADMINISTRATOR', value: 'ADMINISTRATOR'},
                    {label: 'PROPERTY MANAGER', value: 'PROPERTY_MANAGER'},
                    {label: 'CARETAKER', value: 'CARETAKER'},
                    {label: 'OWNER', value: 'OWNER'},
                    {label: 'VENDOR', value: 'VENDOR'}
                    ]

        //admin can create new user with password
        me.canCreatePassword = true;
        
        me.confirmPassword = "Confirm Password";
        
        me.user = {
        	
        };
        
        me.userId = $stateParams.userId ;
        
        //$window.alert(me.userId)
        
        var success =  function(response){
        	growl.success("Saved successfull")
            $state.go('usersList');
        };
        var error =  function(response){
            growl.error("error durring user creations");
        };
        
        
        me.vRules = {
        		firstName :  {required:true},
        		lastName : {required : true},
        		mobilePhone : {required : true},
        		password : {required : true, minlength : 5},
        		confPassword: {required : true, minlength : 5},
        		gender : {required : true},
        		email : {type:'email', required : true},
        		//roleNames : {required : true}
        }
        
        // Create a placeholder for formFor to fill in with controller methods:
        //me.formController = {}
        
        me.save = function () {
        	ModelValidator.validateAll(me.user, me.vRules).then(function(){
        		if( (me.user.password != me.user.confPassword) & (!angular.isNumber(me.userId))) {
                    growl.error("Passwords does not math")
            		return false;
            	} 
    			
    			//the property fullname are not define from now, sow, we have to delete it
            	if(typeof me.user.fullname != undefined) delete me.user.fullname;
            	
            	//the property confPassword are not define from now, sow, we have to delete it
            	if(typeof me.user.confPassword != undefined) delete me.user.confPassword;
    			
            	if(angular.isNumber(me.userId)){
            		//alert(JSON.stringify(me.user))
                	User.update(me.user).then(success,error);
                } else {
                    User.create(me.user).then(success,error);
                }  	
        	});
        }       


        me.backToList = function () {
        	 $state.go('usersList');
        }

        
        if(angular.isNumber(me.userId)){
            User.findById(me.userId).then(function(response){
                me.user = response.data ;
                me.isUpdate = true;
                me.canCreatePassword = false;
            })
        }
        
    }
    
    
    // User profile Controller
    function UserProfileCtrl(Auth,Principal,User,$state,$stateParams,growl,$q, $filter, $scope,Login){
        /*
    	if(!angular.isNumber($stateParams.userId)) {
    		growl.error("You are not allowed to do this operation")
    		$state.go(Principal.resolveHomePage());
    	}*/
    	
    	
    	var me = this ;
        
    	me.user = Principal.getUser();
    	
    	if(!angular.isNumber(me.user.userId)) {
    		growl.error("You are not allowed to do this operation")
    		$state.go(Principal.resolveHomePage());
    	}

    	
        me.genders = [
                    {label: 'Male', value: 'MALE'},
                    {label: 'Female', value: 'FEMALE'},
                    {label: 'Neutral', value: 'NEUTRAL'}
                  ];
        
        me.showGender = function() {
            var selected = $filter('filter')(me.genders, {value: me.user.gender});
            return (me.user.gender && selected.length) ? selected[0].label : 'No Set';
        };
        

        me.resetPFields = function () {
        	me.checkedOldPassword = ""
        	me.newPassword = ""
        	me.confPassword = ""
        }
        
        me.backToList = function () {
        	 $state.go('usersList');
        }        
        
        me.showOldPForm = function () {
        	me.resetPFields()
        	me.showUpdateP = false;
        	me.showOldP = true;
        	me.showNewP = false;
        	$scope.oldPasswordForm.$show()
        }
        
        me.showNewPForm = function () {
        	me.resetPFields()
        	$scope.newPasswordForm.$show()
        	$scope.confPasswordForm.$show()
        	me.showUpdateP = false;
        	me.showNewP = true;
        	me.showOldP = false;
        }
        
        me.showUpdatePForm = function () {
        	me.resetPFields()
        	me.showUpdateP = true;
        	me.showOldP = false;
        	me.showNewP = false;
        }
        
        
        //check the old user's password
        me.checkOldPass = function () {
            //check if the old password is valid
        	var pass = me.checkedOldPassword;
        	alert(pass)
        	
        	return me.updatePassword();
        	
        	/***
            Login.isValidConnectedUserPassword(pass).then(
            		function(response){
            			alert(JSON.stringify(response))
            			me.showNewPForm();
                    	growl.success("Your password have been updated");
                    
            		},function (error) {
            			alert(JSON.stringify(error))
            			growl.error("Your old password does not match. Please Try again");
		    			me.showOldPForm();
		    			$scope.oldPasswordForm.$show()
            		});*/
            
        }
        
        var success = function () {
        	growl.success("Data Save Successfull");
        	me.showUpdatePForm();
        }
        
        var error = function () {
        	growl.error("Error when save data");
        	//me.showUpdatePForm();
        }
        
        //update a given field of form
        me.updateField = function (value,fieldName) {
        	var backup = me.user[fieldName]
        	me.user[fieldName] = value;
        	
        	//the property fullname are not define from now, sow, we have to delete it
        	if(typeof me.user.fullname != undefined) delete me.user.fullname;
        	
        	User.update(me.user).then(
				function(){
	        		success();
	        	},
				function(){
	        		error();
	        		me.user[fieldName] = backup;
	        	})
        }
        
        me.updatePassword = function () {
        	me.newPassword = "admino"
        	Login.updateCurrentUserPass(me.newPassword).then(
        			function(response){
        				alert(JSON.stringify(response))
        			}, 
        			function (error){
        				alert(JSON.stringify(error))
        			}
        	)
        }
        
        me.init = function () {
        	User.findById(Principal.getUser().userId).then(function(response){
                me.user = response.data ;
            })
        }
        
        me.init()
        
        me.showUpdatePForm();
        
        
    }
})();
