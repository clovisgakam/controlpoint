/**
* @author clovis.gakam
*/
(function(){
    'use strict'
    angular.module('caretaker').controller('AccessCtrl',AccessCtrl);
    AccessCtrl.$inject = ['Auth','Principal','$state','$rootScope','$window','ModelValidator','growl','$scope'];
    function AccessCtrl(Auth,Principal,$state,$rootScope,$window,ModelValidator,growl,$scope){
        var vm = this ;
        var success = function(data){
            Auth.currentUser().then(function(response){
                Principal.authenticate(response.data).then(function(identity){
                    $rootScope.userName = angular.isDefined(identity.username)?identity.username : '' ;
                    $rootScope.userRole = angular.isDefined(identity.roleAndPerms)?identity.roleAndPerms[0].role : '';
                    $state.go(Principal.resolveHomePage());
                });

            });
        };
        //Update by Fouomene Boris
        vm.genders = [
                      {label: 'Male', value: 'MALE'},
                      {label: 'Female', value: 'FEMALE'},
                      {label: 'Neutral', value: 'NEUTRAL'}
                    ];
        
        vm.vRules = {
        		company : {required : true},
        		firstname :  {required:true},
        		lastname : {required : true},
        		mobile : {required : true},
        		country : {required : true},
        		city : {required : true},
        		password : {required : true, minlength : 5},
        		confPassword: {required : true, minlength : 5},
        		gender : {required : true},
        		email : {type:'email', required : true}
        		
        }

        var error = function(data){
            growl.error("E");
        };
        
        vm.credential = {};
        vm.regData = {};
        
        vm.login = function(){
            Auth.currentUser().then(success,function(data){

                Auth.login(vm.credential).then(success);
            });

        }
        
        
       //for Carousel
        $scope.slideInterval = 5000;
        $scope.folders = [
          'slides'
        ];
        
        $scope.images = ["2.jpg","3.jpg"];
       
        $scope.currentFolder = $scope.folders[0];
        
        $scope.selectFolder = function (folder) {
        	$scope.$apply(function () {
        		$scope.slideInterval = 5000;
        		$scope.currentFolder = folder;
            });
        };
      	
        
        vm.isRegistration = false ;
        vm.gotToLogin = function () {
        	vm.isRegistration = false;
           	$scope.selectFolder(0);
        	$scope.selectFolder('slides');
        }
        
        vm.goToRegistration = function () {
        	vm.isRegistration = true;
        	$scope.selectFolder(0);
        	$scope.selectFolder('slides');
        }
        
        vm.registration = function(){
        	ModelValidator.validateAll(vm.regData, vm.vRules).then(function(){
        		if( (vm.regData.password != vm.regData.confPassword)) {
                    growl.error("Passwords does not math")
            		return false;
            	} 
    			
            	//the property confPassword are not define from now, sow, we have to delete it
            	//delete vm.regData.confPassword;
    			
                Auth.register(vm.regData).then(function(data){
                    vm.credential.j_username = vm.regData.email;
                    vm.credential.j_password = vm.regData.password;
                    growl.success("Account successfull created");
                    vm.login();

                },function(data){
                    growl.error("An account already exist with the sam email");
                });
        	});
        }
    }
})();
