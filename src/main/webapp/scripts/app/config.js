/**
 * @author clovis.gakam
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider,growlProvider, $httpProvider) {
    $urlRouterProvider.otherwise("/index/main");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });
    growlProvider.onlyUniqueMessages(false);
    growlProvider.globalReversedOrder(true);
    growlProvider.globalTimeToLive(5000);
    //growlProvider.globalPosition('bottom-right');
    $httpProvider.interceptors.push(growlProvider.serverMessagesInterceptor);
   // $httpProvider.responseInterceptors.push('httpInterceptor');

    $stateProvider

        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "views/common/content.html"
        })
        .state('public', {
            abstract: true,
            url: "/public",
            templateUrl: "views/common/public.html"
        })
        .state('index.main', {
            url: "/main",
            data: {
                roles: ['PUBLIC'],
                pageTitle: 'Main Page'
            },
            templateUrl: "views/main.html"
        })
        .state('public.login', {
            url: "/login",
            templateUrl: "scripts/app/access/login.html",
            controller: "AccessCtrl as Access",
            data: { pageTitle: 'Login Page' }
        })
}
angular
    .module('caretaker')
    .config(config)
    .run(function($rootScope, $state,AppName,VERSION,Auth,Principal,editableOptions, editableThemes) {
        $rootScope.$state = $state;
        $rootScope.appName = AppName;
        $rootScope.VERSION = VERSION;
        $rootScope.userName = angular.isDefined(Principal.identity.username)?Principal.identity.username : '';
        $rootScope.userRole = angular.isDefined(Principal.identity.roles)?Principal.identity.roles[0] : '';
        $rootScope.pageTitle = '';
        $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams) {
            $rootScope.toState = toState;
            $rootScope.toStateParams = toStateParams;
            $rootScope.pageTitle = toState.data.pageTitle ;

        });

        $rootScope.$on('$stateChangeSuccess',  function(event, toState, toParams, fromState, fromParams) {
            $rootScope.previousStateName = fromState.name;
            $rootScope.previousStateParams = fromParams;
            if(toState.name !== 'public.login') {
                if (Principal.isIdentityResolved()) {
                    Auth.authorize();
                } else {
                    $state.go('public.login');
                }	
            }
        });

        $rootScope.back = function() {
            // If previous state is 'activate' or do not exist go to 'home'
            if ($rootScope.previousStateName === 'activate' || $state.get($rootScope.previousStateName) === null) {
                $state.go('index.main');
            } else {
                $state.go($rootScope.previousStateName, $rootScope.previousStateParams);
            }
        };
        
        //set X-Editable Theme : 
        editableThemes.bs3.inputClass = 'input-sm form-control';
    	editableThemes.bs3.buttonsClass = 'btn-sm btn-primary';
		editableOptions.theme = 'bs3';
		editableThemes['bs3'].cancelTpl = '<button type="button" class="btn btn-danger" ng-click="$form.$cancel()">'+'<span class="fa fa-times fa-lg" title="cancel"></span>'+'</button>'

    });
