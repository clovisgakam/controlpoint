/**
 * @author clovis.gakam
 */
(function(){
    'use strict'
    angular.module('caretaker').controller('TrusteeListCtrl',TrusteeListCtrl);
    angular.module('caretaker').controller('TrusteeDetailCtrl',TrusteeDetailCtrl);
    TrusteeListCtrl.$inject = ['Trustee','$state','$stateParams','$window'];
    TrusteeDetailCtrl.$inject = ['Trustee','$state','$stateParams','$window','growl'];

    // Trustee List controller
    function TrusteeListCtrl(Trustee,$state,$stateParams,$window){
        var vm = this ;
        vm.searchInput = {
            start:0,
            max:50,
            entity:{},
            fieldNames:[]
        };
        vm.tustees = [];

        var searchTrusteeSuccess =  function(response){
          vm.trustees = response.data.resultList ;
        }
        var searchTrusteeError =  function(response){
            $window.alert("no data found");
        }

        vm.searchTrustees = function(){
            Trustee.findByLike(vm.searchInput).then(searchTrusteeSuccess,searchTrusteeError)
        }
        vm.editTrustee = function(trustee){
            $state.go('trusteeDetails',{trusteeId:trustee.id});
        }
        vm.removeTrustee = function(trustee){
           Trustee.deleteById(trustee.id).then(function(response){
               vm.searchTrustees();
           });
           
        }
        vm.searchTrustees();
    }

    // Trustee Details Controller
    function TrusteeDetailCtrl(Trustee,$state,$stateParams,$window,growl){
        var vm = this ;
        vm.trusteeId = $stateParams.trusteeId ;
        vm.trustee = {};
        var success =  function(response){
            growl.success('Save with success ');
            vm.trustee ={};

        };
        var error =  function(response){
            $window.alert("error durring Trustee creations");
        };

        vm.save = function(){
            if(angular.isNumber(vm.trusteeId)){
                Trustee.update(vm.trustee).then(success,error)
            }else{
                Trustee.create(vm.trustee).then(success,error)
            }

        }
        vm.saveAndQuit = function(){
            vm.save();
            $state.go('trusteeList');
        }



        vm.init = function(){
            if(angular.isNumber(vm.trusteeId)){
                Trustee.findById(vm.trusteeId).then(function(response){
                    vm.trustee = response.data ;
                })
            }

        }
        vm.init();
    }
})();
