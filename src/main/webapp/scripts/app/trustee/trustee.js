/**
 * @author clovis.gakam
 * @desc Trustee config
 */
(function(){
    'use strict';
    angular.module('caretaker')
        .config(function ($stateProvider) {
            $stateProvider
                .state('trusteeList', {
                    parent: 'index',
                    url: '/trustee-list',
                    data: {
                        roles: ['ADMINISTRATOR','PROPERTY_MANAGER'],
                        pageTitle: 'List of Trustee'
                    },
                    controller: "TrusteeListCtrl as trusteeList",
                    templateUrl: "scripts/app/trustee/trustee.list.html"

                })
                .state('trusteeDetails', {
                    parent: 'index',
                    url: '/trustee-details:trusteeId',
                    data: {
                        roles: ['ADMINISTRATOR','PROPERTY_MANAGER'],
                        pageTitle: 'Details of Trustee'
                    },
                    controller: "TrusteeDetailCtrl as trusteeDetail",
                    templateUrl: "scripts/app/trustee/trustee.details.html"

                })

        });





})();
