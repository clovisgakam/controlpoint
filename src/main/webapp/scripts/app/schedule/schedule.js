/**
 * @author achil.sowa
 * @desc schedule
 */
(function() {
	'use strict';
	angular.module('caretaker').config(function($stateProvider) {
		$stateProvider.state('scheduleOverview', {
			parent : 'index',
			url : '/schedule-overview',
			data : {
				roles : [ 'ADMINISTRATOR', 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
				pageTitle : 'Schedule Events'
			},
			controller : "ScheduleOverviewCtrl as scheduleOverview",
			templateUrl : "scripts/app/schedule/schedule.view.html"

		})/*.state('taskOverview', {
			parent : 'index',
			url : '/task-overview:taskId',
			data : {
				roles : [ 'ADMINISTRATOR', 'CARETAKER', 'PROPERTY_MANAGER', 'OWNER', 'TENANT' ],
				pageTitle : 'Task Overview'
			},
			controller : "TaskOverviewCtrl as taskOverview",
			templateUrl : "scripts/app/tasks/task.overview.html"

		})*/
		
		
	});

})();
