/**
 * @author clovis.gakam
 */
(function () {
    angular.module('caretaker', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'datatables',
        'angular-growl', 
        'angularValidator',  
       'formFor',  // formFor and Bootstrap-friendly HTML templates
       'formFor.bootstrapTemplates',
       'xeditable', // XEDITABLE,
       'angular-svg-round-progress'
       // XEDITABLE,
    ]) 
    
})();

